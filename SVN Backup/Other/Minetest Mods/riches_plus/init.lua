minetest.register_node("riches_plus:diamondspawn", {
	tiles = {"spawnegg_diamond.png"},
	inventory_image = "spawnegg_diamond.png",
	description = "Spawn Diamond Block",
	groups = {snappy=1,choppy=2,oddly_breakable_by_hand=2,flammable=3},
})

minetest.register_node("riches_plus:goldspawn", {
	tiles = {"spawnegg_gold.png"},
	inventory_image = "spawnegg_gold.png",
	description = "Spawn Gold Block",
	groups = {snappy=1,choppy=2,oddly_breakable_by_hand=2,flammable=3},
})

minetest.register_node("riches_plus:emeraldspawn", {
	tiles = {"spawnegg_emerald.png"},
	inventory_image = "spawnegg_emerald.png",
	description = "Spawn Emerald Block",
	groups = {snappy=1,choppy=2,oddly_breakable_by_hand=2,flammable=3},
})

minetest.register_node("riches_plus:emerald", {
	tiles = {"emerald.png"},
	inventory_image = "emerald.png",
	description = "Emerald",
	groups = {snappy=1,choppy=2,oddly_breakable_by_hand=2,flammable=3},
})

minetest.register_node("riches_plus:emerald_block", {
	tiles = {"emerald_block.png"},
	description = "Emerald Block",
	groups = {snappy=1,choppy=2,oddly_breakable_by_hand=2,flammable=3},
})

minetest.register_craft({
	output = 'riches_plus:diamondspawn 1',
	recipe = {
		{'', 'default:clay_lump', ''},
		{'default:clay_lump', 'default:diamond', 'default:clay_lump'},
		{'default:clay_lump', 'default:clay_lump', 'default:clay_lump'},
	}
})

minetest.register_craft({
	output = 'riches_plus:goldspawn 1',
	recipe = {
		{'', 'default:clay_lump', ''},
		{'default:clay_lump', 'default:gold_ingot', 'default:clay_lump'},
		{'default:clay_lump', 'default:clay_lump', 'default:clay_lump'},
	}
})

minetest.register_craft({
	output = 'riches_plus:emeraldspawn 1',
	recipe = {
		{'', 'default:clay_lump', ''},
		{'default:clay_lump', 'riches_plus:emerald', 'default:clay_lump'},
		{'default:clay_lump', 'default:clay_lump', 'default:clay_lump'},
	}
})

minetest.register_craft({
	output = 'riches_plus:emerald 1',
	recipe = {
		{'', 'default:clay_lump', ''},
		{'default:clay_lump', 'default:mese_crystal', 'default:clay_lump'},
		{'default:clay_lump', 'default:clay_lump', 'default:clay_lump'},
	}
})

minetest.register_craft({
	output = 'riches_plus:emerald_block 1',
	recipe = {
		{'riches_plus:emerald', 'riches_plus:emerald', 'riches_plus:emerald'},
		{'riches_plus:emerald', 'riches_plus:emerald', 'riches_plus:emerald'},
		{'riches_plus:emerald', 'riches_plus:emerald', 'riches_plus:emerald'},
	}
})

minetest.register_craft({
	type = "cooking",
	recipe = "default:mese_crystal",
	output = "riches_plus:emerald",
})

minetest.register_abm({
	nodenames = {"riches_plus:diamondspawn"},
	interval = 0,
	chance = 1,
	action = function(pos)
		minetest.add_node(pos, {name="default:diamondblock"})
	end,
})

minetest.register_abm({
	nodenames = {"riches_plus:goldspawn"},
	interval = 0,
	chance = 1,
	action = function(pos)
		minetest.add_node(pos, {name="default:goldblock"})
	end,
})

minetest.register_abm({
	nodenames = {"riches_plus:emeraldspawn"},
	interval = 0,
	chance = 1,
	action = function(pos)
		minetest.add_node(pos, {name="riches_plus:emerald_block"})
	end,
})