﻿Imports System.Drawing.Printing
Imports System.Drawing.Drawing2D
Public Class mspaint
    Private startX As Integer = 0   'start position x of line
    Private startY As Integer = 0   'start position y of line
    Private oldEndX As Integer = 0  'the previous ending position x
    Private oldEndY As Integer = 0  'the previous ending position y
    Private gBitmap As Bitmap       'bitmap to place in picturebox
    Private isMouseDown As Boolean = False  'recognize mouse down or up
    Private allowCoolMove As Boolean = False
    Private myCoolPoint As New Point
    Private Sub PictureBox2_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Panel1.MouseDown
        allowCoolMove = True
        myCoolPoint = New Point(e.X, e.Y)
        Me.Cursor = Cursors.SizeAll
    End Sub
    Private Sub Panel1_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Panel1.MouseMove
        If allowCoolMove = True Then
            Me.Location = New Point(Me.Location.X + e.X - myCoolPoint.X, Me.Location.Y + e.Y - myCoolPoint.Y)
        End If
    End Sub
    Private Sub PictureBox2_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Panel1.MouseUp
        allowCoolMove = False
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub
    Private Sub PictureBox1_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles PictureBox1.MouseDown
        isMouseDown = True
        startX = e.X
        startY = e.Y
    End Sub
    Private Sub PictureBox1_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles PictureBox1.MouseMove
        If isMouseDown = True Then
            'draw line during move
            Dim g As Graphics = Graphics.FromImage(gBitmap)
            g.DrawLine(New Pen(PictureBox1.BackColor), startX, startY, oldEndX, oldEndY)
            PictureBox1.Image = gBitmap

            g.DrawLine(Pens.Black, startX, startY, e.X, e.Y)
            PictureBox1.Image = gBitmap
            '
            oldEndX = e.X
            oldEndY = e.Y
        End If
    End Sub
    Private Sub PictureBox1_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles PictureBox1.MouseUp
        isMouseDown = False
    End Sub

    Private Sub mspaint_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        gBitmap = New Bitmap(PictureBox1.Width, PictureBox1.Height)
    End Sub
End Class