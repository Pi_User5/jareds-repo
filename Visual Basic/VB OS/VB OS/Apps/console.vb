﻿Public Class console
    Private allowCoolMove As Boolean = False
    Private myCoolPoint As New Point
    Private Sub PictureBox2_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Panel1.MouseDown
        If e.Button = MouseButtons.Left Then
            allowCoolMove = True
            myCoolPoint = New Point(e.X, e.Y)
        End If
    End Sub
    Private Sub Panel1_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Panel1.MouseMove
        If e.Button = MouseButtons.Left Then
            If allowCoolMove = True Then
                Me.Location = New Point(Me.Location.X + e.X - myCoolPoint.X, Me.Location.Y + e.Y - myCoolPoint.Y)
            End If
        End If
    End Sub
    Private Sub Panel1_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Panel1.MouseUp
        If e.Button = MouseButtons.Left Then
            allowCoolMove = False
        End If
    End Sub
    Private Sub Panel1_Maximize(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Panel1.MouseDoubleClick
        If e.Button = MouseButtons.Left Then
            Me.WindowState = FormWindowState.Maximized
            If Me.WindowState = FormWindowState.Maximized = True Then
                Me.WindowState = FormWindowState.Normal
            End If
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub
    Private Sub Form1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If e.KeyChar = ChrW(Keys.Enter) Then
            Call CommandInterpreter()
            TextBox1.Clear()
        End If
    End Sub
    Private Sub CommandInterpreter()
        Dim consoleinput As String
        consoleinput = TextBox1.Text
        Select Case consoleinput
            Case "cls"
                RichTextBox1.Clear()
            Case "exit"
                Me.Close()
            Case "help"
                RichTextBox1.Text &= Environment.NewLine & "Available commands:"
            Case Else
                RichTextBox1.Text &= Environment.NewLine & "Unkown command"
        End Select
    End Sub

    Private Sub console_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.StartPosition = FormStartPosition.CenterScreen
        TextBox1.Focus()
    End Sub

    Private Sub PictureBox1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox1.MouseDoubleClick
        Me.Close()
    End Sub
End Class