﻿Public Class apps
    Private allowCoolMove As Boolean = False
    Private myCoolPoint As New Point
    Private Sub PictureBox2_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Panel1.MouseDown
        If e.Button = MouseButtons.Left Then
            allowCoolMove = True
            myCoolPoint = New Point(e.X, e.Y)
        End If
    End Sub
    Private Sub Panel1_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Panel1.MouseMove
        If e.Button = MouseButtons.Left Then
            If allowCoolMove = True Then
                Me.Location = New Point(Me.Location.X + e.X - myCoolPoint.X, Me.Location.Y + e.Y - myCoolPoint.Y)
            End If
        End If
    End Sub
    Private Sub Panel1_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Panel1.MouseUp
        If e.Button = MouseButtons.Left Then
            allowCoolMove = False
        End If
    End Sub
    Private Sub Panel1_Maximize(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Panel1.MouseDoubleClick
        If e.Button = MouseButtons.Left Then
            Me.WindowState = FormWindowState.Maximized
            If Me.WindowState = FormWindowState.Maximized = True Then
                Me.WindowState = FormWindowState.Normal
            End If
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub PictureBox2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox2.Click
        notepad.StartPosition = FormStartPosition.Manual
        notepad.TopLevel = False
        shellbackground.Panel1.Controls.Add(notepad)
        notepad.Top = (Screen.PrimaryScreen.Bounds.Height - notepad.Height) / 2
        notepad.Left = (Screen.PrimaryScreen.Bounds.Width - notepad.Width) / 2
        Me.Close()
        notepad.Show()
    End Sub

    Private Sub PictureBox5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox5.Click
        multimedia.TopLevel = False
        shellbackground.Panel1.Controls.Add(multimedia)
        multimedia.Show()
        Me.Close()
    End Sub

    Private Sub PictureBox3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox3.Click
        mspaint.StartPosition = FormStartPosition.Manual
        mspaint.TopLevel = False
        shellbackground.Panel1.Controls.Add(mspaint)
        mspaint.Top = (Screen.PrimaryScreen.Bounds.Height - mspaint.Height) / 2
        mspaint.Left = (Screen.PrimaryScreen.Bounds.Width - mspaint.Width) / 2
        Me.Close()
        mspaint.Show()
    End Sub

    Private Sub PictureBox4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox4.Click
        wordpad.StartPosition = FormStartPosition.Manual
        wordpad.TopLevel = False
        shellbackground.Panel1.Controls.Add(wordpad)
        wordpad.Top = (Screen.PrimaryScreen.Bounds.Height - wordpad.Height) / 2
        wordpad.Left = (Screen.PrimaryScreen.Bounds.Width - wordpad.Width) / 2
        Me.Close()
        wordpad.Show()
    End Sub

    Private Sub PictureBox1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox1.Click
        console.StartPosition = FormStartPosition.Manual
        console.TopLevel = False
        shellbackground.Panel1.Controls.Add(console)
        console.Top = (Screen.PrimaryScreen.Bounds.Height - console.Height) / 2
        console.Left = (Screen.PrimaryScreen.Bounds.Width - console.Width) / 2
        Me.Close()
        console.Show()
    End Sub

    Private Sub PictureBox6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox6.Click
        ie.StartPosition = FormStartPosition.Manual
        ie.TopLevel = False
        shellbackground.Panel1.Controls.Add(ie)
        ie.Top = (Screen.PrimaryScreen.Bounds.Height - ie.Height) / 2
        ie.Left = (Screen.PrimaryScreen.Bounds.Width - ie.Width) / 2
        Me.Close()
        ie.Show()
    End Sub
End Class