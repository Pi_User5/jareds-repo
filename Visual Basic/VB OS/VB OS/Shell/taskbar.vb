﻿Public Class taskbar
    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)
        MyBase.Top = Screen.PrimaryScreen.WorkingArea.Bottom - Me.Height
        MyBase.Left = Screen.PrimaryScreen.WorkingArea.Left
        Me.Width = Me.Width + My.Computer.Screen.WorkingArea.Right - (Me.Left + Me.Width)
    End Sub

    Private Sub PictureBox2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox2.Click
        apps.TopLevel = False
        shellbackground.Panel1.Controls.Add(apps)
        apps.Show()
    End Sub

    Private Sub PictureBox1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox1.Click
        userinfo.StartPosition = FormStartPosition.Manual
        userinfo.TopLevel = False
        shellbackground.Panel1.Controls.Add(userinfo)
        userinfo.Top = (Screen.PrimaryScreen.Bounds.Height - userinfo.Height) / 2
        userinfo.Left = (Screen.PrimaryScreen.Bounds.Width + userinfo.Width) / 2
        userinfo.Show()
    End Sub
End Class