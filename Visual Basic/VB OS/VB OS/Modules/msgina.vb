﻿Public Class msgina
    Private allowCoolMove As Boolean = False
    Private myCoolPoint As New Point
    Private Sub PictureBox2_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Panel1.MouseDown
        If e.Button = MouseButtons.Left Then
            allowCoolMove = True
            myCoolPoint = New Point(e.X, e.Y)
        End If
    End Sub
    Private Sub Panel1_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Panel1.MouseMove
        If e.Button = MouseButtons.Left Then
            If allowCoolMove = True Then
                Me.Location = New Point(Me.Location.X + e.X - myCoolPoint.X, Me.Location.Y + e.Y - myCoolPoint.Y)
            End If
        End If
    End Sub
    Private Sub Panel1_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Panel1.MouseUp
        If e.Button = MouseButtons.Left Then
            allowCoolMove = False
        End If
    End Sub
    Private Sub Panel1_Maximize(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Panel1.MouseDoubleClick
        If e.Button = MouseButtons.Left Then
            Me.WindowState = FormWindowState.Maximized
            If Me.WindowState = FormWindowState.Maximized = True Then
                Me.WindowState = FormWindowState.Normal
            End If
        End If
    End Sub
    'Begin app calls
    Private Sub msgina_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        TextBox1.Focus()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        taskbar.TopLevel = False
        shellbackground.Panel1.Controls.Add(taskbar)
        taskbar.Show()
        userinfo.Label1.Text = "Current User: " & TextBox1.Text.ToString
        Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub
End Class