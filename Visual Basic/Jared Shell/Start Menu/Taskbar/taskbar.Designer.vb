﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class taskbar
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(taskbar))
        Me.classicstartmenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.ProgramsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.programsubmenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.DocumentsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.documentsubmenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.SettingToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ControlPanelToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.NetworkConnectionsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrintersToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TaskbarAndStartMenuToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SearchToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpAndSupportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RunToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.LogToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ShutdownToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.DocumentsImages = New System.Windows.Forms.ImageList(Me.components)
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.startbuttonmenuXP = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.OpenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExploreToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SearchToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.PropertiesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator()
        Me.OpenAllUsersToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExploreAllUsersToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.currenttime = New System.Windows.Forms.Label()
        Me.timeupdate = New System.Windows.Forms.Timer(Me.components)
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.audiomenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.OpenVolumeMixerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AdjustAudioPropertiesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.PictureBox8 = New System.Windows.Forms.PictureBox()
        Me.safelyremovehardware = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.startbuttonmenu7 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.PropertiesToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.OpenExplorerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.programslist = New System.Windows.Forms.ImageList(Me.components)
        Me.taskbarright = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.StartMenuToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StartMenuToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClassicStartMenuToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator8 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator()
        Me.StartTaskManagerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator()
        Me.LockTheTaskbarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PropertiesToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImageList2 = New System.Windows.Forms.ImageList(Me.components)
        Me.PictureBox9 = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ListView1 = New System.Windows.Forms.ListView()
        Me.programspics = New System.Windows.Forms.ImageList(Me.components)
        Me.PictureBox10 = New System.Windows.Forms.PictureBox()
        Me.networkmenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.DisableToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatusToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RepairToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator9 = New System.Windows.Forms.ToolStripSeparator()
        Me.ChangeFirewallSettingsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator10 = New System.Windows.Forms.ToolStripSeparator()
        Me.TheToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.networkIcons = New System.Windows.Forms.ImageList(Me.components)
        Me.PictureBox11 = New System.Windows.Forms.PictureBox()
        Me.othertaskbaritems = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ProgramUpdatesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.classicstartmenu.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.startbuttonmenuXP.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.audiomenu.SuspendLayout()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.startbuttonmenu7.SuspendLayout()
        Me.taskbarright.SuspendLayout()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.networkmenu.SuspendLayout()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.othertaskbaritems.SuspendLayout()
        Me.SuspendLayout()
        '
        'classicstartmenu
        '
        Me.classicstartmenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripSeparator3, Me.ProgramsToolStripMenuItem, Me.DocumentsToolStripMenuItem, Me.SettingToolStripMenuItem, Me.SearchToolStripMenuItem, Me.HelpAndSupportToolStripMenuItem, Me.RunToolStripMenuItem, Me.ToolStripSeparator1, Me.LogToolStripMenuItem, Me.ShutdownToolStripMenuItem})
        Me.classicstartmenu.Name = "startmenu"
        Me.classicstartmenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.classicstartmenu.Size = New System.Drawing.Size(176, 256)
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(172, 6)
        '
        'ProgramsToolStripMenuItem
        '
        Me.ProgramsToolStripMenuItem.DropDown = Me.programsubmenu
        Me.ProgramsToolStripMenuItem.Image = CType(resources.GetObject("ProgramsToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ProgramsToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ProgramsToolStripMenuItem.Name = "ProgramsToolStripMenuItem"
        Me.ProgramsToolStripMenuItem.Size = New System.Drawing.Size(175, 30)
        Me.ProgramsToolStripMenuItem.Text = "Programs"
        '
        'programsubmenu
        '
        Me.programsubmenu.Name = "programsubmenu"
        Me.programsubmenu.OwnerItem = Me.ProgramsToolStripMenuItem
        Me.programsubmenu.Size = New System.Drawing.Size(61, 4)
        '
        'DocumentsToolStripMenuItem
        '
        Me.DocumentsToolStripMenuItem.DropDown = Me.documentsubmenu
        Me.DocumentsToolStripMenuItem.Image = CType(resources.GetObject("DocumentsToolStripMenuItem.Image"), System.Drawing.Image)
        Me.DocumentsToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.DocumentsToolStripMenuItem.Name = "DocumentsToolStripMenuItem"
        Me.DocumentsToolStripMenuItem.Size = New System.Drawing.Size(175, 30)
        Me.DocumentsToolStripMenuItem.Text = "Documents"
        '
        'documentsubmenu
        '
        Me.documentsubmenu.Name = "ContextMenuStrip1"
        Me.documentsubmenu.OwnerItem = Me.DocumentsToolStripMenuItem
        Me.documentsubmenu.Size = New System.Drawing.Size(61, 4)
        '
        'SettingToolStripMenuItem
        '
        Me.SettingToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ControlPanelToolStripMenuItem, Me.ToolStripSeparator2, Me.NetworkConnectionsToolStripMenuItem, Me.PrintersToolStripMenuItem, Me.TaskbarAndStartMenuToolStripMenuItem})
        Me.SettingToolStripMenuItem.Image = CType(resources.GetObject("SettingToolStripMenuItem.Image"), System.Drawing.Image)
        Me.SettingToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.SettingToolStripMenuItem.Name = "SettingToolStripMenuItem"
        Me.SettingToolStripMenuItem.Size = New System.Drawing.Size(175, 30)
        Me.SettingToolStripMenuItem.Text = "Settings"
        '
        'ControlPanelToolStripMenuItem
        '
        Me.ControlPanelToolStripMenuItem.Image = CType(resources.GetObject("ControlPanelToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ControlPanelToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ControlPanelToolStripMenuItem.Name = "ControlPanelToolStripMenuItem"
        Me.ControlPanelToolStripMenuItem.Size = New System.Drawing.Size(199, 22)
        Me.ControlPanelToolStripMenuItem.Text = "Control Panel"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(196, 6)
        '
        'NetworkConnectionsToolStripMenuItem
        '
        Me.NetworkConnectionsToolStripMenuItem.Image = CType(resources.GetObject("NetworkConnectionsToolStripMenuItem.Image"), System.Drawing.Image)
        Me.NetworkConnectionsToolStripMenuItem.Name = "NetworkConnectionsToolStripMenuItem"
        Me.NetworkConnectionsToolStripMenuItem.Size = New System.Drawing.Size(199, 22)
        Me.NetworkConnectionsToolStripMenuItem.Text = "Network Connections"
        '
        'PrintersToolStripMenuItem
        '
        Me.PrintersToolStripMenuItem.Image = CType(resources.GetObject("PrintersToolStripMenuItem.Image"), System.Drawing.Image)
        Me.PrintersToolStripMenuItem.Name = "PrintersToolStripMenuItem"
        Me.PrintersToolStripMenuItem.Size = New System.Drawing.Size(199, 22)
        Me.PrintersToolStripMenuItem.Text = "Printers"
        '
        'TaskbarAndStartMenuToolStripMenuItem
        '
        Me.TaskbarAndStartMenuToolStripMenuItem.Image = CType(resources.GetObject("TaskbarAndStartMenuToolStripMenuItem.Image"), System.Drawing.Image)
        Me.TaskbarAndStartMenuToolStripMenuItem.Name = "TaskbarAndStartMenuToolStripMenuItem"
        Me.TaskbarAndStartMenuToolStripMenuItem.Size = New System.Drawing.Size(199, 22)
        Me.TaskbarAndStartMenuToolStripMenuItem.Text = "Taskbar and Start Menu"
        '
        'SearchToolStripMenuItem
        '
        Me.SearchToolStripMenuItem.Image = CType(resources.GetObject("SearchToolStripMenuItem.Image"), System.Drawing.Image)
        Me.SearchToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.SearchToolStripMenuItem.Name = "SearchToolStripMenuItem"
        Me.SearchToolStripMenuItem.Size = New System.Drawing.Size(175, 30)
        Me.SearchToolStripMenuItem.Text = "Search"
        '
        'HelpAndSupportToolStripMenuItem
        '
        Me.HelpAndSupportToolStripMenuItem.Image = CType(resources.GetObject("HelpAndSupportToolStripMenuItem.Image"), System.Drawing.Image)
        Me.HelpAndSupportToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.HelpAndSupportToolStripMenuItem.Name = "HelpAndSupportToolStripMenuItem"
        Me.HelpAndSupportToolStripMenuItem.Size = New System.Drawing.Size(175, 30)
        Me.HelpAndSupportToolStripMenuItem.Text = "Help and Support"
        '
        'RunToolStripMenuItem
        '
        Me.RunToolStripMenuItem.Image = CType(resources.GetObject("RunToolStripMenuItem.Image"), System.Drawing.Image)
        Me.RunToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.RunToolStripMenuItem.Name = "RunToolStripMenuItem"
        Me.RunToolStripMenuItem.Size = New System.Drawing.Size(175, 30)
        Me.RunToolStripMenuItem.Text = "Run..."
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(172, 6)
        '
        'LogToolStripMenuItem
        '
        Me.LogToolStripMenuItem.Image = CType(resources.GetObject("LogToolStripMenuItem.Image"), System.Drawing.Image)
        Me.LogToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.LogToolStripMenuItem.Name = "LogToolStripMenuItem"
        Me.LogToolStripMenuItem.Size = New System.Drawing.Size(175, 30)
        Me.LogToolStripMenuItem.Text = "Log Off User..."
        '
        'ShutdownToolStripMenuItem
        '
        Me.ShutdownToolStripMenuItem.Image = CType(resources.GetObject("ShutdownToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ShutdownToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ShutdownToolStripMenuItem.Name = "ShutdownToolStripMenuItem"
        Me.ShutdownToolStripMenuItem.Size = New System.Drawing.Size(175, 30)
        Me.ShutdownToolStripMenuItem.Text = "Shut Down"
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "gnome-session-logout.png")
        '
        'DocumentsImages
        '
        Me.DocumentsImages.ImageStream = CType(resources.GetObject("DocumentsImages.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.DocumentsImages.TransparentColor = System.Drawing.Color.Transparent
        Me.DocumentsImages.Images.SetKeyName(0, "folder 16.png")
        '
        'PictureBox1
        '
        Me.PictureBox1.ContextMenuStrip = Me.startbuttonmenuXP
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(2, 3)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(54, 22)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox1.TabIndex = 2
        Me.PictureBox1.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox1, "Click here to begin")
        '
        'startbuttonmenuXP
        '
        Me.startbuttonmenuXP.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OpenToolStripMenuItem, Me.ExploreToolStripMenuItem, Me.SearchToolStripMenuItem1, Me.ToolStripSeparator4, Me.PropertiesToolStripMenuItem, Me.ToolStripSeparator5, Me.OpenAllUsersToolStripMenuItem, Me.ExploreAllUsersToolStripMenuItem})
        Me.startbuttonmenuXP.Name = "startbuttonmenuXP"
        Me.startbuttonmenuXP.Size = New System.Drawing.Size(161, 148)
        '
        'OpenToolStripMenuItem
        '
        Me.OpenToolStripMenuItem.Name = "OpenToolStripMenuItem"
        Me.OpenToolStripMenuItem.Size = New System.Drawing.Size(160, 22)
        Me.OpenToolStripMenuItem.Text = "Open"
        '
        'ExploreToolStripMenuItem
        '
        Me.ExploreToolStripMenuItem.Name = "ExploreToolStripMenuItem"
        Me.ExploreToolStripMenuItem.Size = New System.Drawing.Size(160, 22)
        Me.ExploreToolStripMenuItem.Text = "Explore"
        '
        'SearchToolStripMenuItem1
        '
        Me.SearchToolStripMenuItem1.Name = "SearchToolStripMenuItem1"
        Me.SearchToolStripMenuItem1.Size = New System.Drawing.Size(160, 22)
        Me.SearchToolStripMenuItem1.Text = "Search"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(157, 6)
        '
        'PropertiesToolStripMenuItem
        '
        Me.PropertiesToolStripMenuItem.Name = "PropertiesToolStripMenuItem"
        Me.PropertiesToolStripMenuItem.Size = New System.Drawing.Size(160, 22)
        Me.PropertiesToolStripMenuItem.Text = "Properties"
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(157, 6)
        '
        'OpenAllUsersToolStripMenuItem
        '
        Me.OpenAllUsersToolStripMenuItem.Name = "OpenAllUsersToolStripMenuItem"
        Me.OpenAllUsersToolStripMenuItem.Size = New System.Drawing.Size(160, 22)
        Me.OpenAllUsersToolStripMenuItem.Text = "Open All Users"
        '
        'ExploreAllUsersToolStripMenuItem
        '
        Me.ExploreAllUsersToolStripMenuItem.Name = "ExploreAllUsersToolStripMenuItem"
        Me.ExploreAllUsersToolStripMenuItem.Size = New System.Drawing.Size(160, 22)
        Me.ExploreAllUsersToolStripMenuItem.Text = "Explore All Users"
        '
        'PictureBox2
        '
        Me.PictureBox2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(62, 4)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(3, 20)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 3
        Me.PictureBox2.TabStop = False
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(72, 6)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(16, 16)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox3.TabIndex = 4
        Me.PictureBox3.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox3, "Show Desktop")
        '
        'PictureBox4
        '
        Me.PictureBox4.Image = CType(resources.GetObject("PictureBox4.Image"), System.Drawing.Image)
        Me.PictureBox4.Location = New System.Drawing.Point(95, 6)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(16, 16)
        Me.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox4.TabIndex = 5
        Me.PictureBox4.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox4, "Launch Web Browser")
        '
        'PictureBox5
        '
        Me.PictureBox5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.PictureBox5.Image = CType(resources.GetObject("PictureBox5.Image"), System.Drawing.Image)
        Me.PictureBox5.Location = New System.Drawing.Point(116, 4)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(3, 20)
        Me.PictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox5.TabIndex = 6
        Me.PictureBox5.TabStop = False
        '
        'PictureBox6
        '
        Me.PictureBox6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox6.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox6.Image = CType(resources.GetObject("PictureBox6.Image"), System.Drawing.Image)
        Me.PictureBox6.Location = New System.Drawing.Point(585, 3)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(161, 22)
        Me.PictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox6.TabIndex = 7
        Me.PictureBox6.TabStop = False
        '
        'currenttime
        '
        Me.currenttime.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.currenttime.AutoSize = True
        Me.currenttime.BackColor = System.Drawing.Color.Transparent
        Me.currenttime.Location = New System.Drawing.Point(690, 8)
        Me.currenttime.Name = "currenttime"
        Me.currenttime.Size = New System.Drawing.Size(39, 13)
        Me.currenttime.TabIndex = 8
        Me.currenttime.Text = "Label1"
        Me.ToolTip1.SetToolTip(Me.currenttime, "Time")
        '
        'timeupdate
        '
        Me.timeupdate.Enabled = True
        Me.timeupdate.Interval = 999
        '
        'PictureBox7
        '
        Me.PictureBox7.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.PictureBox7.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox7.ContextMenuStrip = Me.audiomenu
        Me.PictureBox7.Image = CType(resources.GetObject("PictureBox7.Image"), System.Drawing.Image)
        Me.PictureBox7.Location = New System.Drawing.Point(665, 6)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(16, 16)
        Me.PictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox7.TabIndex = 9
        Me.PictureBox7.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox7, "Volume")
        '
        'audiomenu
        '
        Me.audiomenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OpenVolumeMixerToolStripMenuItem, Me.AdjustAudioPropertiesToolStripMenuItem})
        Me.audiomenu.Name = "audiomenu"
        Me.audiomenu.ShowImageMargin = False
        Me.audiomenu.Size = New System.Drawing.Size(175, 48)
        '
        'OpenVolumeMixerToolStripMenuItem
        '
        Me.OpenVolumeMixerToolStripMenuItem.Name = "OpenVolumeMixerToolStripMenuItem"
        Me.OpenVolumeMixerToolStripMenuItem.Size = New System.Drawing.Size(174, 22)
        Me.OpenVolumeMixerToolStripMenuItem.Text = "Open Volume Mixer"
        '
        'AdjustAudioPropertiesToolStripMenuItem
        '
        Me.AdjustAudioPropertiesToolStripMenuItem.Name = "AdjustAudioPropertiesToolStripMenuItem"
        Me.AdjustAudioPropertiesToolStripMenuItem.Size = New System.Drawing.Size(174, 22)
        Me.AdjustAudioPropertiesToolStripMenuItem.Text = "Adjust Audio Properties"
        '
        'PictureBox8
        '
        Me.PictureBox8.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.PictureBox8.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox8.ContextMenuStrip = Me.safelyremovehardware
        Me.PictureBox8.Image = CType(resources.GetObject("PictureBox8.Image"), System.Drawing.Image)
        Me.PictureBox8.Location = New System.Drawing.Point(621, 6)
        Me.PictureBox8.Name = "PictureBox8"
        Me.PictureBox8.Size = New System.Drawing.Size(16, 16)
        Me.PictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox8.TabIndex = 10
        Me.PictureBox8.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox8, "Safely Remove Hardware")
        Me.PictureBox8.Visible = False
        '
        'safelyremovehardware
        '
        Me.safelyremovehardware.Name = "safelyremovehardware"
        Me.safelyremovehardware.Size = New System.Drawing.Size(61, 4)
        '
        'startbuttonmenu7
        '
        Me.startbuttonmenu7.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PropertiesToolStripMenuItem1, Me.OpenExplorerToolStripMenuItem})
        Me.startbuttonmenu7.Name = "startbuttonmenu7"
        Me.startbuttonmenu7.Size = New System.Drawing.Size(149, 48)
        '
        'PropertiesToolStripMenuItem1
        '
        Me.PropertiesToolStripMenuItem1.Name = "PropertiesToolStripMenuItem1"
        Me.PropertiesToolStripMenuItem1.Size = New System.Drawing.Size(148, 22)
        Me.PropertiesToolStripMenuItem1.Text = "Properties"
        '
        'OpenExplorerToolStripMenuItem
        '
        Me.OpenExplorerToolStripMenuItem.Name = "OpenExplorerToolStripMenuItem"
        Me.OpenExplorerToolStripMenuItem.Size = New System.Drawing.Size(148, 22)
        Me.OpenExplorerToolStripMenuItem.Text = "Open Explorer"
        '
        'programslist
        '
        Me.programslist.ImageStream = CType(resources.GetObject("programslist.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.programslist.TransparentColor = System.Drawing.Color.Transparent
        Me.programslist.Images.SetKeyName(0, "folder 16.png")
        '
        'taskbarright
        '
        Me.taskbarright.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.StartMenuToolStripMenuItem, Me.ToolStripSeparator8, Me.ToolStripMenuItem1, Me.ToolStripSeparator6, Me.StartTaskManagerToolStripMenuItem, Me.ToolStripSeparator7, Me.LockTheTaskbarToolStripMenuItem, Me.PropertiesToolStripMenuItem2})
        Me.taskbarright.Name = "taskbarright"
        Me.taskbarright.Size = New System.Drawing.Size(170, 132)
        '
        'StartMenuToolStripMenuItem
        '
        Me.StartMenuToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.StartMenuToolStripMenuItem1, Me.ClassicStartMenuToolStripMenuItem})
        Me.StartMenuToolStripMenuItem.Name = "StartMenuToolStripMenuItem"
        Me.StartMenuToolStripMenuItem.Size = New System.Drawing.Size(169, 22)
        Me.StartMenuToolStripMenuItem.Text = "Start Menu"
        '
        'StartMenuToolStripMenuItem1
        '
        Me.StartMenuToolStripMenuItem1.Checked = True
        Me.StartMenuToolStripMenuItem1.CheckOnClick = True
        Me.StartMenuToolStripMenuItem1.CheckState = System.Windows.Forms.CheckState.Checked
        Me.StartMenuToolStripMenuItem1.Name = "StartMenuToolStripMenuItem1"
        Me.StartMenuToolStripMenuItem1.Size = New System.Drawing.Size(171, 22)
        Me.StartMenuToolStripMenuItem1.Text = "Start menu"
        '
        'ClassicStartMenuToolStripMenuItem
        '
        Me.ClassicStartMenuToolStripMenuItem.CheckOnClick = True
        Me.ClassicStartMenuToolStripMenuItem.Name = "ClassicStartMenuToolStripMenuItem"
        Me.ClassicStartMenuToolStripMenuItem.Size = New System.Drawing.Size(171, 22)
        Me.ClassicStartMenuToolStripMenuItem.Text = "Classic Start menu"
        '
        'ToolStripSeparator8
        '
        Me.ToolStripSeparator8.Name = "ToolStripSeparator8"
        Me.ToolStripSeparator8.Size = New System.Drawing.Size(166, 6)
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(169, 22)
        Me.ToolStripMenuItem1.Text = "Show the Desktop"
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(166, 6)
        '
        'StartTaskManagerToolStripMenuItem
        '
        Me.StartTaskManagerToolStripMenuItem.Name = "StartTaskManagerToolStripMenuItem"
        Me.StartTaskManagerToolStripMenuItem.Size = New System.Drawing.Size(169, 22)
        Me.StartTaskManagerToolStripMenuItem.Text = "Task Manager"
        '
        'ToolStripSeparator7
        '
        Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
        Me.ToolStripSeparator7.Size = New System.Drawing.Size(166, 6)
        '
        'LockTheTaskbarToolStripMenuItem
        '
        Me.LockTheTaskbarToolStripMenuItem.Checked = True
        Me.LockTheTaskbarToolStripMenuItem.CheckOnClick = True
        Me.LockTheTaskbarToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.LockTheTaskbarToolStripMenuItem.Name = "LockTheTaskbarToolStripMenuItem"
        Me.LockTheTaskbarToolStripMenuItem.Size = New System.Drawing.Size(169, 22)
        Me.LockTheTaskbarToolStripMenuItem.Text = "Lock the taskbar"
        '
        'PropertiesToolStripMenuItem2
        '
        Me.PropertiesToolStripMenuItem2.Name = "PropertiesToolStripMenuItem2"
        Me.PropertiesToolStripMenuItem2.Size = New System.Drawing.Size(169, 22)
        Me.PropertiesToolStripMenuItem2.Text = "Properties"
        '
        'ImageList2
        '
        Me.ImageList2.ImageStream = CType(resources.GetObject("ImageList2.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList2.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList2.Images.SetKeyName(0, "start-here.png")
        '
        'PictureBox9
        '
        Me.PictureBox9.Image = CType(resources.GetObject("PictureBox9.Image"), System.Drawing.Image)
        Me.PictureBox9.Location = New System.Drawing.Point(5, 6)
        Me.PictureBox9.Name = "PictureBox9"
        Me.PictureBox9.Size = New System.Drawing.Size(16, 16)
        Me.PictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox9.TabIndex = 11
        Me.PictureBox9.TabStop = False
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.BackColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(0, 1)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(753, 1)
        Me.Label1.TabIndex = 12
        '
        'ListView1
        '
        Me.ListView1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ListView1.BackColor = System.Drawing.SystemColors.Control
        Me.ListView1.BackgroundImage = CType(resources.GetObject("ListView1.BackgroundImage"), System.Drawing.Image)
        Me.ListView1.BackgroundImageTiled = True
        Me.ListView1.ForeColor = System.Drawing.Color.Black
        Me.ListView1.Location = New System.Drawing.Point(125, 3)
        Me.ListView1.Name = "ListView1"
        Me.ListView1.Size = New System.Drawing.Size(454, 22)
        Me.ListView1.SmallImageList = Me.programspics
        Me.ListView1.TabIndex = 13
        Me.ListView1.UseCompatibleStateImageBehavior = False
        Me.ListView1.View = System.Windows.Forms.View.List
        '
        'programspics
        '
        Me.programspics.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit
        Me.programspics.ImageSize = New System.Drawing.Size(16, 16)
        Me.programspics.TransparentColor = System.Drawing.Color.Transparent
        '
        'PictureBox10
        '
        Me.PictureBox10.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.PictureBox10.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox10.ContextMenuStrip = Me.networkmenu
        Me.PictureBox10.Image = CType(resources.GetObject("PictureBox10.Image"), System.Drawing.Image)
        Me.PictureBox10.Location = New System.Drawing.Point(643, 6)
        Me.PictureBox10.Name = "PictureBox10"
        Me.PictureBox10.Size = New System.Drawing.Size(16, 16)
        Me.PictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox10.TabIndex = 14
        Me.PictureBox10.TabStop = False
        '
        'networkmenu
        '
        Me.networkmenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DisableToolStripMenuItem, Me.StatusToolStripMenuItem, Me.RepairToolStripMenuItem, Me.ToolStripSeparator9, Me.ChangeFirewallSettingsToolStripMenuItem, Me.ToolStripSeparator10, Me.TheToolStripMenuItem})
        Me.networkmenu.Name = "networkmenu"
        Me.networkmenu.ShowImageMargin = False
        Me.networkmenu.Size = New System.Drawing.Size(197, 126)
        '
        'DisableToolStripMenuItem
        '
        Me.DisableToolStripMenuItem.Name = "DisableToolStripMenuItem"
        Me.DisableToolStripMenuItem.Size = New System.Drawing.Size(196, 22)
        Me.DisableToolStripMenuItem.Text = "Disable"
        '
        'StatusToolStripMenuItem
        '
        Me.StatusToolStripMenuItem.Name = "StatusToolStripMenuItem"
        Me.StatusToolStripMenuItem.Size = New System.Drawing.Size(196, 22)
        Me.StatusToolStripMenuItem.Text = "Status"
        '
        'RepairToolStripMenuItem
        '
        Me.RepairToolStripMenuItem.Name = "RepairToolStripMenuItem"
        Me.RepairToolStripMenuItem.Size = New System.Drawing.Size(196, 22)
        Me.RepairToolStripMenuItem.Text = "Repair"
        '
        'ToolStripSeparator9
        '
        Me.ToolStripSeparator9.Name = "ToolStripSeparator9"
        Me.ToolStripSeparator9.Size = New System.Drawing.Size(193, 6)
        '
        'ChangeFirewallSettingsToolStripMenuItem
        '
        Me.ChangeFirewallSettingsToolStripMenuItem.Name = "ChangeFirewallSettingsToolStripMenuItem"
        Me.ChangeFirewallSettingsToolStripMenuItem.Size = New System.Drawing.Size(196, 22)
        Me.ChangeFirewallSettingsToolStripMenuItem.Text = "Change Firewall Settings"
        '
        'ToolStripSeparator10
        '
        Me.ToolStripSeparator10.Name = "ToolStripSeparator10"
        Me.ToolStripSeparator10.Size = New System.Drawing.Size(193, 6)
        '
        'TheToolStripMenuItem
        '
        Me.TheToolStripMenuItem.Name = "TheToolStripMenuItem"
        Me.TheToolStripMenuItem.Size = New System.Drawing.Size(196, 22)
        Me.TheToolStripMenuItem.Text = "Open Network Connections"
        '
        'networkIcons
        '
        Me.networkIcons.ImageStream = CType(resources.GetObject("networkIcons.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.networkIcons.TransparentColor = System.Drawing.Color.Transparent
        Me.networkIcons.Images.SetKeyName(0, "network-offline.png")
        Me.networkIcons.Images.SetKeyName(1, "network-idle.png")
        Me.networkIcons.Images.SetKeyName(2, "network-receive.png")
        Me.networkIcons.Images.SetKeyName(3, "network-transmit.png")
        Me.networkIcons.Images.SetKeyName(4, "network-transmit-receive.png")
        Me.networkIcons.Images.SetKeyName(5, "network-error.png")
        '
        'PictureBox11
        '
        Me.PictureBox11.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.PictureBox11.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox11.ContextMenuStrip = Me.othertaskbaritems
        Me.PictureBox11.Image = CType(resources.GetObject("PictureBox11.Image"), System.Drawing.Image)
        Me.PictureBox11.Location = New System.Drawing.Point(599, 6)
        Me.PictureBox11.Name = "PictureBox11"
        Me.PictureBox11.Size = New System.Drawing.Size(16, 16)
        Me.PictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox11.TabIndex = 15
        Me.PictureBox11.TabStop = False
        '
        'othertaskbaritems
        '
        Me.othertaskbaritems.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ProgramUpdatesToolStripMenuItem})
        Me.othertaskbaritems.Name = "othertaskbaritems"
        Me.othertaskbaritems.Size = New System.Drawing.Size(167, 26)
        '
        'ProgramUpdatesToolStripMenuItem
        '
        Me.ProgramUpdatesToolStripMenuItem.Image = CType(resources.GetObject("ProgramUpdatesToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ProgramUpdatesToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ProgramUpdatesToolStripMenuItem.Name = "ProgramUpdatesToolStripMenuItem"
        Me.ProgramUpdatesToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.ProgramUpdatesToolStripMenuItem.Text = "Program Updates"
        Me.ProgramUpdatesToolStripMenuItem.Visible = False
        '
        'taskbar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(749, 28)
        Me.ContextMenuStrip = Me.taskbarright
        Me.Controls.Add(Me.PictureBox11)
        Me.Controls.Add(Me.PictureBox10)
        Me.Controls.Add(Me.ListView1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PictureBox9)
        Me.Controls.Add(Me.PictureBox8)
        Me.Controls.Add(Me.PictureBox7)
        Me.Controls.Add(Me.currenttime)
        Me.Controls.Add(Me.PictureBox6)
        Me.Controls.Add(Me.PictureBox5)
        Me.Controls.Add(Me.PictureBox4)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.PictureBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "taskbar"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Text = "taskbar"
        Me.classicstartmenu.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.startbuttonmenuXP.ResumeLayout(False)
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.audiomenu.ResumeLayout(False)
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.startbuttonmenu7.ResumeLayout(False)
        Me.taskbarright.ResumeLayout(False)
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).EndInit()
        Me.networkmenu.ResumeLayout(False)
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).EndInit()
        Me.othertaskbaritems.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents classicstartmenu As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ProgramsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DocumentsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SettingToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SearchToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HelpAndSupportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RunToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents LogToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ShutdownToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ControlPanelToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents NetworkConnectionsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrintersToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TaskbarAndStartMenuToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents documentsubmenu As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents DocumentsImages As System.Windows.Forms.ImageList
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
    Friend WithEvents currenttime As System.Windows.Forms.Label
    Friend WithEvents timeupdate As System.Windows.Forms.Timer
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents PictureBox8 As System.Windows.Forms.PictureBox
    Friend WithEvents startbuttonmenuXP As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents OpenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExploreToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SearchToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents PropertiesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents OpenAllUsersToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExploreAllUsersToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents startbuttonmenu7 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents PropertiesToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OpenExplorerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents programsubmenu As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents programslist As System.Windows.Forms.ImageList
    Friend WithEvents taskbarright As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents StartTaskManagerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents LockTheTaskbarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PropertiesToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents safelyremovehardware As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents StartMenuToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StartMenuToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClassicStartMenuToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ImageList2 As System.Windows.Forms.ImageList
    Friend WithEvents PictureBox9 As System.Windows.Forms.PictureBox
    Friend WithEvents ToolStripSeparator8 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ListView1 As System.Windows.Forms.ListView
    Friend WithEvents programspics As System.Windows.Forms.ImageList
    Friend WithEvents PictureBox10 As System.Windows.Forms.PictureBox
    Friend WithEvents networkIcons As System.Windows.Forms.ImageList
    Friend WithEvents audiomenu As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents OpenVolumeMixerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AdjustAudioPropertiesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents networkmenu As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents DisableToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StatusToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RepairToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator9 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ChangeFirewallSettingsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator10 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents TheToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PictureBox11 As System.Windows.Forms.PictureBox
    Friend WithEvents othertaskbaritems As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ProgramUpdatesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
