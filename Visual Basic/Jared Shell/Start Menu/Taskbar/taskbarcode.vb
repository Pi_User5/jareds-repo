﻿Imports System.IO
Module taskbarcode
    Public Sub GetRemoveableDrives()
        For Each d As System.IO.DriveInfo In My.Computer.FileSystem.Drives
            If d.DriveType = IO.DriveType.Removable Then
                taskbar.PictureBox8.Visible = True
                taskbar.safelyremovehardware.Items.Add("Safely Remove " & d.Name)
            End If
        Next
    End Sub
    Public Sub AddFavoriteLinks()
        Dim StartPath As String
        StartPath = Environment.GetFolderPath(Environment.SpecialFolder.CommonStartMenu)
        Dim di As New IO.DirectoryInfo(StartPath) 'List Files
        Dim dra As System.IO.FileInfo
        Dim counter = My.Computer.FileSystem.GetFiles(StartPath)
        If CStr(counter.Count) < "2" Then
            Dim favitem As New ToolStripMenuItem("None")
            taskbar.classicstartmenu.Items.Insert(0, favitem)
        Else
            If My.Computer.Info.OSVersion > "6.0" Then
                Dim diar1 As IO.FileInfo() = di.GetFiles("*.lnk")
                For Each dra In diar1
                    taskbar.ImageList1.Images.Add(Icon.ExtractAssociatedIcon(dra.FullName))
                    Dim favitem As New ToolStripMenuItem(Path.GetFileNameWithoutExtension(dra.ToString), taskbar.ImageList1.Images(taskbar.ImageList1.Images.Count - 1))
                    taskbar.classicstartmenu.Items.Insert(0, favitem)
                Next
            Else
                Dim diar1 As IO.FileInfo() = di.GetFiles("*.url")
                For Each dra In diar1
                    taskbar.ImageList1.Images.Add(Icon.ExtractAssociatedIcon(dra.FullName))
                    Dim favitem As New ToolStripMenuItem(Path.GetFileNameWithoutExtension(dra.ToString), taskbar.ImageList1.Images(taskbar.ImageList1.Images.Count - 1))
                    taskbar.classicstartmenu.Items.Insert(0, favitem)
                Next
            End If
        End If
    End Sub
    Public Sub DocumentsFolder()
        Dim DocsPath As String
        DocsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
        If Directory.Exists(DocsPath) Then
            For Each dirname As String In IO.Directory.GetDirectories(DocsPath)
                Dim files As List(Of String) = New List(Of String) 'Create New list
                files.Add(IO.Path.GetFileName(IO.Path.GetFileName(dirname)).ToString) 'Add folders to list
                files.Sort() 'Sort folders A-Z
                For Each element As String In files 'Loop through folders
                Dim lvi As New ToolStripMenuItem(IO.Path.GetFileName(dirname), taskbar.DocumentsImages.Images.Item(0))
                    taskbar.documentsubmenu.Items.Insert(0, lvi)
                Next
            Next
            Dim di As New IO.DirectoryInfo(DocsPath) 'List Files
            Dim diar1 As IO.FileInfo() = di.GetFiles()
            Dim dra As System.IO.FileInfo
            For Each dra In diar1
                Dim files As List(Of String) = New List(Of String) 'Create New list
                files.Add(IO.Path.GetFileName(dra.ToString).ToString) 'Add folders to list
                files.Sort() 'Sort folders A-Z
                For Each element As String In files 'Loop through folders
                    browser.shellimages16.Images.Add(Icon.ExtractAssociatedIcon(dra.FullName))
                    browser.shellimages32.Images.Add(Icon.ExtractAssociatedIcon(dra.FullName))
                    browser.ListView1.Items.Add(dra.ToString, browser.shellimages32.Images.Count - 1)
                Next
            Next
        End If
    End Sub
    Public Sub ProgramsFolder()
        Dim CommonStart As String
        Dim Menu As String
        CommonStart = Environment.GetFolderPath(Environment.SpecialFolder.CommonStartMenu) & "\Programs"
        Menu = Environment.GetFolderPath(Environment.SpecialFolder.StartMenu) & "\Programs"
        'MsgBox(Menu)
        If Directory.Exists(CommonStart) Then
            For Each dirname As String In IO.Directory.GetDirectories(CommonStart)
                'Dim lvi As New ToolStripMenuItem(IO.Path.GetFileName(dirname), geticons.ImageList2.Images(0))
                Dim lvi As New ToolStripMenuItem(IO.Path.GetFileName(dirname), taskbar.DocumentsImages.Images.Item(0))
                taskbar.programsubmenu.Items.Insert(0, lvi)
                startmenu.programmenu.Items.Insert(0, lvi)
            Next
            Dim di As New IO.DirectoryInfo(CommonStart) 'List Files
            If My.Computer.Info.OSVersion > "6.0" Then
                Dim diar1 As IO.FileInfo() = di.GetFiles("*.lnk")
                Dim dra As System.IO.FileInfo
                For Each dra In diar1
                    taskbar.programslist.Images.Add(Icon.ExtractAssociatedIcon(dra.FullName))
                    Dim prog As New ToolStripMenuItem(Path.GetFileNameWithoutExtension(dra.ToString), taskbar.programslist.Images(taskbar.programslist.Images.Count - 1))
                    taskbar.programsubmenu.Items.Add(prog)
                    startmenu.programmenu.Items.Add(prog)
                Next
            End If
            If My.Computer.Info.OSVersion < "6.0" Then ' Windows XP and like
                Dim diar1 As IO.FileInfo() = di.GetFiles("*.url")
                Dim dra As System.IO.FileInfo
                For Each dra In diar1
                    taskbar.programslist.Images.Add(Icon.ExtractAssociatedIcon(dra.FullName))
                    Dim prog As New ToolStripMenuItem(Path.GetFileNameWithoutExtension(dra.ToString), taskbar.programslist.Images(taskbar.programslist.Images.Count - 1))
                    taskbar.programsubmenu.Items.Add(prog)
                    startmenu.programmenu.Items.Add(prog)
                Next
            End If
        End If
        If Directory.Exists(Menu) Then
            For Each dirname As String In IO.Directory.GetDirectories(Menu)
                'Dim lvi As New ToolStripMenuItem(IO.Path.GetFileName(dirname), geticons.ImageList2.Images(0))
                Dim lvi As New ToolStripMenuItem(IO.Path.GetFileName(dirname), taskbar.DocumentsImages.Images.Item(0))
                taskbar.programsubmenu.Items.Insert(0, lvi)
                startmenu.programmenu.Items.Insert(0, lvi)
            Next
            Dim di As New IO.DirectoryInfo(Menu) 'List Files
            If My.Computer.Info.OSVersion > "6.0" Then
                Dim diar1 As IO.FileInfo() = di.GetFiles("*.lnk")
                Dim dra As System.IO.FileInfo
                For Each dra In diar1
                    taskbar.programslist.Images.Add(Icon.ExtractAssociatedIcon(dra.FullName))
                    Dim prog As New ToolStripMenuItem(Path.GetFileNameWithoutExtension(dra.ToString), taskbar.programslist.Images(taskbar.programslist.Images.Count - 1))
                    taskbar.programsubmenu.Items.Add(prog)
                    startmenu.programmenu.Items.Add(prog)
                Next
            End If
            If My.Computer.Info.OSVersion < "6.0" Then ' Windows XP and like
                Dim diar1 As IO.FileInfo() = di.GetFiles("*.url")
                Dim dra As System.IO.FileInfo
                For Each dra In diar1
                    taskbar.programslist.Images.Add(Icon.ExtractAssociatedIcon(dra.FullName))
                    Dim prog As New ToolStripMenuItem(Path.GetFileNameWithoutExtension(dra.ToString), taskbar.programslist.Images(taskbar.programslist.Images.Count - 1))
                    taskbar.programsubmenu.Items.Add(prog)
                    startmenu.programmenu.Items.Add(prog)
                Next
            End If
        End If
    End Sub

    Public Sub ProgramsFolder1()
        Dim CommonStart As String
        Dim Menu As String
        CommonStart = Environment.GetFolderPath(Environment.SpecialFolder.CommonStartMenu) & "\Programs"
        Menu = Environment.GetFolderPath(Environment.SpecialFolder.StartMenu) & "\Programs"
        'MsgBox(Menu)
        If Directory.Exists(CommonStart) Then
            For Each dirname As String In IO.Directory.GetDirectories(CommonStart)
                'Dim lvi As New ToolStripMenuItem(IO.Path.GetFileName(dirname), geticons.ImageList2.Images(0))
                Dim lvi As New ToolStripMenuItem(IO.Path.GetFileName(dirname), taskbar.DocumentsImages.Images.Item(0))
                taskbar.programsubmenu.Items.Insert(0, lvi)
            Next
            Dim di As New IO.DirectoryInfo(CommonStart) 'List Files
            If My.Computer.Info.OSVersion > "6.0" Then
                Dim diar1 As IO.FileInfo() = di.GetFiles("*.lnk")
                Dim dra As System.IO.FileInfo
                For Each dra In diar1
                    taskbar.programslist.Images.Add(Icon.ExtractAssociatedIcon(dra.FullName))
                    Dim prog As New ToolStripMenuItem(Path.GetFileNameWithoutExtension(dra.ToString), taskbar.programslist.Images(taskbar.programslist.Images.Count - 1))
                    taskbar.programsubmenu.Items.Add(prog)
                Next
            End If
            If My.Computer.Info.OSVersion < "6.0" Then ' Windows XP and like
                Dim diar1 As IO.FileInfo() = di.GetFiles("*.url")
                Dim dra As System.IO.FileInfo
                For Each dra In diar1
                    taskbar.programslist.Images.Add(Icon.ExtractAssociatedIcon(dra.FullName))
                    Dim prog As New ToolStripMenuItem(Path.GetFileNameWithoutExtension(dra.ToString), taskbar.programslist.Images(taskbar.programslist.Images.Count - 1))
                    taskbar.programsubmenu.Items.Add(prog)
                Next
            End If
        End If
        If Directory.Exists(Menu) Then
            For Each dirname As String In IO.Directory.GetDirectories(Menu)
                'Dim lvi As New ToolStripMenuItem(IO.Path.GetFileName(dirname), geticons.ImageList2.Images(0))
                Dim lvi As New ToolStripMenuItem(IO.Path.GetFileName(dirname), taskbar.DocumentsImages.Images.Item(0))
                taskbar.programsubmenu.Items.Insert(0, lvi)
            Next
            Dim di As New IO.DirectoryInfo(Menu) 'List Files
            If My.Computer.Info.OSVersion > "6.0" Then
                Dim diar1 As IO.FileInfo() = di.GetFiles("*.lnk")
                Dim dra As System.IO.FileInfo
                For Each dra In diar1
                    taskbar.programslist.Images.Add(Icon.ExtractAssociatedIcon(dra.FullName))
                    Dim prog As New ToolStripMenuItem(Path.GetFileNameWithoutExtension(dra.ToString), taskbar.programslist.Images(taskbar.programslist.Images.Count - 1))
                    taskbar.programsubmenu.Items.Add(prog)
                Next
            End If
            If My.Computer.Info.OSVersion < "6.0" Then ' Windows XP and like
                Dim diar1 As IO.FileInfo() = di.GetFiles("*.url")
                Dim dra As System.IO.FileInfo
                For Each dra In diar1
                    taskbar.programslist.Images.Add(Icon.ExtractAssociatedIcon(dra.FullName))
                    Dim prog As New ToolStripMenuItem(Path.GetFileNameWithoutExtension(dra.ToString), taskbar.programslist.Images(taskbar.programslist.Images.Count - 1))
                    taskbar.programsubmenu.Items.Add(prog)
                Next
            End If
        End If
    End Sub
    Public Sub LoadImages()
        'taskbar.ShutdownToolStripMenuItem.Image = geticons.ImageList1.Images(0)
        'taskbar.LogToolStripMenuItem.Image = geticons.ImageList1.Images(1)
        'taskbar.RunToolStripMenuItem.Image = geticons.ImageList1.Images(2)
        'taskbar.HelpAndSupportToolStripMenuItem.Image = geticons.ImageList1.Images(3)
        'taskbar.SearchToolStripMenuItem.Image = geticons.ImageList1.Images(4)
        'taskbar.SettingToolStripMenuItem.Image = geticons.ImageList1.Images(5)
        'taskbar.DocumentsToolStripMenuItem.Image = geticons.ImageList1.Images(6)
        'taskbar.ProgramsToolStripMenuItem.Image = geticons.ImageList1.Images(7)
        'taskbar.TaskbarAndStartMenuToolStripMenuItem.Image = geticons.ImageList2.Images(1)
        'taskbar.PrintersToolStripMenuItem.Image = geticons.ImageList2.Images(2)
        'taskbar.NetworkConnectionsToolStripMenuItem.Image = geticons.ImageList2.Images(3)
        'taskbar.ControlPanelToolStripMenuItem.Image = geticons.ImageList2.Images(4)

        'startmenu.PictureBox2.Image = geticons.ImageList1.Images(6)
        'startmenu.PictureBox3.Image = geticons.ImageList1.Images(9)
        'startmenu.PictureBox4.Image = geticons.ImageList1.Images(8)
        'startmenu.PictureBox5.Image = geticons.ImageList1.Images(5)
        'startmenu.PictureBox6.Image = geticons.ImageList1.Images(10)
        'startmenu.PictureBox7.Image = geticons.ImageList1.Images(4)
        'startmenu.PictureBox8.Image = geticons.ImageList1.Images(2)
        'startmenu.PictureBox10.Image = geticons.ImageList1.Images(0)
    End Sub
    Public Sub GetTaskbarSettings()
        taskbarproperties.CheckBox1.CheckState = My.Settings.locktaskbar
        taskbarproperties.CheckBox2.CheckState = My.Settings.autotaskbar
        taskbarproperties.CheckBox3.CheckState = My.Settings.ontoptaskbar
        taskbarproperties.CheckBox4.CheckState = My.Settings.grouptaskbar
        taskbarproperties.CheckBox5.CheckState = My.Settings.quicktaskbar
        taskbarproperties.CheckBox6.CheckState = My.Settings.showclock
        taskbarproperties.CheckBox7.CheckState = My.Settings.hideicons
    End Sub
    Public Sub SaveTaskbarSettings()
        My.Settings.locktaskbar = taskbarproperties.CheckBox1.CheckState
        My.Settings.autotaskbar = taskbarproperties.CheckBox2.CheckState
        My.Settings.ontoptaskbar = taskbarproperties.CheckBox3.CheckState
        My.Settings.grouptaskbar = taskbarproperties.CheckBox4.CheckState
        My.Settings.quicktaskbar = taskbarproperties.CheckBox5.CheckState
        My.Settings.showclock = taskbarproperties.CheckBox6.CheckState
        My.Settings.hideicons = taskbarproperties.CheckBox7.CheckState
        My.Settings.Save()
    End Sub
End Module
