﻿Public Class taskbarproperties

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)
        MyBase.Top = Screen.PrimaryScreen.WorkingArea.Bottom - Me.Height - 28
        MyBase.Left = Screen.PrimaryScreen.WorkingArea.Left + 5
        'Me.Width = Me.Width + My.Computer.Screen.WorkingArea.Right - (Me.Left + Me.Width)
    End Sub
    Private Sub taskbarproperties_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    End Sub
    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Button3.Enabled = True
    End Sub

    Private Sub TextBox2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Button3.Enabled = True
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Call taskbarcode.SaveTaskbarSettings()
        Me.Close()
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Button3.Enabled = True
    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        PictureBox3.Image = My.Resources.normal
        Button6.Enabled = True
        Button7.Enabled = False
    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
        PictureBox3.Image = My.Resources.classic
        Button7.Enabled = True
        Button6.Enabled = False
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        classicmenuproperties.Show()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        startmenuproperties.Show()
    End Sub

    Private Sub CheckBox3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox3.CheckedChanged
        If CheckBox3.Checked = True Then
            taskbar.TopMost = True
        Else
            taskbar.TopMost = False
        End If
    End Sub

    Private Sub CheckBox7_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox7.CheckedChanged
        If CheckBox7.Checked = True Then
            Button4.Enabled = True
        Else
            Button4.Enabled = False
        End If
    End Sub

    Private Sub CheckBox6_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox6.CheckedChanged
        If CheckBox6.Checked = True Then
            PictureBox2.Image = My.Resources.showclock
        Else
            PictureBox2.Image = My.Resources.noclock
        End If
    End Sub

    Private Sub OpenFileDialog1_FileOk(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles OpenFileDialog1.FileOk
        taskbar.PictureBox9.Image = System.Drawing.Bitmap.FromFile(OpenFileDialog1.FileName)
        PictureBox5.Image = System.Drawing.Bitmap.FromFile(OpenFileDialog1.FileName)
        TextBox1.Text = OpenFileDialog1.FileName
        My.Settings.StartButtonImage = OpenFileDialog1.FileName
        My.Settings.Save()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        OpenFileDialog1.ShowDialog()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click

    End Sub
    Public Sub GetButtonImage()
        Dim imagepath As String = My.Settings.StartButtonImage
        If imagepath Is Nothing Then
            
        Else 'If there is an image path entered
            If System.IO.File.Exists(imagepath) Then
                taskbar.PictureBox9.Image = System.Drawing.Bitmap.FromFile(imagepath)
                PictureBox5.Image = System.Drawing.Bitmap.FromFile(imagepath)
            End If
        End If
    End Sub
End Class