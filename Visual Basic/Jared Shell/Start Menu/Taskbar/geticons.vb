﻿Imports System.Runtime.InteropServices
Imports System.Reflection

Public Class geticons

    <DllImport("shell32.dll", EntryPoint:="ExtractAssociatedIcon")> _
    Private Shared Function ExtractAssociatedIcon(ByVal hInst As System.IntPtr, <MarshalAs(UnmanagedType.LPStr)> ByVal lpIconPath As String, ByRef lpiIcon As Integer) As System.IntPtr

    End Function
    Dim ico As Icon
    Private Sub GetIconInfo()
        Dim iconPath As String = "C:\windows\system32\shell32.dll"
        Dim hInst As IntPtr = Marshal.GetHINSTANCE([Assembly].GetExecutingAssembly().GetModules()(0))
        'Dim iIcon As Int32
        'Begin List
        Dim folder As IntPtr
        Dim computericon As IntPtr
        Dim recenticon As IntPtr
        Dim shutdownicon As IntPtr
        Dim logoficon As IntPtr
        Dim runicon As IntPtr
        Dim helpicon As IntPtr
        Dim searchicon As IntPtr
        Dim settingsicon As IntPtr
        Dim docsicon As IntPtr
        Dim programsicon As IntPtr
        Dim deskicon As IntPtr
        Dim printiconS As IntPtr
        Dim printiconL As IntPtr
        Dim neticon As IntPtr
        Dim controlicon As IntPtr
        If My.Computer.Info.OSVersion > "6.0" Then 'Windows Vista and newer
            shutdownicon = ExtractAssociatedIcon(hInst, iconPath, 215)
            ImageList1.Images.Add(Icon.FromHandle(shutdownicon))
            logoficon = ExtractAssociatedIcon(hInst, iconPath, 211)
            ImageList1.Images.Add(Icon.FromHandle(logoficon))
            runicon = ExtractAssociatedIcon(hInst, iconPath, 214)
            ImageList1.Images.Add(Icon.FromHandle(runicon))
            helpicon = ExtractAssociatedIcon(hInst, iconPath, 23)
            ImageList1.Images.Add(Icon.FromHandle(helpicon))
            searchicon = ExtractAssociatedIcon(hInst, iconPath, 22)
            ImageList1.Images.Add(Icon.FromHandle(searchicon))
            settingsicon = ExtractAssociatedIcon(hInst, iconPath, 21)
            ImageList1.Images.Add(Icon.FromHandle(settingsicon))
            docsicon = ExtractAssociatedIcon(hInst, iconPath, 126)
            ImageList1.Images.Add(Icon.FromHandle(docsicon))
            programsicon = ExtractAssociatedIcon(hInst, iconPath, 19)
            ImageList1.Images.Add(Icon.FromHandle(programsicon))
            folder = ExtractAssociatedIcon(hInst, iconPath, 3)
            ImageList2.Images.Add(Icon.FromHandle(folder))
            deskicon = ExtractAssociatedIcon(hInst, iconPath, 39)
            ImageList2.Images.Add(Icon.FromHandle(deskicon))
            printicons = ExtractAssociatedIcon(hInst, iconPath, 58)
            ImageList2.Images.Add(Icon.FromHandle(printicons))
            neticon = ExtractAssociatedIcon(hInst, iconPath, 88)
            ImageList2.Images.Add(Icon.FromHandle(neticon))
            controlicon = ExtractAssociatedIcon(hInst, iconPath, 21)
            ImageList2.Images.Add(Icon.FromHandle(controlicon))
            computericon = ExtractAssociatedIcon(hInst, iconPath, 15)
            ImageList1.Images.Add(Icon.FromHandle(computericon))
            recenticon = ExtractAssociatedIcon(hInst, iconPath, 213)
            ImageList1.Images.Add(Icon.FromHandle(recenticon))
            printiconL = ExtractAssociatedIcon(hInst, iconPath, 58)
            ImageList1.Images.Add(Icon.FromHandle(printiconL))
        End If
        If My.Computer.Info.OSVersion < "6.0" Then 'Windows XP and like
            shutdownicon = ExtractAssociatedIcon(hInst, iconPath, 215)
            ImageList1.Images.Add(Icon.FromHandle(shutdownicon))
            logoficon = ExtractAssociatedIcon(hInst, iconPath, 211)
            ImageList1.Images.Add(Icon.FromHandle(logoficon))
            runicon = ExtractAssociatedIcon(hInst, iconPath, 214)
            ImageList1.Images.Add(Icon.FromHandle(runicon))
            helpicon = ExtractAssociatedIcon(hInst, iconPath, 210)
            ImageList1.Images.Add(Icon.FromHandle(helpicon))
            searchicon = ExtractAssociatedIcon(hInst, iconPath, 209)
            ImageList1.Images.Add(Icon.FromHandle(searchicon))
            settingsicon = ExtractAssociatedIcon(hInst, iconPath, 216)
            ImageList1.Images.Add(Icon.FromHandle(settingsicon))
            docsicon = ExtractAssociatedIcon(hInst, iconPath, 126)
            ImageList1.Images.Add(Icon.FromHandle(docsicon))
            programsicon = ExtractAssociatedIcon(hInst, iconPath, 212)
            ImageList1.Images.Add(Icon.FromHandle(programsicon))
            folder = ExtractAssociatedIcon(hInst, iconPath, 3)
            ImageList2.Images.Add(Icon.FromHandle(folder))
            deskicon = ExtractAssociatedIcon(hInst, iconPath, 39)
            ImageList2.Images.Add(Icon.FromHandle(deskicon))
            printiconS = ExtractAssociatedIcon(hInst, iconPath, 58)
            ImageList2.Images.Add(Icon.FromHandle(printiconS))
            neticon = ExtractAssociatedIcon(hInst, iconPath, 88)
            ImageList2.Images.Add(Icon.FromHandle(neticon))
            controlicon = ExtractAssociatedIcon(hInst, iconPath, 216)
            ImageList2.Images.Add(Icon.FromHandle(controlicon))
            computericon = ExtractAssociatedIcon(hInst, iconPath, 15)
            ImageList1.Images.Add(Icon.FromHandle(computericon))
            recenticon = ExtractAssociatedIcon(hInst, iconPath, 213)
            ImageList1.Images.Add(Icon.FromHandle(recenticon))
            printiconL = ExtractAssociatedIcon(hInst, iconPath, 58)
            ImageList1.Images.Add(Icon.FromHandle(printiconL))
        End If
    End Sub
    Private Sub GetIcons_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles MyBase.Paint
        'e.Graphics.DrawIcon(ico, 0, 0)
    End Sub

    Private Sub geticons_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Visible = False
        GetIconInfo()
    End Sub
End Class
