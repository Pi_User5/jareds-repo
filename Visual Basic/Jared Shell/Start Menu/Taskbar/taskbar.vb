﻿Imports System.IO
Imports Microsoft.Win32
'Imports Shell32
Public Class taskbar
    Dim oshellapp As New Object
    Private Sub taskbar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call StartUp()
        'Call shellbrowser.RetrunOSVersion()
    End Sub
    Public Function getDefaultBrowser() As String
        Dim browser As String = String.Empty
        Dim key As RegistryKey = Nothing
        Try
            key = Registry.ClassesRoot.OpenSubKey("HTTP\shell\open\command", False)
            'trim off quotes
            browser = key.GetValue(Nothing).ToString().ToLower().Replace("""", "")
            If Not browser.EndsWith("exe") Then
                'get rid of everything after the ".exe"
                browser = browser.Substring(0, browser.LastIndexOf(".exe") + 4)
            End If
        Finally
            If key IsNot Nothing Then
                key.Close()
            End If
        End Try
        Return browser
    End Function
    Private Sub StartUp()
        Call shellbrowser.BeginLog()
        Call shellbrowser.PopulateTaskbar()
        Call shellbrowser.GetPrimaryNic()
        Call taskbarcode.GetTaskbarSettings()
        Call updater.CheckForUpdatesSlim()
        Call taskbarproperties.GetButtonImage()
        Call taskbarcode.AddFavoriteLinks()
        Call taskbarcode.DocumentsFolder()
        Call taskbarcode.ProgramsFolder()
        Call taskbarcode.ProgramsFolder1()
        'Call shellbrowser.PopulateSendTo()
        'desktop.Show()
        'geticons.Show()
        Me.Height = Me.Height - 10
        LogToolStripMenuItem.Text = "Log Off " & Environment.UserName & "..."
        Me.BackColor = Color.FromArgb(212, 208, 200)
        currenttime.Text = Now.ToShortTimeString
        Call GetRemoveableDrives()
        If My.Computer.Info.OSVersion > "6.0" Then 'Windows Vista and newer
            PictureBox1.ContextMenuStrip = startbuttonmenu7
        Else
            PictureBox1.ContextMenuStrip = startbuttonmenuXP
        End If
        Dim smallicon As System.Drawing.Icon
        Dim browsericon As System.Drawing.Icon
        smallicon = System.Drawing.Icon.ExtractAssociatedIcon(getDefaultBrowser)
        browsericon = New System.Drawing.Icon(smallicon, 16, 16)
        PictureBox4.Image = browsericon.ToBitmap
    End Sub
    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)
        MyBase.Top = Screen.PrimaryScreen.WorkingArea.Bottom - Me.Height
        MyBase.Left = Screen.PrimaryScreen.WorkingArea.Left
        Me.Width = Me.Width + My.Computer.Screen.WorkingArea.Right - (Me.Left + Me.Width)
    End Sub
    Private Sub PictureBox1_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles PictureBox1.MouseDown, PictureBox1.Click
        If e.Button = Windows.Forms.MouseButtons.Left Then
            PictureBox1.Image = My.Resources.start_pressed
            If StartMenuToolStripMenuItem1.Checked = True Then
                ClassicStartMenuToolStripMenuItem.Checked = False
                startmenu.Show()
            End If
            If ClassicStartMenuToolStripMenuItem.Checked = True Then
                StartMenuToolStripMenuItem1.Checked = False
                classicstartmenu.Show(Me, New System.Drawing.Point(2, 0))
            End If
        End If
    End Sub
    Private Sub PictureBox9_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles PictureBox9.MouseDown
        If e.Button = Windows.Forms.MouseButtons.Left Then
            PictureBox1.Image = My.Resources.start_pressed
            If StartMenuToolStripMenuItem1.Checked = True Then
                ClassicStartMenuToolStripMenuItem.Checked = False
                startmenu.Show()
            End If
            If ClassicStartMenuToolStripMenuItem.Checked = True Then
                StartMenuToolStripMenuItem1.Checked = False
                classicstartmenu.Show(Me, New System.Drawing.Point(2, 0))
            End If
        End If
    End Sub
    Private Sub taskbar_click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseClick
        If startmenu.Visible = True Then
            startmenu.Close()
        End If
    End Sub
    'Begin Start Functions
    Private Sub RunToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RunToolStripMenuItem.Click
        oshellapp = CreateObject("Shell.Application")
        oshellapp.FileRun()
    End Sub

    Private Sub ControlPanelToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ControlPanelToolStripMenuItem.Click
        If System.IO.File.Exists("C:\Windows\system32\control.exe") Then
            Process.Start("C:\Windows\system32\control.exe")
        End If
    End Sub

    Private Sub NetworkConnectionsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NetworkConnectionsToolStripMenuItem.Click
        Process.Start("::{7007acc7-3202-11d1-aad2-00805fc1270e}")
    End Sub

    Private Sub PrintersToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrintersToolStripMenuItem.Click
        Dim startInfo As New ProcessStartInfo("control.exe")
        startInfo.WindowStyle = ProcessWindowStyle.Minimized
        startInfo.Arguments = "printers"
        Process.Start(startInfo)
    End Sub

    Private Sub TaskbarAndStartMenuToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TaskbarAndStartMenuToolStripMenuItem.Click
        taskbarproperties.Show()
    End Sub
    Private Sub timeupdate_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles timeupdate.Tick
        currenttime.Text = Now.ToShortTimeString
        'Call GetRemoveableDrives()
    End Sub

    Private Sub currenttime_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles currenttime.MouseDoubleClick
        If My.Computer.FileSystem.FileExists("C:/Windows/system32/timedate.cpl") Then
            Process.Start("timedate.cpl")
        Else
            MsgBox("Unable to locate timedate.cpl")
        End If
    End Sub

    Private Sub PictureBox7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox7.Click
        If My.Computer.FileSystem.FileExists("C:/Windows/system32/mmsys.cpl") Then
            Process.Start("C:/Windows/system32/mmsys.cpl")
        Else
            MsgBox("Unable to locate mmsys.cpl")
        End If
    End Sub

    Private Sub PropertiesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PropertiesToolStripMenuItem.Click
        taskbarproperties.Show()
    End Sub

    Private Sub PropertiesToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PropertiesToolStripMenuItem1.Click
        taskbarproperties.Show()
    End Sub
    Private Sub OpenAllUsersToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OpenAllUsersToolStripMenuItem.Click
        Process.Start("::{450d8fba-ad25-11d0-98a8-0800361b1103}")
    End Sub

    Private Sub OpenExplorerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OpenExplorerToolStripMenuItem.Click
        Dim startfolder As String = Environment.GetFolderPath(Environment.SpecialFolder.StartMenu)
        browser.ToolStripComboBox1.Text = startfolder
        browser.Text = "Start Menu"
        browser.Icon = My.Resources.my_computer
        browser.Show()
    End Sub

    Private Sub PictureBox3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox3.Click
        Call shellbrowser.MinimizeWindows()
    End Sub

    Private Sub PropertiesToolStripMenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PropertiesToolStripMenuItem2.Click
        taskbarproperties.Show()
    End Sub

    Private Sub ShutdownToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ShutdownToolStripMenuItem.Click
        msgina.Show()
    End Sub

    Private Sub StartMenuToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StartMenuToolStripMenuItem1.Click
        Call ProgramsFolder()
        ClassicStartMenuToolStripMenuItem.Checked = False
    End Sub

    Private Sub ClassicStartMenuToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClassicStartMenuToolStripMenuItem.Click
        Call ProgramsFolder()
        StartMenuToolStripMenuItem1.Checked = False
    End Sub

    Private Sub PictureBox4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox4.Click
        If System.IO.File.Exists(getDefaultBrowser) Then
            Process.Start(getDefaultBrowser)
        End If
    End Sub

    Private Sub StartTaskManagerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StartTaskManagerToolStripMenuItem.Click
        Call shellbrowser.TaskManager()
    End Sub

    Private Sub LockTheTaskbarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LockTheTaskbarToolStripMenuItem.Click
        If LockTheTaskbarToolStripMenuItem.Checked = True Then
            Call shellbrowser.LockTaskbar()
        End If
        If LockTheTaskbarToolStripMenuItem.Checked = False Then
            Call shellbrowser.UnlockTaskbar()
        End If
    End Sub

    Private Sub ToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem1.Click
        Call shellbrowser.MinimizeWindows()
    End Sub

    Private Sub AdjustAudioPropertiesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AdjustAudioPropertiesToolStripMenuItem.Click
        If My.Computer.FileSystem.FileExists("C:/Windows/system32/mmsys.cpl") Then
            Process.Start("C:/Windows/system32/mmsys.cpl")
        Else
            MsgBox("Unable to locate mmsys.cpl")
        End If
    End Sub
    Private Sub TheToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TheToolStripMenuItem.Click
        Process.Start("::{7007acc7-3202-11d1-aad2-00805fc1270e}")
    End Sub
    Private Sub OpenToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OpenToolStripMenuItem.Click
        browser.Show()
    End Sub

    Private Sub HelpAndSupportToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles HelpAndSupportToolStripMenuItem.Click
        Process.Start("https://bitbucket.org/Pi_User5/jareds-repo/wiki/Jared%20Shell")
    End Sub

    Private Sub ProgramUpdatesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProgramUpdatesToolStripMenuItem.Click
        updater.Show()
    End Sub
End Class