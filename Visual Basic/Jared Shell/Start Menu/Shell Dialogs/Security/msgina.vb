﻿Public Class msgina
    Protected Overrides ReadOnly Property CreateParams() As CreateParams
        Get
            Dim param As CreateParams = MyBase.CreateParams
            param.ClassStyle = param.ClassStyle Or &H200
            Return param
        End Get
    End Property
    Private Sub msgina_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ComboBox1.Items.Add("Log off " & Environment.UserName)
        ComboBox1.Items.Add("Shut Down")
        ComboBox1.Items.Add("Restart")
        ComboBox1.SelectedItem = "Shut Down"
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        MsgBox("Unimplemented", MsgBoxStyle.Exclamation)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim currentoption As String = ComboBox1.SelectedItem.ToString
        Select Case currentoption
            Case ("Log off " & Environment.UserName)
                browser.Close()
                Me.Close()
                winlogon.Show()
            Case "Shut Down"
                Application.Exit()
            Case "Restart"
                Application.Exit()
                Process.Start(Application.StartupPath.ToString & "\Jared Shell.exe")
            Case Else
                MsgBox("Unknown option", MsgBoxStyle.Exclamation)
        End Select
    End Sub
End Class