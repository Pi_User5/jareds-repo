﻿Imports System.IO
Public Class folderimage

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        OpenFileDialog1.Filter = "PNG Image|*.png|BMP Image|*.bmp|JPG Image|*.jpg|All Files|*.*"
        OpenFileDialog1.ShowDialog()
    End Sub

    Private Sub OpenFileDialog1_FileOk(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles OpenFileDialog1.FileOk
        TextBox1.Text = OpenFileDialog1.FileName.ToString
        PictureBox1.Image = New System.Drawing.Bitmap(OpenFileDialog1.FileName.ToString)
        My.Settings.backgroundimage = OpenFileDialog1.FileName.ToString
        My.Settings.Save()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If TextBox1.Text = "" Then
            My.Settings.backgroundimage = ""
            My.Settings.Save()
            browser.ListView1.BackgroundImage.Dispose()
            Me.Close()
        Else
            browser.ListView1.BackgroundImage = New System.Drawing.Bitmap(OpenFileDialog1.FileName.ToString)
            Me.Close()
        End If
    End Sub

    Private Sub folderimage_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        TextBox1.Text = My.Settings.backgroundimage
        If File.Exists(TextBox1.Text) Then
            PictureBox1.Image = New System.Drawing.Bitmap(TextBox1.Text)
        End If
    End Sub
End Class