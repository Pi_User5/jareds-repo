﻿'Code from http://www.dreamincode.net/forums/topic/382410-get-list-of-toolstrip-buttons/
Option Strict On
Option Explicit On
Option Infer Off
Public Class toolbarcustomize
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Close()
    End Sub

    Private Sub toolbarcustomize_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        GetButtonsFromToolStrip()
    End Sub

    Private Sub GetButtonsFromToolStrip()
        Dim AddImages As Boolean = (ImageList1.Images.Count = 0) 'if the imagelist is empty set AddImages to True
        'iterate through all the ToolStrip`s Buttons
        For Each tsi As ToolStripItem In browser.ToolStrip1.Items
            If TypeOf tsi Is ToolStripSeparator Then
                'If the ToolStripItem is a Separator then add "Separator" as an Item (Has No Image)
                ListView2.Items.Add("Separator")
            Else
                'only if the imagelist was empty, then add the ToolStripButton`s Image to the ImgList
                'use the ToolStripButtons Text as the Key name for the Image
                If AddImages Then ImageList1.Images.Add(tsi.Text, tsi.Image)
                'add a new ListViewItem with the Text set to the ToolStripButtons Text. Use the Text to set the
                'Image from the ImgList with the same Key name.
                ListView2.Items.Add(tsi.Text, tsi.Text)
            End If
        Next
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Process.Start("https://bitbucket.org/Pi_User5/jareds-repo/wiki/Customize%20Toolbar")
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If ListView1.SelectedItems.Count = 1 Then
            Dim item As String = ListView1.FocusedItem.Text
            'Dim buttonimage As Bitmap =
            Select Case item
                Case "Separator"
                    Dim sep As New ToolStripSeparator
                    browser.ToolStrip1.Items.Add(sep)
                Case Else
                    Dim button As New ToolStripButton
                    button.Text = item
                    'button.Image = 
                    button.DisplayStyle = ToolStripItemDisplayStyle.Image
                    browser.ToolStrip1.Items.Add(button)
            End Select
            ListView2.Items.Add(ListView1.FocusedItem.Text)
        Else
            Button1.Enabled = False
        End If
    End Sub

    Private Sub ListView1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListView1.SelectedIndexChanged
        If ListView1.SelectedItems.Count = 1 Then
            Button1.Enabled = True
        Else
            Button1.Enabled = False
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If ListView1.SelectedItems.Count = 1 Then
            ListView2.Items.Remove(ListView2.FocusedItem)
        Else
            Button1.Enabled = False
        End If
    End Sub

    Private Sub ListView2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListView2.SelectedIndexChanged
        If ListView2.SelectedItems.Count = 1 Then
            Button2.Enabled = True
        Else
            Button2.Enabled = False
        End If
    End Sub
End Class