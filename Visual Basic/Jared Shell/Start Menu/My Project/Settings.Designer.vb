﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:4.0.30319.34014
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Namespace My
    
    <Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute(),  _
     Global.System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "10.0.0.0"),  _
     Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
    Partial Friend NotInheritable Class MySettings
        Inherits Global.System.Configuration.ApplicationSettingsBase
        
        Private Shared defaultInstance As MySettings = CType(Global.System.Configuration.ApplicationSettingsBase.Synchronized(New MySettings()),MySettings)
        
#Region "My.Settings Auto-Save Functionality"
#If _MyType = "WindowsForms" Then
    Private Shared addedHandler As Boolean

    Private Shared addedHandlerLockObject As New Object

    <Global.System.Diagnostics.DebuggerNonUserCodeAttribute(), Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)> _
    Private Shared Sub AutoSaveSettings(ByVal sender As Global.System.Object, ByVal e As Global.System.EventArgs)
        If My.Application.SaveMySettingsOnExit Then
            My.Settings.Save()
        End If
    End Sub
#End If
#End Region
        
        Public Shared ReadOnly Property [Default]() As MySettings
            Get
                
#If _MyType = "WindowsForms" Then
               If Not addedHandler Then
                    SyncLock addedHandlerLockObject
                        If Not addedHandler Then
                            AddHandler My.Application.Shutdown, AddressOf AutoSaveSettings
                            addedHandler = True
                        End If
                    End SyncLock
                End If
#End If
                Return defaultInstance
            End Get
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("")>  _
        Public Property UnpressedImage() As String
            Get
                Return CType(Me("UnpressedImage"),String)
            End Get
            Set
                Me("UnpressedImage") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("")>  _
        Public Property PressedImage() As String
            Get
                Return CType(Me("PressedImage"),String)
            End Get
            Set
                Me("PressedImage") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("Unchecked")>  _
        Public Property NormalStart() As Global.System.Windows.Forms.CheckState
            Get
                Return CType(Me("NormalStart"),Global.System.Windows.Forms.CheckState)
            End Get
            Set
                Me("NormalStart") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("Unchecked")>  _
        Public Property ClassicStart() As Global.System.Windows.Forms.CheckState
            Get
                Return CType(Me("ClassicStart"),Global.System.Windows.Forms.CheckState)
            End Get
            Set
                Me("ClassicStart") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("")>  _
        Public Property CurrentMenu() As String
            Get
                Return CType(Me("CurrentMenu"),String)
            End Get
            Set
                Me("CurrentMenu") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("")>  _
        Public Property lastfolder() As String
            Get
                Return CType(Me("lastfolder"),String)
            End Get
            Set
                Me("lastfolder") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("")>  _
        Public Property currentfolder() As String
            Get
                Return CType(Me("currentfolder"),String)
            End Get
            Set
                Me("currentfolder") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("")>  _
        Public Property backgroundimage() As String
            Get
                Return CType(Me("backgroundimage"),String)
            End Get
            Set
                Me("backgroundimage") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("Checked")>  _
        Public Property locktaskbar() As Global.System.Windows.Forms.CheckState
            Get
                Return CType(Me("locktaskbar"),Global.System.Windows.Forms.CheckState)
            End Get
            Set
                Me("locktaskbar") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("Unchecked")>  _
        Public Property autotaskbar() As Global.System.Windows.Forms.CheckState
            Get
                Return CType(Me("autotaskbar"),Global.System.Windows.Forms.CheckState)
            End Get
            Set
                Me("autotaskbar") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("Unchecked")>  _
        Public Property ontoptaskbar() As Global.System.Windows.Forms.CheckState
            Get
                Return CType(Me("ontoptaskbar"),Global.System.Windows.Forms.CheckState)
            End Get
            Set
                Me("ontoptaskbar") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("Unchecked")>  _
        Public Property grouptaskbar() As Global.System.Windows.Forms.CheckState
            Get
                Return CType(Me("grouptaskbar"),Global.System.Windows.Forms.CheckState)
            End Get
            Set
                Me("grouptaskbar") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("Checked")>  _
        Public Property quicktaskbar() As Global.System.Windows.Forms.CheckState
            Get
                Return CType(Me("quicktaskbar"),Global.System.Windows.Forms.CheckState)
            End Get
            Set
                Me("quicktaskbar") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("Checked")>  _
        Public Property showclock() As Global.System.Windows.Forms.CheckState
            Get
                Return CType(Me("showclock"),Global.System.Windows.Forms.CheckState)
            End Get
            Set
                Me("showclock") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("Unchecked")>  _
        Public Property hideicons() As Global.System.Windows.Forms.CheckState
            Get
                Return CType(Me("hideicons"),Global.System.Windows.Forms.CheckState)
            End Get
            Set
                Me("hideicons") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("")>  _
        Public Property StartButtonImage() As String
            Get
                Return CType(Me("StartButtonImage"),String)
            End Get
            Set
                Me("StartButtonImage") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("")>  _
        Public Property iprange() As String
            Get
                Return CType(Me("iprange"),String)
            End Get
            Set
                Me("iprange") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("")>  _
        Public Property startip() As String
            Get
                Return CType(Me("startip"),String)
            End Get
            Set
                Me("startip") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("")>  _
        Public Property endip() As String
            Get
                Return CType(Me("endip"),String)
            End Get
            Set
                Me("endip") = value
            End Set
        End Property
    End Class
End Namespace

Namespace My
    
    <Global.Microsoft.VisualBasic.HideModuleNameAttribute(),  _
     Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
     Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute()>  _
    Friend Module MySettingsProperty
        
        <Global.System.ComponentModel.Design.HelpKeywordAttribute("My.Settings")>  _
        Friend ReadOnly Property Settings() As Global.Start.My.MySettings
            Get
                Return Global.Start.My.MySettings.Default
            End Get
        End Property
    End Module
End Namespace
