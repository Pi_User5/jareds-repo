﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class browser
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(browser))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.browsersettingsmenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.StandardButtonsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddressBarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LinksToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator()
        Me.LockTheToolbarsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CustomizeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolbarsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CloseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UndoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator9 = New System.Windows.Forms.ToolStripSeparator()
        Me.CutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CopyToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PasteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PasteShortcutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator10 = New System.Windows.Forms.ToolStripSeparator()
        Me.CopyToFolderToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MoveToFolderToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator11 = New System.Windows.Forms.ToolStripSeparator()
        Me.SelectAllToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InvertSelectionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatusBarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExplorerBarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SearchToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FavoritesToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.HistoryToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FoldersToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator23 = New System.Windows.Forms.ToolStripSeparator()
        Me.TipOfTheDayToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator()
        Me.LargeIconsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SmallIconsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DetailsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator24 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator8 = New System.Windows.Forms.ToolStripSeparator()
        Me.GotoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RefreshToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FavoritesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddToFavoritesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OrganizeFavoritesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MapNetworkDriveToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DisconnectNetworkDriveToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.FolderOptionsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator21 = New System.Windows.Forms.ToolStripSeparator()
        Me.OtherToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DebugLogToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FolderImageToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IPScannerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LaunchProgramToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IsThisOSLegalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CheckForUpdatesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AboutOSToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ToolStripSplitButton1 = New System.Windows.Forms.ToolStripSplitButton()
        Me.ToolStripSplitButton2 = New System.Windows.Forms.ToolStripSplitButton()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton3 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton4 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton5 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton6 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton7 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripDropDownButton1 = New System.Windows.Forms.ToolStripDropDownButton()
        Me.LargeImageToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SmallIconsToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DetailsToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStrip2 = New System.Windows.Forms.ToolStrip()
        Me.gobutton = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.StandardButtonToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddressBarToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.LinksToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator22 = New System.Windows.Forms.ToolStripSeparator()
        Me.LockTheToolbarsToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.GoButtonToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel()
        Me.ToolStripComboBox1 = New System.Windows.Forms.ToolStripComboBox()
        Me.ToolStripButton8 = New System.Windows.Forms.ToolStripButton()
        Me.shellimages16 = New System.Windows.Forms.ImageList(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ListView1 = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.noitemselect = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ViewsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LargeIconsToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.SmallIconsToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DetailsToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ArrangeIconsByToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AscendingToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DescendingToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RefreshToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator12 = New System.Windows.Forms.ToolStripSeparator()
        Me.PasteToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.PasteShortcutToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator13 = New System.Windows.Forms.ToolStripSeparator()
        Me.NewToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FolderToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ShortcutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator20 = New System.Windows.Forms.ToolStripSeparator()
        Me.BitmapImageToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RichTextDocumentToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TextDocumentToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.shellimages32 = New System.Windows.Forms.ImageList(Me.components)
        Me.favorites16 = New System.Windows.Forms.ImageList(Me.components)
        Me.fileitemselect = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.OpenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExplorerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator14 = New System.Windows.Forms.ToolStripSeparator()
        Me.CutToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CopyToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator15 = New System.Windows.Forms.ToolStripSeparator()
        Me.CreateShortcutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeleteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RenameToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator16 = New System.Windows.Forms.ToolStripSeparator()
        Me.PropertiesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.computermenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.OpenToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExplorerToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator17 = New System.Windows.Forms.ToolStripSeparator()
        Me.SharingAndSecurityToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator18 = New System.Windows.Forms.ToolStripSeparator()
        Me.FormatToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator19 = New System.Windows.Forms.ToolStripSeparator()
        Me.PropertiesToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.AdvacnedToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        Me.browsersettingsmenu.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        Me.ToolStrip2.SuspendLayout()
        Me.gobutton.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.noitemselect.SuspendLayout()
        Me.fileitemselect.SuspendLayout()
        Me.computermenu.SuspendLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.SystemColors.Control
        Me.MenuStrip1.ContextMenuStrip = Me.browsersettingsmenu
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.EditToolStripMenuItem, Me.ViewToolStripMenuItem, Me.FavoritesToolStripMenuItem, Me.ToolsToolStripMenuItem, Me.HelpToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(685, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'browsersettingsmenu
        '
        Me.browsersettingsmenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.StandardButtonsToolStripMenuItem, Me.AddressBarToolStripMenuItem, Me.LinksToolStripMenuItem, Me.ToolStripSeparator6, Me.LockTheToolbarsToolStripMenuItem, Me.CustomizeToolStripMenuItem})
        Me.browsersettingsmenu.Name = "browsersettingsmenu"
        Me.browsersettingsmenu.OwnerItem = Me.ToolbarsToolStripMenuItem
        Me.browsersettingsmenu.Size = New System.Drawing.Size(169, 120)
        '
        'StandardButtonsToolStripMenuItem
        '
        Me.StandardButtonsToolStripMenuItem.Checked = True
        Me.StandardButtonsToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.StandardButtonsToolStripMenuItem.Name = "StandardButtonsToolStripMenuItem"
        Me.StandardButtonsToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.StandardButtonsToolStripMenuItem.Text = "Standard Buttons"
        '
        'AddressBarToolStripMenuItem
        '
        Me.AddressBarToolStripMenuItem.Checked = True
        Me.AddressBarToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.AddressBarToolStripMenuItem.Name = "AddressBarToolStripMenuItem"
        Me.AddressBarToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.AddressBarToolStripMenuItem.Text = "Address Bar"
        '
        'LinksToolStripMenuItem
        '
        Me.LinksToolStripMenuItem.Name = "LinksToolStripMenuItem"
        Me.LinksToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.LinksToolStripMenuItem.Text = "Links"
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(165, 6)
        '
        'LockTheToolbarsToolStripMenuItem
        '
        Me.LockTheToolbarsToolStripMenuItem.Checked = True
        Me.LockTheToolbarsToolStripMenuItem.CheckOnClick = True
        Me.LockTheToolbarsToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.LockTheToolbarsToolStripMenuItem.Name = "LockTheToolbarsToolStripMenuItem"
        Me.LockTheToolbarsToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.LockTheToolbarsToolStripMenuItem.Text = "Lock the Toolbars"
        '
        'CustomizeToolStripMenuItem
        '
        Me.CustomizeToolStripMenuItem.Name = "CustomizeToolStripMenuItem"
        Me.CustomizeToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.CustomizeToolStripMenuItem.Text = "Customize..."
        '
        'ToolbarsToolStripMenuItem
        '
        Me.ToolbarsToolStripMenuItem.DropDown = Me.browsersettingsmenu
        Me.ToolbarsToolStripMenuItem.Name = "ToolbarsToolStripMenuItem"
        Me.ToolbarsToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.ToolbarsToolStripMenuItem.Text = "Toolbars"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CloseToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'CloseToolStripMenuItem
        '
        Me.CloseToolStripMenuItem.Name = "CloseToolStripMenuItem"
        Me.CloseToolStripMenuItem.Size = New System.Drawing.Size(103, 22)
        Me.CloseToolStripMenuItem.Text = "Close"
        '
        'EditToolStripMenuItem
        '
        Me.EditToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.UndoToolStripMenuItem, Me.ToolStripSeparator9, Me.CutToolStripMenuItem, Me.CopyToolStripMenuItem, Me.PasteToolStripMenuItem, Me.PasteShortcutToolStripMenuItem, Me.ToolStripSeparator10, Me.CopyToFolderToolStripMenuItem, Me.MoveToFolderToolStripMenuItem, Me.ToolStripSeparator11, Me.SelectAllToolStripMenuItem, Me.InvertSelectionToolStripMenuItem})
        Me.EditToolStripMenuItem.Name = "EditToolStripMenuItem"
        Me.EditToolStripMenuItem.Size = New System.Drawing.Size(39, 20)
        Me.EditToolStripMenuItem.Text = "Edit"
        '
        'UndoToolStripMenuItem
        '
        Me.UndoToolStripMenuItem.Enabled = False
        Me.UndoToolStripMenuItem.Name = "UndoToolStripMenuItem"
        Me.UndoToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Z), System.Windows.Forms.Keys)
        Me.UndoToolStripMenuItem.Size = New System.Drawing.Size(164, 22)
        Me.UndoToolStripMenuItem.Text = "Undo"
        '
        'ToolStripSeparator9
        '
        Me.ToolStripSeparator9.Name = "ToolStripSeparator9"
        Me.ToolStripSeparator9.Size = New System.Drawing.Size(161, 6)
        '
        'CutToolStripMenuItem
        '
        Me.CutToolStripMenuItem.Enabled = False
        Me.CutToolStripMenuItem.Name = "CutToolStripMenuItem"
        Me.CutToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.X), System.Windows.Forms.Keys)
        Me.CutToolStripMenuItem.Size = New System.Drawing.Size(164, 22)
        Me.CutToolStripMenuItem.Text = "Cut"
        '
        'CopyToolStripMenuItem
        '
        Me.CopyToolStripMenuItem.Enabled = False
        Me.CopyToolStripMenuItem.Name = "CopyToolStripMenuItem"
        Me.CopyToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.C), System.Windows.Forms.Keys)
        Me.CopyToolStripMenuItem.Size = New System.Drawing.Size(164, 22)
        Me.CopyToolStripMenuItem.Text = "Copy"
        '
        'PasteToolStripMenuItem
        '
        Me.PasteToolStripMenuItem.Enabled = False
        Me.PasteToolStripMenuItem.Name = "PasteToolStripMenuItem"
        Me.PasteToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.V), System.Windows.Forms.Keys)
        Me.PasteToolStripMenuItem.Size = New System.Drawing.Size(164, 22)
        Me.PasteToolStripMenuItem.Text = "Paste"
        '
        'PasteShortcutToolStripMenuItem
        '
        Me.PasteShortcutToolStripMenuItem.Enabled = False
        Me.PasteShortcutToolStripMenuItem.Name = "PasteShortcutToolStripMenuItem"
        Me.PasteShortcutToolStripMenuItem.Size = New System.Drawing.Size(164, 22)
        Me.PasteShortcutToolStripMenuItem.Text = "Paste shortcut"
        '
        'ToolStripSeparator10
        '
        Me.ToolStripSeparator10.Name = "ToolStripSeparator10"
        Me.ToolStripSeparator10.Size = New System.Drawing.Size(161, 6)
        '
        'CopyToFolderToolStripMenuItem
        '
        Me.CopyToFolderToolStripMenuItem.Enabled = False
        Me.CopyToFolderToolStripMenuItem.Name = "CopyToFolderToolStripMenuItem"
        Me.CopyToFolderToolStripMenuItem.Size = New System.Drawing.Size(164, 22)
        Me.CopyToFolderToolStripMenuItem.Text = "Copy to Folder..."
        '
        'MoveToFolderToolStripMenuItem
        '
        Me.MoveToFolderToolStripMenuItem.Enabled = False
        Me.MoveToFolderToolStripMenuItem.Name = "MoveToFolderToolStripMenuItem"
        Me.MoveToFolderToolStripMenuItem.Size = New System.Drawing.Size(164, 22)
        Me.MoveToFolderToolStripMenuItem.Text = "Move to Folder..."
        '
        'ToolStripSeparator11
        '
        Me.ToolStripSeparator11.Name = "ToolStripSeparator11"
        Me.ToolStripSeparator11.Size = New System.Drawing.Size(161, 6)
        '
        'SelectAllToolStripMenuItem
        '
        Me.SelectAllToolStripMenuItem.Name = "SelectAllToolStripMenuItem"
        Me.SelectAllToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.A), System.Windows.Forms.Keys)
        Me.SelectAllToolStripMenuItem.Size = New System.Drawing.Size(164, 22)
        Me.SelectAllToolStripMenuItem.Text = "Select All"
        '
        'InvertSelectionToolStripMenuItem
        '
        Me.InvertSelectionToolStripMenuItem.Enabled = False
        Me.InvertSelectionToolStripMenuItem.Name = "InvertSelectionToolStripMenuItem"
        Me.InvertSelectionToolStripMenuItem.Size = New System.Drawing.Size(164, 22)
        Me.InvertSelectionToolStripMenuItem.Text = "Invert Selection"
        '
        'ViewToolStripMenuItem
        '
        Me.ViewToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolbarsToolStripMenuItem, Me.StatusBarToolStripMenuItem, Me.ExplorerBarToolStripMenuItem, Me.ToolStripSeparator7, Me.LargeIconsToolStripMenuItem, Me.SmallIconsToolStripMenuItem, Me.ListToolStripMenuItem, Me.DetailsToolStripMenuItem, Me.ToolStripSeparator24, Me.ToolStripMenuItem1, Me.ToolStripSeparator8, Me.GotoToolStripMenuItem, Me.RefreshToolStripMenuItem})
        Me.ViewToolStripMenuItem.Name = "ViewToolStripMenuItem"
        Me.ViewToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.ViewToolStripMenuItem.Text = "View"
        '
        'StatusBarToolStripMenuItem
        '
        Me.StatusBarToolStripMenuItem.Checked = True
        Me.StatusBarToolStripMenuItem.CheckOnClick = True
        Me.StatusBarToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.StatusBarToolStripMenuItem.Name = "StatusBarToolStripMenuItem"
        Me.StatusBarToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.StatusBarToolStripMenuItem.Text = "Status Bar"
        '
        'ExplorerBarToolStripMenuItem
        '
        Me.ExplorerBarToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SearchToolStripMenuItem, Me.FavoritesToolStripMenuItem1, Me.HistoryToolStripMenuItem, Me.FoldersToolStripMenuItem, Me.ToolStripSeparator23, Me.TipOfTheDayToolStripMenuItem})
        Me.ExplorerBarToolStripMenuItem.Name = "ExplorerBarToolStripMenuItem"
        Me.ExplorerBarToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.ExplorerBarToolStripMenuItem.Text = "Explorer Bar"
        '
        'SearchToolStripMenuItem
        '
        Me.SearchToolStripMenuItem.CheckOnClick = True
        Me.SearchToolStripMenuItem.Name = "SearchToolStripMenuItem"
        Me.SearchToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.E), System.Windows.Forms.Keys)
        Me.SearchToolStripMenuItem.Size = New System.Drawing.Size(164, 22)
        Me.SearchToolStripMenuItem.Text = "Search  "
        '
        'FavoritesToolStripMenuItem1
        '
        Me.FavoritesToolStripMenuItem1.CheckOnClick = True
        Me.FavoritesToolStripMenuItem1.Name = "FavoritesToolStripMenuItem1"
        Me.FavoritesToolStripMenuItem1.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.I), System.Windows.Forms.Keys)
        Me.FavoritesToolStripMenuItem1.Size = New System.Drawing.Size(164, 22)
        Me.FavoritesToolStripMenuItem1.Text = "Favorites"
        '
        'HistoryToolStripMenuItem
        '
        Me.HistoryToolStripMenuItem.CheckOnClick = True
        Me.HistoryToolStripMenuItem.Name = "HistoryToolStripMenuItem"
        Me.HistoryToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.H), System.Windows.Forms.Keys)
        Me.HistoryToolStripMenuItem.Size = New System.Drawing.Size(164, 22)
        Me.HistoryToolStripMenuItem.Text = "History   "
        '
        'FoldersToolStripMenuItem
        '
        Me.FoldersToolStripMenuItem.CheckOnClick = True
        Me.FoldersToolStripMenuItem.Name = "FoldersToolStripMenuItem"
        Me.FoldersToolStripMenuItem.Size = New System.Drawing.Size(164, 22)
        Me.FoldersToolStripMenuItem.Text = "Folders"
        '
        'ToolStripSeparator23
        '
        Me.ToolStripSeparator23.Name = "ToolStripSeparator23"
        Me.ToolStripSeparator23.Size = New System.Drawing.Size(161, 6)
        '
        'TipOfTheDayToolStripMenuItem
        '
        Me.TipOfTheDayToolStripMenuItem.CheckOnClick = True
        Me.TipOfTheDayToolStripMenuItem.Name = "TipOfTheDayToolStripMenuItem"
        Me.TipOfTheDayToolStripMenuItem.Size = New System.Drawing.Size(164, 22)
        Me.TipOfTheDayToolStripMenuItem.Text = "Tip of the Day"
        '
        'ToolStripSeparator7
        '
        Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
        Me.ToolStripSeparator7.Size = New System.Drawing.Size(160, 6)
        '
        'LargeIconsToolStripMenuItem
        '
        Me.LargeIconsToolStripMenuItem.Name = "LargeIconsToolStripMenuItem"
        Me.LargeIconsToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.LargeIconsToolStripMenuItem.Text = "Large Icons"
        '
        'SmallIconsToolStripMenuItem
        '
        Me.SmallIconsToolStripMenuItem.Name = "SmallIconsToolStripMenuItem"
        Me.SmallIconsToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.SmallIconsToolStripMenuItem.Text = "Small Icons"
        '
        'ListToolStripMenuItem
        '
        Me.ListToolStripMenuItem.Name = "ListToolStripMenuItem"
        Me.ListToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.ListToolStripMenuItem.Text = "List"
        '
        'DetailsToolStripMenuItem
        '
        Me.DetailsToolStripMenuItem.Name = "DetailsToolStripMenuItem"
        Me.DetailsToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.DetailsToolStripMenuItem.Text = "Details"
        '
        'ToolStripSeparator24
        '
        Me.ToolStripSeparator24.Name = "ToolStripSeparator24"
        Me.ToolStripSeparator24.Size = New System.Drawing.Size(160, 6)
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(163, 22)
        Me.ToolStripMenuItem1.Text = "Arrange Icons by"
        '
        'ToolStripSeparator8
        '
        Me.ToolStripSeparator8.Name = "ToolStripSeparator8"
        Me.ToolStripSeparator8.Size = New System.Drawing.Size(160, 6)
        '
        'GotoToolStripMenuItem
        '
        Me.GotoToolStripMenuItem.Enabled = False
        Me.GotoToolStripMenuItem.Name = "GotoToolStripMenuItem"
        Me.GotoToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.GotoToolStripMenuItem.Text = "Go To"
        '
        'RefreshToolStripMenuItem
        '
        Me.RefreshToolStripMenuItem.Name = "RefreshToolStripMenuItem"
        Me.RefreshToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5
        Me.RefreshToolStripMenuItem.ShowShortcutKeys = False
        Me.RefreshToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.RefreshToolStripMenuItem.Text = "Refresh"
        '
        'FavoritesToolStripMenuItem
        '
        Me.FavoritesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AddToFavoritesToolStripMenuItem, Me.OrganizeFavoritesToolStripMenuItem, Me.ToolStripSeparator5})
        Me.FavoritesToolStripMenuItem.Name = "FavoritesToolStripMenuItem"
        Me.FavoritesToolStripMenuItem.Size = New System.Drawing.Size(66, 20)
        Me.FavoritesToolStripMenuItem.Text = "Favorites"
        '
        'AddToFavoritesToolStripMenuItem
        '
        Me.AddToFavoritesToolStripMenuItem.Name = "AddToFavoritesToolStripMenuItem"
        Me.AddToFavoritesToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.AddToFavoritesToolStripMenuItem.Text = "Add to Favorites..."
        '
        'OrganizeFavoritesToolStripMenuItem
        '
        Me.OrganizeFavoritesToolStripMenuItem.Name = "OrganizeFavoritesToolStripMenuItem"
        Me.OrganizeFavoritesToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.OrganizeFavoritesToolStripMenuItem.Text = "Organize Favorites..."
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(177, 6)
        '
        'ToolsToolStripMenuItem
        '
        Me.ToolsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MapNetworkDriveToolStripMenuItem, Me.DisconnectNetworkDriveToolStripMenuItem, Me.ToolStripSeparator4, Me.FolderOptionsToolStripMenuItem, Me.ToolStripSeparator21, Me.OtherToolStripMenuItem})
        Me.ToolsToolStripMenuItem.Name = "ToolsToolStripMenuItem"
        Me.ToolsToolStripMenuItem.Size = New System.Drawing.Size(48, 20)
        Me.ToolsToolStripMenuItem.Text = "Tools"
        '
        'MapNetworkDriveToolStripMenuItem
        '
        Me.MapNetworkDriveToolStripMenuItem.Name = "MapNetworkDriveToolStripMenuItem"
        Me.MapNetworkDriveToolStripMenuItem.Size = New System.Drawing.Size(220, 22)
        Me.MapNetworkDriveToolStripMenuItem.Text = "Map Network Drive..."
        '
        'DisconnectNetworkDriveToolStripMenuItem
        '
        Me.DisconnectNetworkDriveToolStripMenuItem.Name = "DisconnectNetworkDriveToolStripMenuItem"
        Me.DisconnectNetworkDriveToolStripMenuItem.Size = New System.Drawing.Size(220, 22)
        Me.DisconnectNetworkDriveToolStripMenuItem.Text = "Disconnect Network Drive..."
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(217, 6)
        '
        'FolderOptionsToolStripMenuItem
        '
        Me.FolderOptionsToolStripMenuItem.Name = "FolderOptionsToolStripMenuItem"
        Me.FolderOptionsToolStripMenuItem.Size = New System.Drawing.Size(220, 22)
        Me.FolderOptionsToolStripMenuItem.Text = "Folder Options"
        '
        'ToolStripSeparator21
        '
        Me.ToolStripSeparator21.Name = "ToolStripSeparator21"
        Me.ToolStripSeparator21.Size = New System.Drawing.Size(217, 6)
        '
        'OtherToolStripMenuItem
        '
        Me.OtherToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DebugLogToolStripMenuItem, Me.FolderImageToolStripMenuItem, Me.IPScannerToolStripMenuItem, Me.LaunchProgramToolStripMenuItem, Me.AdvacnedToolStripMenuItem})
        Me.OtherToolStripMenuItem.Name = "OtherToolStripMenuItem"
        Me.OtherToolStripMenuItem.Size = New System.Drawing.Size(220, 22)
        Me.OtherToolStripMenuItem.Text = "Other"
        '
        'DebugLogToolStripMenuItem
        '
        Me.DebugLogToolStripMenuItem.Name = "DebugLogToolStripMenuItem"
        Me.DebugLogToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.D), System.Windows.Forms.Keys)
        Me.DebugLogToolStripMenuItem.Size = New System.Drawing.Size(170, 22)
        Me.DebugLogToolStripMenuItem.Text = "Debug Log"
        '
        'FolderImageToolStripMenuItem
        '
        Me.FolderImageToolStripMenuItem.Name = "FolderImageToolStripMenuItem"
        Me.FolderImageToolStripMenuItem.Size = New System.Drawing.Size(170, 22)
        Me.FolderImageToolStripMenuItem.Text = "Folder Image"
        '
        'IPScannerToolStripMenuItem
        '
        Me.IPScannerToolStripMenuItem.Name = "IPScannerToolStripMenuItem"
        Me.IPScannerToolStripMenuItem.Size = New System.Drawing.Size(170, 22)
        Me.IPScannerToolStripMenuItem.Text = "IP Scanner"
        '
        'LaunchProgramToolStripMenuItem
        '
        Me.LaunchProgramToolStripMenuItem.Name = "LaunchProgramToolStripMenuItem"
        Me.LaunchProgramToolStripMenuItem.Size = New System.Drawing.Size(170, 22)
        Me.LaunchProgramToolStripMenuItem.Text = "Launch Program"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.IsThisOSLegalToolStripMenuItem, Me.CheckForUpdatesToolStripMenuItem, Me.AboutOSToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'IsThisOSLegalToolStripMenuItem
        '
        Me.IsThisOSLegalToolStripMenuItem.Name = "IsThisOSLegalToolStripMenuItem"
        Me.IsThisOSLegalToolStripMenuItem.Size = New System.Drawing.Size(232, 22)
        Me.IsThisOSLegalToolStripMenuItem.Text = "Is this copy of Windows legal?"
        '
        'CheckForUpdatesToolStripMenuItem
        '
        Me.CheckForUpdatesToolStripMenuItem.Name = "CheckForUpdatesToolStripMenuItem"
        Me.CheckForUpdatesToolStripMenuItem.Size = New System.Drawing.Size(232, 22)
        Me.CheckForUpdatesToolStripMenuItem.Text = "Check for Updates"
        '
        'AboutOSToolStripMenuItem
        '
        Me.AboutOSToolStripMenuItem.Name = "AboutOSToolStripMenuItem"
        Me.AboutOSToolStripMenuItem.Size = New System.Drawing.Size(232, 22)
        Me.AboutOSToolStripMenuItem.Text = "About Windows"
        '
        'ToolStrip1
        '
        Me.ToolStrip1.BackColor = System.Drawing.SystemColors.Control
        Me.ToolStrip1.ContextMenuStrip = Me.browsersettingsmenu
        Me.ToolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip1.ImageScalingSize = New System.Drawing.Size(24, 24)
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripSplitButton1, Me.ToolStripSplitButton2, Me.ToolStripButton1, Me.ToolStripSeparator1, Me.ToolStripButton2, Me.ToolStripButton3, Me.ToolStripSeparator2, Me.ToolStripButton4, Me.ToolStripButton5, Me.ToolStripButton6, Me.ToolStripButton7, Me.ToolStripSeparator3, Me.ToolStripDropDownButton1})
        Me.ToolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 24)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(685, 31)
        Me.ToolStrip1.TabIndex = 1
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStripSplitButton1
        '
        Me.ToolStripSplitButton1.Enabled = False
        Me.ToolStripSplitButton1.Image = CType(resources.GetObject("ToolStripSplitButton1.Image"), System.Drawing.Image)
        Me.ToolStripSplitButton1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ToolStripSplitButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripSplitButton1.Name = "ToolStripSplitButton1"
        Me.ToolStripSplitButton1.Size = New System.Drawing.Size(72, 28)
        Me.ToolStripSplitButton1.Text = "Back"
        '
        'ToolStripSplitButton2
        '
        Me.ToolStripSplitButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripSplitButton2.Enabled = False
        Me.ToolStripSplitButton2.Image = CType(resources.GetObject("ToolStripSplitButton2.Image"), System.Drawing.Image)
        Me.ToolStripSplitButton2.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ToolStripSplitButton2.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripSplitButton2.Name = "ToolStripSplitButton2"
        Me.ToolStripSplitButton2.Size = New System.Drawing.Size(40, 28)
        Me.ToolStripSplitButton2.Text = "Forward"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(28, 28)
        Me.ToolStripButton1.Text = "Up"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 31)
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.Image = CType(resources.GetObject("ToolStripButton2.Image"), System.Drawing.Image)
        Me.ToolStripButton2.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ToolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.Size = New System.Drawing.Size(70, 28)
        Me.ToolStripButton2.Text = "Search"
        '
        'ToolStripButton3
        '
        Me.ToolStripButton3.Image = CType(resources.GetObject("ToolStripButton3.Image"), System.Drawing.Image)
        Me.ToolStripButton3.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ToolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton3.Name = "ToolStripButton3"
        Me.ToolStripButton3.Size = New System.Drawing.Size(73, 28)
        Me.ToolStripButton3.Text = "Folders"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 31)
        '
        'ToolStripButton4
        '
        Me.ToolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton4.Enabled = False
        Me.ToolStripButton4.Image = CType(resources.GetObject("ToolStripButton4.Image"), System.Drawing.Image)
        Me.ToolStripButton4.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ToolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton4.Name = "ToolStripButton4"
        Me.ToolStripButton4.Size = New System.Drawing.Size(28, 28)
        Me.ToolStripButton4.Text = "Move To"
        '
        'ToolStripButton5
        '
        Me.ToolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton5.Enabled = False
        Me.ToolStripButton5.Image = CType(resources.GetObject("ToolStripButton5.Image"), System.Drawing.Image)
        Me.ToolStripButton5.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ToolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton5.Name = "ToolStripButton5"
        Me.ToolStripButton5.Size = New System.Drawing.Size(28, 28)
        Me.ToolStripButton5.Text = "Copy To"
        '
        'ToolStripButton6
        '
        Me.ToolStripButton6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton6.Enabled = False
        Me.ToolStripButton6.Image = CType(resources.GetObject("ToolStripButton6.Image"), System.Drawing.Image)
        Me.ToolStripButton6.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ToolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton6.Name = "ToolStripButton6"
        Me.ToolStripButton6.Size = New System.Drawing.Size(28, 28)
        Me.ToolStripButton6.Text = "Delete"
        '
        'ToolStripButton7
        '
        Me.ToolStripButton7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton7.Enabled = False
        Me.ToolStripButton7.Image = CType(resources.GetObject("ToolStripButton7.Image"), System.Drawing.Image)
        Me.ToolStripButton7.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ToolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton7.Name = "ToolStripButton7"
        Me.ToolStripButton7.Size = New System.Drawing.Size(28, 28)
        Me.ToolStripButton7.Text = "Undo"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 31)
        '
        'ToolStripDropDownButton1
        '
        Me.ToolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripDropDownButton1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LargeImageToolStripMenuItem, Me.SmallIconsToolStripMenuItem1, Me.ListToolStripMenuItem1, Me.DetailsToolStripMenuItem1})
        Me.ToolStripDropDownButton1.Image = CType(resources.GetObject("ToolStripDropDownButton1.Image"), System.Drawing.Image)
        Me.ToolStripDropDownButton1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ToolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripDropDownButton1.Name = "ToolStripDropDownButton1"
        Me.ToolStripDropDownButton1.Size = New System.Drawing.Size(37, 28)
        Me.ToolStripDropDownButton1.Text = "Views"
        '
        'LargeImageToolStripMenuItem
        '
        Me.LargeImageToolStripMenuItem.Name = "LargeImageToolStripMenuItem"
        Me.LargeImageToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me.LargeImageToolStripMenuItem.Text = "Large Icons"
        '
        'SmallIconsToolStripMenuItem1
        '
        Me.SmallIconsToolStripMenuItem1.Name = "SmallIconsToolStripMenuItem1"
        Me.SmallIconsToolStripMenuItem1.Size = New System.Drawing.Size(134, 22)
        Me.SmallIconsToolStripMenuItem1.Text = "Small Icons"
        '
        'ListToolStripMenuItem1
        '
        Me.ListToolStripMenuItem1.Name = "ListToolStripMenuItem1"
        Me.ListToolStripMenuItem1.Size = New System.Drawing.Size(134, 22)
        Me.ListToolStripMenuItem1.Text = "List"
        '
        'DetailsToolStripMenuItem1
        '
        Me.DetailsToolStripMenuItem1.Name = "DetailsToolStripMenuItem1"
        Me.DetailsToolStripMenuItem1.Size = New System.Drawing.Size(134, 22)
        Me.DetailsToolStripMenuItem1.Text = "Details"
        '
        'ToolStrip2
        '
        Me.ToolStrip2.BackColor = System.Drawing.SystemColors.Control
        Me.ToolStrip2.ContextMenuStrip = Me.gobutton
        Me.ToolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripLabel1, Me.ToolStripComboBox1, Me.ToolStripButton8})
        Me.ToolStrip2.Location = New System.Drawing.Point(0, 55)
        Me.ToolStrip2.Name = "ToolStrip2"
        Me.ToolStrip2.Size = New System.Drawing.Size(685, 25)
        Me.ToolStrip2.TabIndex = 2
        Me.ToolStrip2.Text = "ToolStrip2"
        '
        'gobutton
        '
        Me.gobutton.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.StandardButtonToolStripMenuItem, Me.AddressBarToolStripMenuItem1, Me.LinksToolStripMenuItem1, Me.ToolStripSeparator22, Me.LockTheToolbarsToolStripMenuItem1, Me.GoButtonToolStripMenuItem})
        Me.gobutton.Name = "gobutton"
        Me.gobutton.Size = New System.Drawing.Size(169, 120)
        '
        'StandardButtonToolStripMenuItem
        '
        Me.StandardButtonToolStripMenuItem.Checked = True
        Me.StandardButtonToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.StandardButtonToolStripMenuItem.Name = "StandardButtonToolStripMenuItem"
        Me.StandardButtonToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.StandardButtonToolStripMenuItem.Text = "Standard Buttons"
        '
        'AddressBarToolStripMenuItem1
        '
        Me.AddressBarToolStripMenuItem1.Checked = True
        Me.AddressBarToolStripMenuItem1.CheckState = System.Windows.Forms.CheckState.Checked
        Me.AddressBarToolStripMenuItem1.Name = "AddressBarToolStripMenuItem1"
        Me.AddressBarToolStripMenuItem1.Size = New System.Drawing.Size(168, 22)
        Me.AddressBarToolStripMenuItem1.Text = "Address Bar"
        '
        'LinksToolStripMenuItem1
        '
        Me.LinksToolStripMenuItem1.Name = "LinksToolStripMenuItem1"
        Me.LinksToolStripMenuItem1.Size = New System.Drawing.Size(168, 22)
        Me.LinksToolStripMenuItem1.Text = "Links"
        '
        'ToolStripSeparator22
        '
        Me.ToolStripSeparator22.Name = "ToolStripSeparator22"
        Me.ToolStripSeparator22.Size = New System.Drawing.Size(165, 6)
        '
        'LockTheToolbarsToolStripMenuItem1
        '
        Me.LockTheToolbarsToolStripMenuItem1.Checked = True
        Me.LockTheToolbarsToolStripMenuItem1.CheckState = System.Windows.Forms.CheckState.Checked
        Me.LockTheToolbarsToolStripMenuItem1.Name = "LockTheToolbarsToolStripMenuItem1"
        Me.LockTheToolbarsToolStripMenuItem1.Size = New System.Drawing.Size(168, 22)
        Me.LockTheToolbarsToolStripMenuItem1.Text = "Lock the Toolbars"
        '
        'GoButtonToolStripMenuItem
        '
        Me.GoButtonToolStripMenuItem.Checked = True
        Me.GoButtonToolStripMenuItem.CheckOnClick = True
        Me.GoButtonToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.GoButtonToolStripMenuItem.Name = "GoButtonToolStripMenuItem"
        Me.GoButtonToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.GoButtonToolStripMenuItem.Text = "Go Button"
        '
        'ToolStripLabel1
        '
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        Me.ToolStripLabel1.Size = New System.Drawing.Size(49, 22)
        Me.ToolStripLabel1.Text = "Address"
        '
        'ToolStripComboBox1
        '
        Me.ToolStripComboBox1.AutoSize = False
        Me.ToolStripComboBox1.Name = "ToolStripComboBox1"
        Me.ToolStripComboBox1.Size = New System.Drawing.Size(560, 23)
        '
        'ToolStripButton8
        '
        Me.ToolStripButton8.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripButton8.Image = CType(resources.GetObject("ToolStripButton8.Image"), System.Drawing.Image)
        Me.ToolStripButton8.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ToolStripButton8.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton8.Name = "ToolStripButton8"
        Me.ToolStripButton8.Size = New System.Drawing.Size(42, 22)
        Me.ToolStripButton8.Text = "Go"
        '
        'shellimages16
        '
        Me.shellimages16.ImageStream = CType(resources.GetObject("shellimages16.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.shellimages16.TransparentColor = System.Drawing.Color.Transparent
        Me.shellimages16.Images.SetKeyName(0, "folder 16.png")
        Me.shellimages16.Images.SetKeyName(1, "drive-harddisk 16.png")
        Me.shellimages16.Images.SetKeyName(2, "drive-optical 16.png")
        Me.shellimages16.Images.SetKeyName(3, "drive-removable-media 16.png")
        Me.shellimages16.Images.SetKeyName(4, "drive-network 16.png")
        Me.shellimages16.Images.SetKeyName(5, "drive-ram.png")
        Me.shellimages16.Images.SetKeyName(6, "drive-unknown 16.png")
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 394)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(685, 22)
        Me.StatusStrip1.TabIndex = 3
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(58, 17)
        Me.ToolStripStatusLabel1.Text = "% objects"
        '
        'ListView1
        '
        Me.ListView1.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.ListView1.ContextMenuStrip = Me.noitemselect
        Me.ListView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ListView1.LabelEdit = True
        Me.ListView1.LargeImageList = Me.shellimages32
        Me.ListView1.Location = New System.Drawing.Point(0, 0)
        Me.ListView1.Name = "ListView1"
        Me.ListView1.Size = New System.Drawing.Size(478, 314)
        Me.ListView1.SmallImageList = Me.shellimages16
        Me.ListView1.TabIndex = 4
        Me.ListView1.UseCompatibleStateImageBehavior = False
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "File"
        Me.ColumnHeader1.Width = 100
        '
        'noitemselect
        '
        Me.noitemselect.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ViewsToolStripMenuItem, Me.ArrangeIconsByToolStripMenuItem, Me.RefreshToolStripMenuItem1, Me.ToolStripSeparator12, Me.PasteToolStripMenuItem1, Me.PasteShortcutToolStripMenuItem1, Me.ToolStripSeparator13, Me.NewToolStripMenuItem})
        Me.noitemselect.Name = "noitemselect"
        Me.noitemselect.ShowImageMargin = False
        Me.noitemselect.Size = New System.Drawing.Size(139, 148)
        '
        'ViewsToolStripMenuItem
        '
        Me.ViewsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LargeIconsToolStripMenuItem1, Me.SmallIconsToolStripMenuItem2, Me.ListToolStripMenuItem2, Me.DetailsToolStripMenuItem2})
        Me.ViewsToolStripMenuItem.Name = "ViewsToolStripMenuItem"
        Me.ViewsToolStripMenuItem.Size = New System.Drawing.Size(138, 22)
        Me.ViewsToolStripMenuItem.Text = "Views"
        '
        'LargeIconsToolStripMenuItem1
        '
        Me.LargeIconsToolStripMenuItem1.Name = "LargeIconsToolStripMenuItem1"
        Me.LargeIconsToolStripMenuItem1.Size = New System.Drawing.Size(134, 22)
        Me.LargeIconsToolStripMenuItem1.Text = "Large Icons"
        '
        'SmallIconsToolStripMenuItem2
        '
        Me.SmallIconsToolStripMenuItem2.Name = "SmallIconsToolStripMenuItem2"
        Me.SmallIconsToolStripMenuItem2.Size = New System.Drawing.Size(134, 22)
        Me.SmallIconsToolStripMenuItem2.Text = "Small Icons"
        '
        'ListToolStripMenuItem2
        '
        Me.ListToolStripMenuItem2.Name = "ListToolStripMenuItem2"
        Me.ListToolStripMenuItem2.Size = New System.Drawing.Size(134, 22)
        Me.ListToolStripMenuItem2.Text = "List"
        '
        'DetailsToolStripMenuItem2
        '
        Me.DetailsToolStripMenuItem2.Name = "DetailsToolStripMenuItem2"
        Me.DetailsToolStripMenuItem2.Size = New System.Drawing.Size(134, 22)
        Me.DetailsToolStripMenuItem2.Text = "Details"
        '
        'ArrangeIconsByToolStripMenuItem
        '
        Me.ArrangeIconsByToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AscendingToolStripMenuItem, Me.DescendingToolStripMenuItem})
        Me.ArrangeIconsByToolStripMenuItem.Name = "ArrangeIconsByToolStripMenuItem"
        Me.ArrangeIconsByToolStripMenuItem.Size = New System.Drawing.Size(138, 22)
        Me.ArrangeIconsByToolStripMenuItem.Text = "Arrange Icons By"
        '
        'AscendingToolStripMenuItem
        '
        Me.AscendingToolStripMenuItem.Name = "AscendingToolStripMenuItem"
        Me.AscendingToolStripMenuItem.Size = New System.Drawing.Size(136, 22)
        Me.AscendingToolStripMenuItem.Text = "Ascending"
        '
        'DescendingToolStripMenuItem
        '
        Me.DescendingToolStripMenuItem.Name = "DescendingToolStripMenuItem"
        Me.DescendingToolStripMenuItem.Size = New System.Drawing.Size(136, 22)
        Me.DescendingToolStripMenuItem.Text = "Descending"
        '
        'RefreshToolStripMenuItem1
        '
        Me.RefreshToolStripMenuItem1.Name = "RefreshToolStripMenuItem1"
        Me.RefreshToolStripMenuItem1.Size = New System.Drawing.Size(138, 22)
        Me.RefreshToolStripMenuItem1.Text = "Refresh"
        '
        'ToolStripSeparator12
        '
        Me.ToolStripSeparator12.Name = "ToolStripSeparator12"
        Me.ToolStripSeparator12.Size = New System.Drawing.Size(135, 6)
        '
        'PasteToolStripMenuItem1
        '
        Me.PasteToolStripMenuItem1.Enabled = False
        Me.PasteToolStripMenuItem1.Name = "PasteToolStripMenuItem1"
        Me.PasteToolStripMenuItem1.Size = New System.Drawing.Size(138, 22)
        Me.PasteToolStripMenuItem1.Text = "Paste"
        '
        'PasteShortcutToolStripMenuItem1
        '
        Me.PasteShortcutToolStripMenuItem1.Enabled = False
        Me.PasteShortcutToolStripMenuItem1.Name = "PasteShortcutToolStripMenuItem1"
        Me.PasteShortcutToolStripMenuItem1.Size = New System.Drawing.Size(138, 22)
        Me.PasteShortcutToolStripMenuItem1.Text = "Paste Shortcut"
        '
        'ToolStripSeparator13
        '
        Me.ToolStripSeparator13.Name = "ToolStripSeparator13"
        Me.ToolStripSeparator13.Size = New System.Drawing.Size(135, 6)
        '
        'NewToolStripMenuItem
        '
        Me.NewToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FolderToolStripMenuItem, Me.ShortcutToolStripMenuItem, Me.ToolStripSeparator20, Me.BitmapImageToolStripMenuItem, Me.RichTextDocumentToolStripMenuItem, Me.TextDocumentToolStripMenuItem})
        Me.NewToolStripMenuItem.Name = "NewToolStripMenuItem"
        Me.NewToolStripMenuItem.Size = New System.Drawing.Size(138, 22)
        Me.NewToolStripMenuItem.Text = "New"
        '
        'FolderToolStripMenuItem
        '
        Me.FolderToolStripMenuItem.Image = CType(resources.GetObject("FolderToolStripMenuItem.Image"), System.Drawing.Image)
        Me.FolderToolStripMenuItem.Name = "FolderToolStripMenuItem"
        Me.FolderToolStripMenuItem.Size = New System.Drawing.Size(181, 22)
        Me.FolderToolStripMenuItem.Text = "Folder"
        '
        'ShortcutToolStripMenuItem
        '
        Me.ShortcutToolStripMenuItem.Image = CType(resources.GetObject("ShortcutToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ShortcutToolStripMenuItem.Name = "ShortcutToolStripMenuItem"
        Me.ShortcutToolStripMenuItem.Size = New System.Drawing.Size(181, 22)
        Me.ShortcutToolStripMenuItem.Text = "Shortcut"
        '
        'ToolStripSeparator20
        '
        Me.ToolStripSeparator20.Name = "ToolStripSeparator20"
        Me.ToolStripSeparator20.Size = New System.Drawing.Size(178, 6)
        '
        'BitmapImageToolStripMenuItem
        '
        Me.BitmapImageToolStripMenuItem.Image = CType(resources.GetObject("BitmapImageToolStripMenuItem.Image"), System.Drawing.Image)
        Me.BitmapImageToolStripMenuItem.Name = "BitmapImageToolStripMenuItem"
        Me.BitmapImageToolStripMenuItem.Size = New System.Drawing.Size(181, 22)
        Me.BitmapImageToolStripMenuItem.Text = "Bitmap Image"
        '
        'RichTextDocumentToolStripMenuItem
        '
        Me.RichTextDocumentToolStripMenuItem.Image = CType(resources.GetObject("RichTextDocumentToolStripMenuItem.Image"), System.Drawing.Image)
        Me.RichTextDocumentToolStripMenuItem.Name = "RichTextDocumentToolStripMenuItem"
        Me.RichTextDocumentToolStripMenuItem.Size = New System.Drawing.Size(181, 22)
        Me.RichTextDocumentToolStripMenuItem.Text = "Rich Text Document"
        '
        'TextDocumentToolStripMenuItem
        '
        Me.TextDocumentToolStripMenuItem.Image = CType(resources.GetObject("TextDocumentToolStripMenuItem.Image"), System.Drawing.Image)
        Me.TextDocumentToolStripMenuItem.Name = "TextDocumentToolStripMenuItem"
        Me.TextDocumentToolStripMenuItem.Size = New System.Drawing.Size(181, 22)
        Me.TextDocumentToolStripMenuItem.Text = "Text Document"
        '
        'shellimages32
        '
        Me.shellimages32.ImageStream = CType(resources.GetObject("shellimages32.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.shellimages32.TransparentColor = System.Drawing.Color.Transparent
        Me.shellimages32.Images.SetKeyName(0, "folder.png")
        Me.shellimages32.Images.SetKeyName(1, "drive-harddisk.png")
        Me.shellimages32.Images.SetKeyName(2, "drive-optical.png")
        Me.shellimages32.Images.SetKeyName(3, "drive-removable-media.png")
        Me.shellimages32.Images.SetKeyName(4, "drive-network.png")
        Me.shellimages32.Images.SetKeyName(5, "drive-ram.png")
        Me.shellimages32.Images.SetKeyName(6, "drive-unknown.png")
        '
        'favorites16
        '
        Me.favorites16.ImageStream = CType(resources.GetObject("favorites16.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.favorites16.TransparentColor = System.Drawing.Color.Transparent
        Me.favorites16.Images.SetKeyName(0, "folder 16.png")
        Me.favorites16.Images.SetKeyName(1, "favorite.png")
        '
        'fileitemselect
        '
        Me.fileitemselect.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OpenToolStripMenuItem, Me.ExplorerToolStripMenuItem, Me.ToolStripSeparator14, Me.CutToolStripMenuItem1, Me.CopyToolStripMenuItem1, Me.ToolStripSeparator15, Me.CreateShortcutToolStripMenuItem, Me.DeleteToolStripMenuItem, Me.RenameToolStripMenuItem, Me.ToolStripSeparator16, Me.PropertiesToolStripMenuItem})
        Me.fileitemselect.Name = "itemselect"
        Me.fileitemselect.ShowImageMargin = False
        Me.fileitemselect.Size = New System.Drawing.Size(132, 198)
        '
        'OpenToolStripMenuItem
        '
        Me.OpenToolStripMenuItem.Name = "OpenToolStripMenuItem"
        Me.OpenToolStripMenuItem.Size = New System.Drawing.Size(131, 22)
        Me.OpenToolStripMenuItem.Text = "Open"
        '
        'ExplorerToolStripMenuItem
        '
        Me.ExplorerToolStripMenuItem.Name = "ExplorerToolStripMenuItem"
        Me.ExplorerToolStripMenuItem.Size = New System.Drawing.Size(131, 22)
        Me.ExplorerToolStripMenuItem.Text = "Explore"
        '
        'ToolStripSeparator14
        '
        Me.ToolStripSeparator14.Name = "ToolStripSeparator14"
        Me.ToolStripSeparator14.Size = New System.Drawing.Size(128, 6)
        '
        'CutToolStripMenuItem1
        '
        Me.CutToolStripMenuItem1.Name = "CutToolStripMenuItem1"
        Me.CutToolStripMenuItem1.Size = New System.Drawing.Size(131, 22)
        Me.CutToolStripMenuItem1.Text = "Cut"
        '
        'CopyToolStripMenuItem1
        '
        Me.CopyToolStripMenuItem1.Name = "CopyToolStripMenuItem1"
        Me.CopyToolStripMenuItem1.Size = New System.Drawing.Size(131, 22)
        Me.CopyToolStripMenuItem1.Text = "Copy"
        '
        'ToolStripSeparator15
        '
        Me.ToolStripSeparator15.Name = "ToolStripSeparator15"
        Me.ToolStripSeparator15.Size = New System.Drawing.Size(128, 6)
        '
        'CreateShortcutToolStripMenuItem
        '
        Me.CreateShortcutToolStripMenuItem.Name = "CreateShortcutToolStripMenuItem"
        Me.CreateShortcutToolStripMenuItem.Size = New System.Drawing.Size(131, 22)
        Me.CreateShortcutToolStripMenuItem.Text = "Create Shortcut"
        '
        'DeleteToolStripMenuItem
        '
        Me.DeleteToolStripMenuItem.Name = "DeleteToolStripMenuItem"
        Me.DeleteToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.Delete
        Me.DeleteToolStripMenuItem.ShowShortcutKeys = False
        Me.DeleteToolStripMenuItem.Size = New System.Drawing.Size(131, 22)
        Me.DeleteToolStripMenuItem.Text = "Delete"
        '
        'RenameToolStripMenuItem
        '
        Me.RenameToolStripMenuItem.Name = "RenameToolStripMenuItem"
        Me.RenameToolStripMenuItem.Size = New System.Drawing.Size(131, 22)
        Me.RenameToolStripMenuItem.Text = "Rename"
        '
        'ToolStripSeparator16
        '
        Me.ToolStripSeparator16.Name = "ToolStripSeparator16"
        Me.ToolStripSeparator16.Size = New System.Drawing.Size(128, 6)
        '
        'PropertiesToolStripMenuItem
        '
        Me.PropertiesToolStripMenuItem.Name = "PropertiesToolStripMenuItem"
        Me.PropertiesToolStripMenuItem.Size = New System.Drawing.Size(131, 22)
        Me.PropertiesToolStripMenuItem.Text = "Properties"
        '
        'computermenu
        '
        Me.computermenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OpenToolStripMenuItem1, Me.ExplorerToolStripMenuItem1, Me.ToolStripSeparator17, Me.SharingAndSecurityToolStripMenuItem, Me.ToolStripSeparator18, Me.FormatToolStripMenuItem, Me.ToolStripSeparator19, Me.PropertiesToolStripMenuItem1})
        Me.computermenu.Name = "computermenu"
        Me.computermenu.ShowImageMargin = False
        Me.computermenu.Size = New System.Drawing.Size(167, 132)
        '
        'OpenToolStripMenuItem1
        '
        Me.OpenToolStripMenuItem1.Name = "OpenToolStripMenuItem1"
        Me.OpenToolStripMenuItem1.Size = New System.Drawing.Size(166, 22)
        Me.OpenToolStripMenuItem1.Text = "Open"
        '
        'ExplorerToolStripMenuItem1
        '
        Me.ExplorerToolStripMenuItem1.Name = "ExplorerToolStripMenuItem1"
        Me.ExplorerToolStripMenuItem1.Size = New System.Drawing.Size(166, 22)
        Me.ExplorerToolStripMenuItem1.Text = "Explore"
        '
        'ToolStripSeparator17
        '
        Me.ToolStripSeparator17.Name = "ToolStripSeparator17"
        Me.ToolStripSeparator17.Size = New System.Drawing.Size(163, 6)
        '
        'SharingAndSecurityToolStripMenuItem
        '
        Me.SharingAndSecurityToolStripMenuItem.Name = "SharingAndSecurityToolStripMenuItem"
        Me.SharingAndSecurityToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.SharingAndSecurityToolStripMenuItem.Text = "Sharing and Security..."
        '
        'ToolStripSeparator18
        '
        Me.ToolStripSeparator18.Name = "ToolStripSeparator18"
        Me.ToolStripSeparator18.Size = New System.Drawing.Size(163, 6)
        '
        'FormatToolStripMenuItem
        '
        Me.FormatToolStripMenuItem.Name = "FormatToolStripMenuItem"
        Me.FormatToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.FormatToolStripMenuItem.Text = "Format..."
        '
        'ToolStripSeparator19
        '
        Me.ToolStripSeparator19.Name = "ToolStripSeparator19"
        Me.ToolStripSeparator19.Size = New System.Drawing.Size(163, 6)
        '
        'PropertiesToolStripMenuItem1
        '
        Me.PropertiesToolStripMenuItem1.Name = "PropertiesToolStripMenuItem1"
        Me.PropertiesToolStripMenuItem1.Size = New System.Drawing.Size(166, 22)
        Me.PropertiesToolStripMenuItem1.Text = "Properties"
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "folder.png")
        Me.ImageList1.Images.SetKeyName(1, "favorite 32.png")
        '
        'TreeView1
        '
        Me.TreeView1.Location = New System.Drawing.Point(564, 0)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(121, 52)
        Me.TreeView1.TabIndex = 5
        Me.TreeView1.Visible = False
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.SplitContainer1.IsSplitterFixed = True
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 80)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.Panel1)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.ListView1)
        Me.SplitContainer1.Size = New System.Drawing.Size(685, 314)
        Me.SplitContainer1.SplitterDistance = 206
        Me.SplitContainer1.SplitterWidth = 1
        Me.SplitContainer1.TabIndex = 6
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(111, Byte), Integer), CType(CType(140, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(206, 314)
        Me.Panel1.TabIndex = 0
        '
        'AdvacnedToolStripMenuItem
        '
        Me.AdvacnedToolStripMenuItem.Name = "AdvacnedToolStripMenuItem"
        Me.AdvacnedToolStripMenuItem.Size = New System.Drawing.Size(170, 22)
        Me.AdvacnedToolStripMenuItem.Text = "Advacned"
        '
        'browser
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(685, 416)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.TreeView1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.ToolStrip2)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "browser"
        Me.Text = "browser"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.browsersettingsmenu.ResumeLayout(False)
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.ToolStrip2.ResumeLayout(False)
        Me.ToolStrip2.PerformLayout()
        Me.gobutton.ResumeLayout(False)
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.noitemselect.ResumeLayout(False)
        Me.fileitemselect.ResumeLayout(False)
        Me.computermenu.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ViewToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FavoritesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HelpToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripSplitButton1 As System.Windows.Forms.ToolStripSplitButton
    Friend WithEvents ToolStripSplitButton2 As System.Windows.Forms.ToolStripSplitButton
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripButton2 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton3 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripButton4 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton5 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton6 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton7 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents IsThisOSLegalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AboutOSToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MapNetworkDriveToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DisconnectNetworkDriveToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents FolderOptionsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AddToFavoritesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OrganizeFavoritesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStrip2 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripLabel1 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripButton8 As System.Windows.Forms.ToolStripButton
    Friend WithEvents browsersettingsmenu As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents StandardButtonsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AddressBarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LinksToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents LockTheToolbarsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CustomizeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents shellimages16 As System.Windows.Forms.ImageList
    Friend WithEvents ToolbarsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StatusBarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExplorerBarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents LargeIconsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SmallIconsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DetailsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator8 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents GotoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RefreshToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UndoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator9 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CopyToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PasteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PasteShortcutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator10 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CopyToFolderToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MoveToFolderToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator11 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents SelectAllToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InvertSelectionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CloseToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents ListView1 As System.Windows.Forms.ListView
    Friend WithEvents ToolStripComboBox1 As System.Windows.Forms.ToolStripComboBox
    Friend WithEvents shellimages32 As System.Windows.Forms.ImageList
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents favorites16 As System.Windows.Forms.ImageList
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents noitemselect As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ArrangeIconsByToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RefreshToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator12 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents PasteToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PasteShortcutToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator13 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents NewToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents fileitemselect As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents OpenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExplorerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator14 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CutToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CopyToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator15 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CreateShortcutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeleteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RenameToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator16 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents PropertiesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripDropDownButton1 As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents LargeImageToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SmallIconsToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DetailsToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents computermenu As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents OpenToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExplorerToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator17 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents SharingAndSecurityToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator18 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents FormatToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator19 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents PropertiesToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FolderToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ShortcutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator20 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BitmapImageToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RichTextDocumentToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TextDocumentToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ViewsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LargeIconsToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SmallIconsToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DetailsToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents ToolStripSeparator21 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents OtherToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DebugLogToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FolderImageToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents gobutton As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents StandardButtonToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AddressBarToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LinksToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator22 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents LockTheToolbarsToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GoButtonToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
    Friend WithEvents CheckForUpdatesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents LaunchProgramToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SearchToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FavoritesToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HistoryToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FoldersToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator23 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents TipOfTheDayToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AscendingToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DescendingToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator24 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IPScannerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AdvacnedToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
