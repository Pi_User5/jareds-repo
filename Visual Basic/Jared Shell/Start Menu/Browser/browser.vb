﻿Imports System.IO
Public Class browser
    Declare Auto Function SetParent Lib "user32.dll" (ByVal hWndChild As IntPtr, ByVal hWndNewParent As IntPtr) As Integer
    Declare Auto Function SendMessage Lib "user32.dll" (ByVal hWnd As IntPtr, ByVal Msg As Integer, ByVal wParam As Integer, ByVal lParam As Integer) As Integer
    Private Const WM_SYSCOMMAND As Integer = 274
    Private Const SC_MAXIMIZE As Integer = 61488
    Dim proc As Process
    Private cbleft As Integer

    Private Sub browser_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call shellbrowser.GetFavorties()
        Call shellbrowser.ParseURL()
        Call shellbrowser.SetListViewImage()
        Call shellbrowser.AddExplorerSideBar()
        'Call shellbrowser.GetToolBarItems() 'Does not get toolbar images :(
        ToolStripComboBox1.AutoSize = False
        cbleft = ToolStripComboBox1.Bounds.Left
    End Sub
    Private Sub ToolStip2_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ToolStrip2.SizeChanged
        ToolStripComboBox1.Width = ToolStrip2.Width - cbleft - 50
    End Sub
    Private Sub AboutOSToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AboutOSToolStripMenuItem.Click
        If System.IO.File.Exists("C:\Windows\system32\winver.exe") Then
            Process.Start("C:\Windows\system32\winver.exe")
        End If
    End Sub

    Private Sub MapNetworkDriveToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MapNetworkDriveToolStripMenuItem.Click
        Shell("rundll32.exe shell32.dll,SHHelpShortcuts_RunDLL Connect")
    End Sub

    Private Sub DisconnectNetworkDriveToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DisconnectNetworkDriveToolStripMenuItem.Click
        If My.Computer.Info.OSVersion > "6.0" Then
            'What do we do here?!
            MsgBox("MS decided to remove the command to show the Disconnect Network drive dialog")
        End If
        If My.Computer.Info.OSVersion < "6.0" Then ' Windows XP and like
            Shell("rundll32.exe netplwiz.dll,SHDisconnectNetDrives")
        End If
    End Sub

    Private Sub FolderOptionsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FolderOptionsToolStripMenuItem.Click
        Shell("rundll32.exe shell32.dll,Options_RunDLL 0")
    End Sub

    Private Sub CloseToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CloseToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub CustomizeToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CustomizeToolStripMenuItem.Click
        toolbarcustomize.Show()
    End Sub
    Private Sub ToolStripComboBox1_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles ToolStripComboBox1.KeyDown
        If e.KeyCode = Keys.Enter Then
            ListView1.Focus()
            Call shellbrowser.ParseURL()
        End If
    End Sub

    Private Sub DetailsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DetailsToolStripMenuItem.Click
        Call functions.DetailsView()
    End Sub

    Private Sub ListToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListToolStripMenuItem.Click
        Call functions.ListView()
    End Sub

    Private Sub SmallIconsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SmallIconsToolStripMenuItem.Click
        Call functions.SmallView()
    End Sub

    Private Sub LargeIconsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LargeIconsToolStripMenuItem.Click
        Call functions.LargeView()
    End Sub

    Private Sub ToolStripButton6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton6.Click
        Call shellbrowser.Delete()
    End Sub
    Private Sub ListView1_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListView1.MouseDoubleClick
        Dim basepath As String = ToolStripComboBox1.Text & "\" & ListView1.FocusedItem.Text.ToString
        Dim filepath As String
        filepath = basepath.Replace("\\", "\")
        If ListView1.SelectedItems.Count > 0 Then 'See if listview has a selected item
            If File.Exists(filepath) Then 'Determine if item is a file
                Try 'Try to launch file
                    Process.Start(filepath)
                Catch ex As Exception
                    debugger.RichTextBox1.AppendText(ex.ToString) 'Add Exception
                    debugger.RichTextBox1.AppendText(Environment.NewLine + Environment.NewLine) 'Add New line
                End Try
            End If
            If ToolStripComboBox1.Text = "My Computer" Then
                ToolStripComboBox1.Text = ListView1.FocusedItem.Text.ToString
                Call shellbrowser.BrowseSystem()
            Else
                If Directory.Exists(filepath) Then
                    ToolStripComboBox1.Text = filepath
                    Call shellbrowser.BrowseSystem()
                End If
            End If
        End If
    End Sub
    Private Sub ListView1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListView1.SelectedIndexChanged
        If ListView1.SelectedItems.Count = 1 Then
            If ToolStripComboBox1.Text = "My Computer" Then
                ToolStripButton4.Enabled = False
                ToolStripButton5.Enabled = False
                ToolStripButton6.Enabled = False
                ToolStripButton7.Enabled = False
                CopyToFolderToolStripMenuItem.Enabled = False
                ListView1.ContextMenuStrip = computermenu
            Else
                ToolStripButton4.Enabled = True
                ToolStripButton5.Enabled = True
                ToolStripButton6.Enabled = True
                ToolStripButton7.Enabled = True
                CopyToFolderToolStripMenuItem.Enabled = True
                ListView1.ContextMenuStrip = fileitemselect
            End If
        Else
            ToolStripButton4.Enabled = False
            ToolStripButton5.Enabled = False
            ToolStripButton6.Enabled = False
            ToolStripButton7.Enabled = False
            CopyToFolderToolStripMenuItem.Enabled = False
            ListView1.ContextMenuStrip = noitemselect
        End If
    End Sub

    Private Sub ToolStripButton8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton8.Click
        Call shellbrowser.ParseURL()
    End Sub

    Private Sub OrganizeFavoritesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OrganizeFavoritesToolStripMenuItem.Click
        Call shellbrowser.OrganizeFavs()
    End Sub

    Private Sub PropertiesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PropertiesToolStripMenuItem.Click
        shellbrowser.Properties()
    End Sub

    Private Sub ToolStripButton5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton5.Click
        Call shellbrowser.FileCopy()
    End Sub

    Private Sub ToolStripSplitButton1_ButtonClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripSplitButton1.ButtonClick
        Call shellbrowser.GoBack()
    End Sub

    Private Sub ToolStripSplitButton2_ButtonClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripSplitButton2.ButtonClick
        Call shellbrowser.GoForward()
    End Sub

    Private Sub LockTheToolbarsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LockTheToolbarsToolStripMenuItem.Click
        If LockTheToolbarsToolStripMenuItem.Checked = True Then
            Call shellbrowser.LockToolBar()
        End If
        If LockTheToolbarsToolStripMenuItem.Checked = False Then
            Call shellbrowser.UnlockToolBar()
        End If
    End Sub

    Private Sub RenameToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RenameToolStripMenuItem.Click
        Call shellbrowser.RenameFile()
    End Sub

    Private Sub PropertiesToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PropertiesToolStripMenuItem1.Click
        shellbrowser.ComputerProperties()
    End Sub

    Private Sub LargeImageToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LargeImageToolStripMenuItem.Click
        Call functions.LargeView()
    End Sub

    Private Sub ListToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListToolStripMenuItem1.Click
        Call functions.ListView()
    End Sub

    Private Sub SmallIconsToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SmallIconsToolStripMenuItem1.Click
        Call functions.SmallView()
    End Sub

    Private Sub DetailsToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DetailsToolStripMenuItem1.Click
        Call functions.DetailsView()
    End Sub

    Private Sub LargeIconsToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LargeIconsToolStripMenuItem1.Click
        Call functions.LargeView()
    End Sub

    Private Sub SmallIconsToolStripMenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SmallIconsToolStripMenuItem2.Click
        Call functions.SmallView()
    End Sub

    Private Sub ListToolStripMenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListToolStripMenuItem2.Click
        Call functions.ListView()
    End Sub

    Private Sub DetailsToolStripMenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DetailsToolStripMenuItem2.Click
        Call functions.DetailsView()
    End Sub
    Private Sub DebugLogToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DebugLogToolStripMenuItem.Click
        debugger.Show()
    End Sub

    Private Sub FolderImageToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FolderImageToolStripMenuItem.Click
        folderimage.Show()
    End Sub

    Private Sub DeleteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteToolStripMenuItem.Click
        Call shellbrowser.Delete()
    End Sub

    Private Sub IsThisOSLegalToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IsThisOSLegalToolStripMenuItem.Click
        Process.Start("https://bitbucket.org/Pi_User5/jareds-repo/wiki/Jared%20Shell")
    End Sub

    Private Sub SelectAllToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SelectAllToolStripMenuItem.Click
        For i = 0 To ListView1.Items.Count - 1
            ListView1.Items(i).Selected = True
        Next i
    End Sub

    Private Sub CopyToFolderToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CopyToFolderToolStripMenuItem.Click
        Call shellbrowser.FileCopy()
    End Sub
    Private Sub GoButtonToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GoButtonToolStripMenuItem.Click
        If GoButtonToolStripMenuItem.Checked = True Then
            ToolStripButton8.Visible = True
        Else
            ToolStripButton8.Visible = False
        End If
    End Sub

    Private Sub RefreshToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RefreshToolStripMenuItem1.Click
        Call shellbrowser.BrowseSystem()
    End Sub

    Private Sub RefreshToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RefreshToolStripMenuItem.Click
        Call shellbrowser.BrowseSystem()
    End Sub

    Private Sub CheckForUpdatesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckForUpdatesToolStripMenuItem.Click
        updater.Show()
    End Sub

    Private Sub ToolStripButton3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton3.Click
        Call foldersbar.PopulateTreeView()
        foldersbar.TopLevel = False
        Panel1.Controls.Add(foldersbar)
        foldersbar.Show()
        foldersbar.MaximizeBox = True
        foldersbar.TreeView1.TopNode.Expand()
    End Sub

    Private Sub StatusBarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StatusBarToolStripMenuItem.Click
        If StatusBarToolStripMenuItem.Checked = True Then
            StatusStrip1.Visible = True
        End If
        If StatusBarToolStripMenuItem.Checked = False Then
            StatusStrip1.Visible = False
        End If
    End Sub

    Private Sub FoldersToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FoldersToolStripMenuItem.Click
        If FoldersToolStripMenuItem.Checked = True Then
            Call foldersbar.PopulateTreeView()
            foldersbar.TopLevel = False
            Panel1.Controls.Add(foldersbar)
            foldersbar.Show()
            foldersbar.MaximizeBox = True
            foldersbar.TreeView1.TopNode.Expand()
        End If
        If FoldersToolStripMenuItem.Checked = False Then
            foldersbar.Close()
        End If
    End Sub

    Private Sub LaunchProgramToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LaunchProgramToolStripMenuItem.Click
        proc = Process.Start("C:\WINDOWS\notepad.exe")
        proc.WaitForInputIdle()

        SetParent(proc.MainWindowHandle, Panel1.Handle)
        SendMessage(proc.MainWindowHandle, WM_SYSCOMMAND, SC_MAXIMIZE, 0)
    End Sub

    Private Sub AscendingToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AscendingToolStripMenuItem.Click
        Call shellbrowser.IconsAscending()
    End Sub

    Private Sub DescendingToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DescendingToolStripMenuItem.Click
        Call shellbrowser.IconsDescending()
    End Sub

    Private Sub IPScannerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IPScannerToolStripMenuItem.Click
        ipscanner.Show()
    End Sub

    Private Sub AdvacnedToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AdvacnedToolStripMenuItem.Click
        advancedtoolsvb.Show()
    End Sub
End Class