﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class desktop
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(desktop))
        Me.ListView1 = New System.Windows.Forms.ListView()
        Me.noitemselect = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ArrangeIconsByToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LargeIconsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SmallIconsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RefreshToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.PasteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.NeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FolderToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ShortcutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.PropertiesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LargeIcons = New System.Windows.Forms.ImageList(Me.components)
        Me.SmallIcons = New System.Windows.Forms.ImageList(Me.components)
        Me.fileselected = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.OpenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OpenWithToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator()
        Me.SendToToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator13 = New System.Windows.Forms.ToolStripSeparator()
        Me.CutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CopyToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator()
        Me.CreateShortcutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeleteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RenameToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator()
        Me.PropertiesToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.folderselected = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.OpenToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExploreToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SearchToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator8 = New System.Windows.Forms.ToolStripSeparator()
        Me.SharingAndSecurityToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator9 = New System.Windows.Forms.ToolStripSeparator()
        Me.SendToToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator10 = New System.Windows.Forms.ToolStripSeparator()
        Me.CutToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CopyToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator11 = New System.Windows.Forms.ToolStripSeparator()
        Me.CreateShortcutToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeleteToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.RenameToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator12 = New System.Windows.Forms.ToolStripSeparator()
        Me.PropertiesToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.sendtomenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.SendToImages = New System.Windows.Forms.ImageList(Me.components)
        Me.noitemselect.SuspendLayout()
        Me.fileselected.SuspendLayout()
        Me.folderselected.SuspendLayout()
        Me.SuspendLayout()
        '
        'ListView1
        '
        Me.ListView1.Alignment = System.Windows.Forms.ListViewAlignment.Left
        Me.ListView1.BackColor = System.Drawing.Color.White
        Me.ListView1.ContextMenuStrip = Me.noitemselect
        Me.ListView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ListView1.ForeColor = System.Drawing.Color.White
        Me.ListView1.LargeImageList = Me.LargeIcons
        Me.ListView1.Location = New System.Drawing.Point(0, 0)
        Me.ListView1.Name = "ListView1"
        Me.ListView1.Scrollable = False
        Me.ListView1.Size = New System.Drawing.Size(757, 458)
        Me.ListView1.SmallImageList = Me.SmallIcons
        Me.ListView1.TabIndex = 0
        Me.ListView1.UseCompatibleStateImageBehavior = False
        '
        'noitemselect
        '
        Me.noitemselect.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ArrangeIconsByToolStripMenuItem, Me.ViewToolStripMenuItem, Me.RefreshToolStripMenuItem, Me.ToolStripSeparator1, Me.PasteToolStripMenuItem, Me.ToolStripSeparator2, Me.NeToolStripMenuItem, Me.ToolStripSeparator3, Me.PropertiesToolStripMenuItem})
        Me.noitemselect.Name = "noitemselect"
        Me.noitemselect.ShowImageMargin = False
        Me.noitemselect.Size = New System.Drawing.Size(139, 154)
        '
        'ArrangeIconsByToolStripMenuItem
        '
        Me.ArrangeIconsByToolStripMenuItem.Name = "ArrangeIconsByToolStripMenuItem"
        Me.ArrangeIconsByToolStripMenuItem.Size = New System.Drawing.Size(138, 22)
        Me.ArrangeIconsByToolStripMenuItem.Text = "Arrange Icons By"
        '
        'ViewToolStripMenuItem
        '
        Me.ViewToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LargeIconsToolStripMenuItem, Me.SmallIconsToolStripMenuItem})
        Me.ViewToolStripMenuItem.Name = "ViewToolStripMenuItem"
        Me.ViewToolStripMenuItem.Size = New System.Drawing.Size(138, 22)
        Me.ViewToolStripMenuItem.Text = "View"
        '
        'LargeIconsToolStripMenuItem
        '
        Me.LargeIconsToolStripMenuItem.Name = "LargeIconsToolStripMenuItem"
        Me.LargeIconsToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me.LargeIconsToolStripMenuItem.Text = "Large Icons"
        '
        'SmallIconsToolStripMenuItem
        '
        Me.SmallIconsToolStripMenuItem.Name = "SmallIconsToolStripMenuItem"
        Me.SmallIconsToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me.SmallIconsToolStripMenuItem.Text = "Small Icons"
        '
        'RefreshToolStripMenuItem
        '
        Me.RefreshToolStripMenuItem.Name = "RefreshToolStripMenuItem"
        Me.RefreshToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5
        Me.RefreshToolStripMenuItem.ShowShortcutKeys = False
        Me.RefreshToolStripMenuItem.Size = New System.Drawing.Size(138, 22)
        Me.RefreshToolStripMenuItem.Text = "Refresh"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(135, 6)
        '
        'PasteToolStripMenuItem
        '
        Me.PasteToolStripMenuItem.Name = "PasteToolStripMenuItem"
        Me.PasteToolStripMenuItem.Size = New System.Drawing.Size(138, 22)
        Me.PasteToolStripMenuItem.Text = "Paste"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(135, 6)
        '
        'NeToolStripMenuItem
        '
        Me.NeToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FolderToolStripMenuItem, Me.ShortcutToolStripMenuItem, Me.ToolStripSeparator4})
        Me.NeToolStripMenuItem.Name = "NeToolStripMenuItem"
        Me.NeToolStripMenuItem.Size = New System.Drawing.Size(138, 22)
        Me.NeToolStripMenuItem.Text = "New"
        '
        'FolderToolStripMenuItem
        '
        Me.FolderToolStripMenuItem.Image = CType(resources.GetObject("FolderToolStripMenuItem.Image"), System.Drawing.Image)
        Me.FolderToolStripMenuItem.Name = "FolderToolStripMenuItem"
        Me.FolderToolStripMenuItem.Size = New System.Drawing.Size(119, 22)
        Me.FolderToolStripMenuItem.Text = "Folder"
        '
        'ShortcutToolStripMenuItem
        '
        Me.ShortcutToolStripMenuItem.Image = CType(resources.GetObject("ShortcutToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ShortcutToolStripMenuItem.Name = "ShortcutToolStripMenuItem"
        Me.ShortcutToolStripMenuItem.Size = New System.Drawing.Size(119, 22)
        Me.ShortcutToolStripMenuItem.Text = "Shortcut"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(116, 6)
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(135, 6)
        '
        'PropertiesToolStripMenuItem
        '
        Me.PropertiesToolStripMenuItem.Name = "PropertiesToolStripMenuItem"
        Me.PropertiesToolStripMenuItem.Size = New System.Drawing.Size(138, 22)
        Me.PropertiesToolStripMenuItem.Text = "Properties"
        '
        'LargeIcons
        '
        Me.LargeIcons.ImageStream = CType(resources.GetObject("LargeIcons.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.LargeIcons.TransparentColor = System.Drawing.Color.Transparent
        Me.LargeIcons.Images.SetKeyName(0, "computer 32.png")
        Me.LargeIcons.Images.SetKeyName(1, "home 32.png")
        Me.LargeIcons.Images.SetKeyName(2, "network 32.png")
        Me.LargeIcons.Images.SetKeyName(3, "trash 32.png")
        Me.LargeIcons.Images.SetKeyName(4, "folder 32.png")
        '
        'SmallIcons
        '
        Me.SmallIcons.ImageStream = CType(resources.GetObject("SmallIcons.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.SmallIcons.TransparentColor = System.Drawing.Color.Transparent
        Me.SmallIcons.Images.SetKeyName(0, "computer 16.png")
        Me.SmallIcons.Images.SetKeyName(1, "home 16.png")
        Me.SmallIcons.Images.SetKeyName(2, "network 16.png")
        Me.SmallIcons.Images.SetKeyName(3, "trash 16.png")
        Me.SmallIcons.Images.SetKeyName(4, "folder 16.png")
        '
        'fileselected
        '
        Me.fileselected.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OpenToolStripMenuItem, Me.OpenWithToolStripMenuItem, Me.ToolStripSeparator5, Me.SendToToolStripMenuItem1, Me.ToolStripSeparator13, Me.CutToolStripMenuItem, Me.CopyToolStripMenuItem, Me.ToolStripSeparator6, Me.CreateShortcutToolStripMenuItem, Me.DeleteToolStripMenuItem, Me.RenameToolStripMenuItem, Me.ToolStripSeparator7, Me.PropertiesToolStripMenuItem1})
        Me.fileselected.Name = "fileselected"
        Me.fileselected.ShowImageMargin = False
        Me.fileselected.Size = New System.Drawing.Size(132, 226)
        '
        'OpenToolStripMenuItem
        '
        Me.OpenToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpenToolStripMenuItem.Name = "OpenToolStripMenuItem"
        Me.OpenToolStripMenuItem.Size = New System.Drawing.Size(131, 22)
        Me.OpenToolStripMenuItem.Text = "Open"
        '
        'OpenWithToolStripMenuItem
        '
        Me.OpenWithToolStripMenuItem.Name = "OpenWithToolStripMenuItem"
        Me.OpenWithToolStripMenuItem.Size = New System.Drawing.Size(131, 22)
        Me.OpenWithToolStripMenuItem.Text = "Open With"
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(128, 6)
        '
        'SendToToolStripMenuItem1
        '
        Me.SendToToolStripMenuItem1.DropDown = Me.sendtomenu
        Me.SendToToolStripMenuItem1.Name = "SendToToolStripMenuItem1"
        Me.SendToToolStripMenuItem1.Size = New System.Drawing.Size(131, 22)
        Me.SendToToolStripMenuItem1.Text = "Send To"
        '
        'ToolStripSeparator13
        '
        Me.ToolStripSeparator13.Name = "ToolStripSeparator13"
        Me.ToolStripSeparator13.Size = New System.Drawing.Size(128, 6)
        '
        'CutToolStripMenuItem
        '
        Me.CutToolStripMenuItem.Name = "CutToolStripMenuItem"
        Me.CutToolStripMenuItem.Size = New System.Drawing.Size(131, 22)
        Me.CutToolStripMenuItem.Text = "Cut"
        '
        'CopyToolStripMenuItem
        '
        Me.CopyToolStripMenuItem.Name = "CopyToolStripMenuItem"
        Me.CopyToolStripMenuItem.Size = New System.Drawing.Size(131, 22)
        Me.CopyToolStripMenuItem.Text = "Copy"
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(128, 6)
        '
        'CreateShortcutToolStripMenuItem
        '
        Me.CreateShortcutToolStripMenuItem.Name = "CreateShortcutToolStripMenuItem"
        Me.CreateShortcutToolStripMenuItem.Size = New System.Drawing.Size(131, 22)
        Me.CreateShortcutToolStripMenuItem.Text = "Create Shortcut"
        '
        'DeleteToolStripMenuItem
        '
        Me.DeleteToolStripMenuItem.Name = "DeleteToolStripMenuItem"
        Me.DeleteToolStripMenuItem.Size = New System.Drawing.Size(131, 22)
        Me.DeleteToolStripMenuItem.Text = "Delete"
        '
        'RenameToolStripMenuItem
        '
        Me.RenameToolStripMenuItem.Name = "RenameToolStripMenuItem"
        Me.RenameToolStripMenuItem.Size = New System.Drawing.Size(131, 22)
        Me.RenameToolStripMenuItem.Text = "Rename"
        '
        'ToolStripSeparator7
        '
        Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
        Me.ToolStripSeparator7.Size = New System.Drawing.Size(128, 6)
        '
        'PropertiesToolStripMenuItem1
        '
        Me.PropertiesToolStripMenuItem1.Name = "PropertiesToolStripMenuItem1"
        Me.PropertiesToolStripMenuItem1.Size = New System.Drawing.Size(131, 22)
        Me.PropertiesToolStripMenuItem1.Text = "Properties"
        '
        'folderselected
        '
        Me.folderselected.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OpenToolStripMenuItem1, Me.ExploreToolStripMenuItem, Me.SearchToolStripMenuItem, Me.ToolStripSeparator8, Me.SharingAndSecurityToolStripMenuItem, Me.ToolStripSeparator9, Me.SendToToolStripMenuItem, Me.ToolStripSeparator10, Me.CutToolStripMenuItem1, Me.CopyToolStripMenuItem1, Me.ToolStripSeparator11, Me.CreateShortcutToolStripMenuItem1, Me.DeleteToolStripMenuItem1, Me.RenameToolStripMenuItem1, Me.ToolStripSeparator12, Me.PropertiesToolStripMenuItem2})
        Me.folderselected.Name = "folderselected"
        Me.folderselected.ShowImageMargin = False
        Me.folderselected.Size = New System.Drawing.Size(167, 298)
        '
        'OpenToolStripMenuItem1
        '
        Me.OpenToolStripMenuItem1.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpenToolStripMenuItem1.Name = "OpenToolStripMenuItem1"
        Me.OpenToolStripMenuItem1.Size = New System.Drawing.Size(166, 22)
        Me.OpenToolStripMenuItem1.Text = "Open"
        '
        'ExploreToolStripMenuItem
        '
        Me.ExploreToolStripMenuItem.Name = "ExploreToolStripMenuItem"
        Me.ExploreToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.ExploreToolStripMenuItem.Text = "Explore"
        '
        'SearchToolStripMenuItem
        '
        Me.SearchToolStripMenuItem.Name = "SearchToolStripMenuItem"
        Me.SearchToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.SearchToolStripMenuItem.Text = "Search..."
        '
        'ToolStripSeparator8
        '
        Me.ToolStripSeparator8.Name = "ToolStripSeparator8"
        Me.ToolStripSeparator8.Size = New System.Drawing.Size(163, 6)
        '
        'SharingAndSecurityToolStripMenuItem
        '
        Me.SharingAndSecurityToolStripMenuItem.Name = "SharingAndSecurityToolStripMenuItem"
        Me.SharingAndSecurityToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.SharingAndSecurityToolStripMenuItem.Text = "Sharing and Security..."
        '
        'ToolStripSeparator9
        '
        Me.ToolStripSeparator9.Name = "ToolStripSeparator9"
        Me.ToolStripSeparator9.Size = New System.Drawing.Size(163, 6)
        '
        'SendToToolStripMenuItem
        '
        Me.SendToToolStripMenuItem.DropDown = Me.sendtomenu
        Me.SendToToolStripMenuItem.Name = "SendToToolStripMenuItem"
        Me.SendToToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.SendToToolStripMenuItem.Text = "Send To"
        '
        'ToolStripSeparator10
        '
        Me.ToolStripSeparator10.Name = "ToolStripSeparator10"
        Me.ToolStripSeparator10.Size = New System.Drawing.Size(163, 6)
        '
        'CutToolStripMenuItem1
        '
        Me.CutToolStripMenuItem1.Name = "CutToolStripMenuItem1"
        Me.CutToolStripMenuItem1.Size = New System.Drawing.Size(166, 22)
        Me.CutToolStripMenuItem1.Text = "Cut"
        '
        'CopyToolStripMenuItem1
        '
        Me.CopyToolStripMenuItem1.Name = "CopyToolStripMenuItem1"
        Me.CopyToolStripMenuItem1.Size = New System.Drawing.Size(166, 22)
        Me.CopyToolStripMenuItem1.Text = "Copy"
        '
        'ToolStripSeparator11
        '
        Me.ToolStripSeparator11.Name = "ToolStripSeparator11"
        Me.ToolStripSeparator11.Size = New System.Drawing.Size(163, 6)
        '
        'CreateShortcutToolStripMenuItem1
        '
        Me.CreateShortcutToolStripMenuItem1.Name = "CreateShortcutToolStripMenuItem1"
        Me.CreateShortcutToolStripMenuItem1.Size = New System.Drawing.Size(166, 22)
        Me.CreateShortcutToolStripMenuItem1.Text = "Create Shortcut"
        '
        'DeleteToolStripMenuItem1
        '
        Me.DeleteToolStripMenuItem1.Name = "DeleteToolStripMenuItem1"
        Me.DeleteToolStripMenuItem1.Size = New System.Drawing.Size(166, 22)
        Me.DeleteToolStripMenuItem1.Text = "Delete"
        '
        'RenameToolStripMenuItem1
        '
        Me.RenameToolStripMenuItem1.Name = "RenameToolStripMenuItem1"
        Me.RenameToolStripMenuItem1.Size = New System.Drawing.Size(166, 22)
        Me.RenameToolStripMenuItem1.Text = "Rename"
        '
        'ToolStripSeparator12
        '
        Me.ToolStripSeparator12.Name = "ToolStripSeparator12"
        Me.ToolStripSeparator12.Size = New System.Drawing.Size(163, 6)
        '
        'PropertiesToolStripMenuItem2
        '
        Me.PropertiesToolStripMenuItem2.Name = "PropertiesToolStripMenuItem2"
        Me.PropertiesToolStripMenuItem2.Size = New System.Drawing.Size(166, 22)
        Me.PropertiesToolStripMenuItem2.Text = "Properties"
        '
        'sendtomenu
        '
        Me.sendtomenu.Name = "sendtomenu"
        Me.sendtomenu.Size = New System.Drawing.Size(61, 4)
        '
        'SendToImages
        '
        Me.SendToImages.ImageStream = CType(resources.GetObject("SendToImages.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.SendToImages.TransparentColor = System.Drawing.Color.Transparent
        Me.SendToImages.Images.SetKeyName(0, "fixed.png")
        Me.SendToImages.Images.SetKeyName(1, "removeable.png")
        Me.SendToImages.Images.SetKeyName(2, "network.png")
        Me.SendToImages.Images.SetKeyName(3, "network_off.png")
        Me.SendToImages.Images.SetKeyName(4, "ram.png")
        Me.SendToImages.Images.SetKeyName(5, "zip.png")
        Me.SendToImages.Images.SetKeyName(6, "desktop.png")
        Me.SendToImages.Images.SetKeyName(7, "home.png")
        '
        'desktop
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(757, 458)
        Me.Controls.Add(Me.ListView1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "desktop"
        Me.Text = "desktop"
        Me.noitemselect.ResumeLayout(False)
        Me.fileselected.ResumeLayout(False)
        Me.folderselected.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ListView1 As System.Windows.Forms.ListView
    Friend WithEvents LargeIcons As System.Windows.Forms.ImageList
    Friend WithEvents SmallIcons As System.Windows.Forms.ImageList
    Friend WithEvents noitemselect As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ArrangeIconsByToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ViewToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RefreshToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents PasteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents NeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents PropertiesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents fileselected As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents FolderToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ShortcutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents LargeIconsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SmallIconsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents folderselected As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents OpenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OpenWithToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CopyToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CreateShortcutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeleteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RenameToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents PropertiesToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OpenToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExploreToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SendToToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator13 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents SearchToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator8 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents SharingAndSecurityToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator9 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents SendToToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator10 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CutToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CopyToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator11 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CreateShortcutToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeleteToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RenameToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator12 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents PropertiesToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents sendtomenu As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents SendToImages As System.Windows.Forms.ImageList
End Class
