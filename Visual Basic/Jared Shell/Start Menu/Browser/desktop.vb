﻿Imports Microsoft.Win32
Imports System.IO
Public Class desktop
    Dim regKey As Object = My.Computer.Registry.CurrentUser.OpenSubKey("Control Panel\Desktop", True).GetValue("Wallpaper")
    Dim myID As String = Environment.UserName
    Private Sub desktop_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ListView1.BackColor = Color.FromArgb(58, 110, 255)
        Call GetWallpaper()
        Call getItems()
        'Me.WindowState = FormWindowState.Maximized
    End Sub
    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)
        MyBase.Top = (0)
        MyBase.Left = Screen.PrimaryScreen.WorkingArea.Left
        Me.Height = Me.Height + 28
        Me.Width = Me.Width + My.Computer.Screen.WorkingArea.Right - (Me.Left + Me.Width)
    End Sub
    Private Sub GetWallpaper()
        If regKey Is Nothing Then
            ListView1.BackColor = Color.FromArgb(58, 110, 255)
        Else
            ListView1.BackgroundImage = System.Drawing.Image.FromFile(regKey)
        End If
    End Sub
    Private Sub getItems()
        ListView1.Clear()
        Dim path As String
        If My.Computer.Info.OSVersion > "6.0" Then 'Windows Vista and newer
            Dim dir As String = ("C:\Users\" & myID & "\Desktop")
            path = dir
            For i As Integer = LargeIcons.Images.Count - 1 To 1 Step -1
                SmallIcons.Images(i).Dispose()
                LargeIcons.Images(i).Dispose()
            Next
            ListView1.Items.Add("My Computer", imageIndex:=0)
            ListView1.Items.Add(myID, imageIndex:=1)
            ListView1.Items.Add("Network", imageIndex:=2)
            ListView1.Items.Add("Recycle Bin", imageIndex:=3)
            If Directory.Exists(path) Then 'Check if folder exists
                Try
                    browser.ToolStripComboBox1.Text = path 'Update the address bar
                    Dim treenode As New TreeNode(path)
                    For Each dirname As String In IO.Directory.GetDirectories(path) 'Loop through folders in the folder
                        Dim folders As List(Of String) = New List(Of String) 'Create New list
                        folders.Add(IO.Path.GetFileName(dirname).ToString) 'Add folders to list
                        folders.Sort() 'Sort folders A-Z
                        For Each element As String In folders 'Loop through folders
                            Dim lvi As ListViewItem = ListView1.Items.Add(IO.Path.GetFileName(dirname).ToString, imageIndex:=4) 'Add folders
                        Next
                    Next
                Catch ex As Exception
                    debugger.RichTextBox1.AppendText(ex.ToString) 'Add Exception
                    debugger.RichTextBox1.AppendText(Environment.NewLine + Environment.NewLine) 'Add New line
                End Try
            End If
            Try
                Dim di As New IO.DirectoryInfo(path) 'Folder being browsed
                Dim diar1 As IO.FileInfo() = di.GetFiles()
                Dim dra As System.IO.FileInfo
                For Each dra In diar1
                    Dim files As List(Of String) = New List(Of String) 'Create New list
                    files.Add(IO.Path.GetFileName(dra.ToString).ToString) 'Add folders to list
                    files.Sort() 'Sort folders A-Z
                    For Each element As String In files 'Loop through folders
                        SmallIcons.Images.Add(Icon.ExtractAssociatedIcon(dra.FullName))
                        LargeIcons.Images.Add(Icon.ExtractAssociatedIcon(dra.FullName))
                        ListView1.Items.Add(dra.ToString, LargeIcons.Images.Count - 1)
                    Next
                Next
            Catch ex As Exception
                debugger.RichTextBox1.AppendText(ex.ToString) 'Add Exception
                debugger.RichTextBox1.AppendText(Environment.NewLine + Environment.NewLine) 'Add New line
            End Try
        End If
        If My.Computer.Info.OSVersion < "6.0" Then ' Windows XP and like
            Dim dir As String = ("C:\Documents and Settings\" & myID & "\Desktop")
            path = dir
            For i As Integer = LargeIcons.Images.Count - 1 To 1 Step -1
                SmallIcons.Images(i).Dispose()
                LargeIcons.Images(i).Dispose()
            Next
            ListView1.Items.Add("My Computer", imageIndex:=0)
            ListView1.Items.Add("My Documents", imageIndex:=1)
            ListView1.Items.Add("My Network Places", imageIndex:=2)
            ListView1.Items.Add("Recycle Bin", imageIndex:=3)
            If Directory.Exists(path) Then 'Check if folder exists
                Try
                    browser.ToolStripComboBox1.Text = path 'Update the address bar
                    Dim treenode As New TreeNode(path)
                    For Each dirname As String In IO.Directory.GetDirectories(path) 'Loop through folders in the folder
                        Dim folders As List(Of String) = New List(Of String) 'Create New list
                        folders.Add(IO.Path.GetFileName(dirname).ToString) 'Add folders to list
                        folders.Sort() 'Sort folders A-Z
                        For Each element As String In folders 'Loop through folders
                            Dim lvi As ListViewItem = ListView1.Items.Add(IO.Path.GetFileName(dirname).ToString, imageIndex:=4) 'Add folders
                        Next
                    Next
                Catch ex As Exception
                    debugger.RichTextBox1.AppendText(ex.ToString) 'Add Exception
                    debugger.RichTextBox1.AppendText(Environment.NewLine + Environment.NewLine) 'Add New line
                End Try
            End If
            Try
                Dim di As New IO.DirectoryInfo(path) 'Folder being browsed
                Dim diar1 As IO.FileInfo() = di.GetFiles()
                Dim dra As System.IO.FileInfo
                For Each dra In diar1
                    Dim files As List(Of String) = New List(Of String) 'Create New list
                    files.Add(IO.Path.GetFileName(dra.ToString).ToString) 'Add folders to list
                    files.Sort() 'Sort folders A-Z
                    For Each element As String In files 'Loop through folders
                        SmallIcons.Images.Add(Icon.ExtractAssociatedIcon(dra.FullName))
                        LargeIcons.Images.Add(Icon.ExtractAssociatedIcon(dra.FullName))
                        ListView1.Items.Add(dra.ToString, LargeIcons.Images.Count - 1)
                    Next
                Next
            Catch ex As Exception
                debugger.RichTextBox1.AppendText(ex.ToString) 'Add Exception
                debugger.RichTextBox1.AppendText(Environment.NewLine + Environment.NewLine) 'Add New line
            End Try
        End If
    End Sub

    Private Sub PropertiesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PropertiesToolStripMenuItem.Click
        If File.Exists("C:\Windows\system32\desk.cpl") Then
            Process.Start("C:\Windows\system32\desk.cpl")
        End If
    End Sub

    Private Sub LargeIconsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LargeIconsToolStripMenuItem.Click
        ListView1.View = View.LargeIcon
    End Sub

    Private Sub SmallIconsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SmallIconsToolStripMenuItem.Click
        ListView1.View = View.SmallIcon
    End Sub

    Private Sub ListView1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListView1.SelectedIndexChanged
        If ListView1.SelectedItems.Count > 0 Then
            If My.Computer.Info.OSVersion > "6.0" Then 'Windows Vista and newer
                Dim dir As String = ("C:\Users\" & myID & "\Desktop")
                If System.IO.File.Exists(dir) Then
                    ListView1.ContextMenuStrip = fileselected
                End If
                If System.IO.Directory.Exists(dir) Then
                    ListView1.ContextMenuStrip = folderselected
                End If
            End If
            If My.Computer.Info.OSVersion < "6.0" Then ' Windows XP and like
                Dim dir As String = ("C:\Documents and Settings\" & myID & "\Desktop")
                If System.IO.File.Exists(dir) Then
                    ListView1.ContextMenuStrip = fileselected
                End If
                If System.IO.Directory.Exists(dir) Then
                    ListView1.ContextMenuStrip = folderselected
                End If
            End If
        Else
            ListView1.ContextMenuStrip = noitemselect
        End If
    End Sub
    Private Sub ListView1_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListView1.MouseDoubleClick
        Call DesktopDoubleClick()
    End Sub
    Private Sub DesktopDoubleClick()
        If ListView1.SelectedItems.Count > 0 Then 'See if listview has a selected item
            If My.Computer.Info.OSVersion > "6.0" Then ' Windows Vista and like
                Dim dir As String = ("C:\Documents and Settings\" & myID & "\Desktop\" & ListView1.FocusedItem.Text.ToString)
                If File.Exists(dir) Then 'Determine if item is a file
                    Try 'Try to launch file
                        Process.Start(dir)
                    Catch ex As Exception
                        debugger.RichTextBox1.AppendText(ex.ToString) 'Add Exception
                        debugger.RichTextBox1.AppendText(Environment.NewLine + Environment.NewLine) 'Add New line
                    End Try
                End If
                If Directory.Exists(dir) Then 'Determine if item is a file
                    Try 'Try to launch file
                        browser.ToolStripComboBox1.Text = dir
                        browser.Show()
                    Catch ex As Exception
                        debugger.RichTextBox1.AppendText(ex.ToString) 'Add Exception
                        debugger.RichTextBox1.AppendText(Environment.NewLine + Environment.NewLine) 'Add New line
                    End Try
                End If
            End If
            If My.Computer.Info.OSVersion < "6.0" Then ' Windows XP and like
                Dim dir As String = ("C:\Documents and Settings\" & myID & "\Desktop\" & ListView1.FocusedItem.Text.ToString)
                If File.Exists(dir) Then 'Determine if item is a file
                    Try 'Try to launch file
                        Process.Start(dir)
                    Catch ex As Exception
                        debugger.RichTextBox1.AppendText(ex.ToString) 'Add Exception
                        debugger.RichTextBox1.AppendText(Environment.NewLine + Environment.NewLine) 'Add New line
                    End Try
                End If
                If Directory.Exists(dir) Then 'Determine if item is a file
                    Try 'Try to launch file
                        browser.ToolStripComboBox1.Text = dir
                        browser.Show()
                    Catch ex As Exception
                        debugger.RichTextBox1.AppendText(ex.ToString) 'Add Exception
                        debugger.RichTextBox1.AppendText(Environment.NewLine + Environment.NewLine) 'Add New line
                    End Try
                End If
            End If
        End If
    End Sub
    Private Sub RefreshToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RefreshToolStripMenuItem.Click
        Call getItems()
    End Sub

    Private Sub OpenToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OpenToolStripMenuItem.Click
        Call DesktopDoubleClick()
    End Sub

    Private Sub OpenToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OpenToolStripMenuItem1.Click
        Call DesktopDoubleClick()
    End Sub

    Private Sub ExploreToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExploreToolStripMenuItem.Click
        Call DesktopDoubleClick()
    End Sub
End Class