﻿'Function: Code used for the file browser
'Programmer: Jared Smudde
Imports System
Imports System.IO
Imports Shell32
Imports System.Net.NetworkInformation

Module shellbrowser
    Dim favs As String = Environment.GetFolderPath(Environment.SpecialFolder.Favorites)
    Public Sub RetrunOSVersion()
        Dim osver As String = Environment.OSVersion.ToString()
        'MsgBox(osver)
        Select Case osver
            Case "5.1.2600.0" 'Windows XP
                'MsgBox("XP")
            Case "5.2.3790" 'Windows Server 2003
                'MsgBox("Server 2003")
                'Call SetToolbuttons()
            Case "6.0.6000" 'Windows Vista
                'MsgBox("Vista")
            Case "6.0.6001" 'Windows Server 2008
                'MsgBox("Server 2008")
            Case "6.0.6002" 'Windows Vista SP2
                'MsgBox("Vista SP2")
            Case "Microsoft Windows NT 6.1.7600.0" 'Windows 7 & Windows Server 2008 R2
                'MsgBox("7 and Server 2008 R2")
            Case "6.1.7601" 'Windows 7 SP1 & Windows Server 2008 R2, SP1
                'MsgBox("7 SP1 and Server 2008 R2 SP1")
            Case "6.1.8400" 'Windows Home Server 2011
                'MsgBox("Server 2011")
            Case "Microsoft Windows NT 6.2.9200" 'Windows 8 & Windows 8.1 & Windows Server 2012 & Windows Server 2012 R2
                'MsgBox("8")
            Case "6.3.9600" 'Windows 8.1 Update 1
                'MsgBox("Windows 8.1 Update 1")
            Case Else

        End Select
    End Sub
    Private Sub SetToolbuttons()
        browser.ToolStripSeparator2.Visible = True
        browser.ToolStripButton4.Visible = True
        browser.ToolStripButton5.Visible = True
        browser.ToolStripButton6.Visible = True
        browser.ToolStripButton7.Visible = True
    End Sub
    Public Sub GetFavorties()
        Dim myID As String
        myID = Environment.UserName()
        If My.Computer.Info.OSVersion > "6.0" Then 'Windows Vista and newer
            Dim di As New IO.DirectoryInfo("C:\Users\" & myID & "\Favorites")
            'Get Folders
            For Each subdi As DirectoryInfo In di.GetDirectories
                browser.FavoritesToolStripMenuItem.DropDownItems.Add(subdi.Name.ToString, browser.favorites16.Images(0))
            Next
            'Get Files
            Dim diar1 As IO.FileInfo() = di.GetFiles("*.lnk")
            Dim dra As System.IO.FileInfo
            For Each dra In diar1
                browser.FavoritesToolStripMenuItem.DropDownItems.Add(Path.GetFileNameWithoutExtension(dra.ToString), browser.favorites16.Images(1))
            Next
        End If
        If My.Computer.Info.OSVersion < "6.0" Then ' Windows XP and like
            Dim di As New IO.DirectoryInfo("C:\Documents and Settings\" & myID & "\Favorites")
            'Get Folders
            For Each subdi As DirectoryInfo In di.GetDirectories
                browser.FavoritesToolStripMenuItem.DropDownItems.Add(subdi.Name.ToString, browser.favorites16.Images(0))
            Next
            'Get Files
            Dim diar1 As IO.FileInfo() = di.GetFiles("*.url")
            Dim dra As IO.FileInfo
            For Each dra In diar1
                browser.FavoritesToolStripMenuItem.DropDownItems.Add(Path.GetFileNameWithoutExtension(dra.ToString), browser.favorites16.Images(1))
            Next
        End If
    End Sub
    Public Sub BrowseSystem()
        browser.ListView1.Items.Clear() 'Clear previous files and folders
        For i As Integer = browser.shellimages32.Images.Count - 1 To 1 Step -1
            browser.shellimages16.Images(i).Dispose()
            browser.shellimages32.Images(i).Dispose()
        Next
        Dim path As String = browser.ToolStripComboBox1.Text 'Get path from address box
        browser.ListView1.Focus() 'Focus to listbox
        My.Settings.lastfolder = path 'Save to settings current folder
        Dim treehistory As New TreeNode(path)
        browser.TreeView1.Nodes.Add(treehistory)
        browser.TreeView1.SelectedNode = treehistory
        If Directory.Exists(path) Then 'Check if folder exists
            Try
                browser.ToolStripComboBox1.Text = path 'Update the address bar
                Dim treenode As New TreeNode(path)
                For Each dirname As String In IO.Directory.GetDirectories(path) 'Loop through folders in the folder
                    Dim folders As List(Of String) = New List(Of String) 'Create New list
                    folders.Add(IO.Path.GetFileName(dirname).ToString) 'Add folders to list
                    folders.Sort() 'Sort folders A-Z
                    For Each element As String In folders 'Loop through folders
                        Dim lvi As ListViewItem = browser.ListView1.Items.Add(IO.Path.GetFileName(dirname).ToString, imageIndex:=0) 'Add folders
                    Next
                Next
            Catch ex As Exception
                debugger.RichTextBox1.AppendText(ex.ToString) 'Add Exception
                debugger.RichTextBox1.AppendText(Environment.NewLine + Environment.NewLine) 'Add New line
            End Try
        End If
        Try
            Dim di As New IO.DirectoryInfo(path) 'Folder being browsed
            Dim diar1 As IO.FileInfo() = di.GetFiles()
            Dim dra As System.IO.FileInfo
            For Each dra In diar1
                Dim files As List(Of String) = New List(Of String) 'Create New list
                files.Add(IO.Path.GetFileName(dra.ToString).ToString) 'Add folders to list
                files.Sort() 'Sort folders A-Z
                For Each element As String In files 'Loop through folders
                    browser.shellimages16.Images.Add(Icon.ExtractAssociatedIcon(dra.FullName))
                    browser.shellimages32.Images.Add(Icon.ExtractAssociatedIcon(dra.FullName))
                    browser.ListView1.Items.Add(dra.ToString, browser.shellimages32.Images.Count - 1)
                Next
            Next
        Catch ex As Exception
            debugger.RichTextBox1.AppendText(ex.ToString) 'Add Exception
            debugger.RichTextBox1.AppendText(Environment.NewLine + Environment.NewLine) 'Add New line
        End Try
        Call UpdateHeader()
        browser.ToolStripStatusLabel1.Text = browser.ListView1.Items.Count & " objects"
        browser.ListView1.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize)
    End Sub
    Public Sub Delete()
        Dim path As String
        path = browser.ToolStripComboBox1.Text & "\" & browser.ListView1.FocusedItem.Text.ToString
        If browser.ListView1.SelectedItems.Count > 0 Then
            If System.IO.File.Exists(path) = True Then
                If File.Exists(path) Then
                    Dim deldialog As New filedelete
                    deldialog.Label1.Text = "Are you sure you want to send '" & IO.Path.GetFileName(path) & "' to the Recycle Bin?"
                    If deldialog.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                        System.IO.File.Delete(path)
                        filedelete.Close()
                        browser.ListView1.FocusedItem.Remove()
                        browser.ToolStripStatusLabel1.Text = browser.ListView1.Items.Count & " objects"
                    End If
                End If
            End If
        End If
    End Sub
    Public Sub RenameFile()
        Dim path As String
        path = browser.ToolStripComboBox1.Text & "\" & browser.ListView1.FocusedItem.Text.ToString
        Dim newpath As String
        If browser.ListView1.SelectedItems.Count > 0 Then
            If System.IO.File.Exists(path) = True Then
                Dim renamedialog As New filerename
                renamedialog.TextBox1.Text = browser.ListView1.FocusedItem.Text.ToString
                If renamedialog.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                    newpath = renamedialog.TextBox1.Text
                    My.Computer.FileSystem.RenameFile(path, newpath)
                    Call shellbrowser.ParseURL()
                End If
            End If
        End If
    End Sub
    Public Sub ParseURL()
        Dim URL As String = browser.ToolStripComboBox1.Text
        Select Case URL
            Case "My Computer"
                Call MyComputer()
            Case "My Documents"

            Case Else
                If System.IO.Directory.Exists(browser.ToolStripComboBox1.Text) Then
                    Call BrowseSystem()
                Else
                    If browser.ToolStripComboBox1.Text = "" Then 'Handle for times when textbox may be empty
                    Else
                        MsgBox("Path not found", MsgBoxStyle.Critical)
                    End If
                End If
        End Select
    End Sub
    Public Sub OrganizeFavs()
        Dim favform As New browser
        favform.ToolStripComboBox1.Text = favs
        favform.Text = "Favorites"
        favform.Icon = My.Resources.favorites
        favform.ListView1.SmallImageList = browser.favorites16
        favform.ListView1.LargeImageList = browser.favorites16
        If Directory.Exists(favs) Then 'Check if folder exists
            Try
                For Each dirname As String In IO.Directory.GetDirectories(favs) 'Loop through folders in the folder
                    Dim lvi As ListViewItem = favform.ListView1.Items.Add(IO.Path.GetFileName(dirname).ToString, imageIndex:=0)
                Next
            Catch ex As Exception
                debugger.RichTextBox1.AppendText(ex.ToString) 'Add Exception
                debugger.RichTextBox1.AppendText(Environment.NewLine + Environment.NewLine) 'Add New line
            End Try
        End If
        Try
            Dim di As New IO.DirectoryInfo(favs) 'Folder being browsed
            Dim diar1 As IO.FileInfo() = di.GetFiles()
            Dim dra As System.IO.FileInfo
            For Each dra In diar1
                favform.ListView1.Items.Add(dra.ToString, imageIndex:=1)
            Next
        Catch ex As Exception
            debugger.RichTextBox1.AppendText(ex.ToString) 'Add Exception
            debugger.RichTextBox1.AppendText(Environment.NewLine + Environment.NewLine) 'Add New line
        End Try
        favform.Show()
    End Sub
    Private Sub UpdateHeader()
        Dim desktop As String = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
        Dim docs As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
        Select Case browser.ToolStripComboBox1.Text
            Case desktop
                browser.Icon = My.Resources.desktop
                browser.Text = "Desktop"
            Case docs
                browser.Icon = My.Resources.my_documents
                browser.Text = "My Documents"
            Case favs
                browser.Icon = My.Resources.favorites
                browser.Text = "Favorites"
            Case Else
                browser.Icon = My.Resources.folder
                browser.Text = browser.ToolStripComboBox1.Text
        End Select
    End Sub
    Public Sub Properties()
        Dim path As String
        path = browser.ToolStripComboBox1.Text & "\" & browser.ListView1.FocusedItem.Text.ToString
        'MsgBox(path)
        If File.Exists(path) Then
            fileproperties.PictureBox1.Image = Icon.ExtractAssociatedIcon(path).ToBitmap
            fileproperties.TextBox1.Text = IO.Path.GetFileName(path)
            fileproperties.Text = IO.Path.GetFileName(path) & " Properties"
            fileproperties.TextBox2.Text = path
            Dim infoReader As System.IO.FileInfo
            infoReader = My.Computer.FileSystem.GetFileInfo(path)
            fileproperties.Label6.Text = "Size:                 " & infoReader.Length.ToString & " bytes"
            fileproperties.Show()
        End If
    End Sub
    Public Sub ComputerProperties()
        If browser.ToolStripComboBox1.Text = "My Computer" Then
            driveproperties.PictureBox1.Image = browser.shellimages32.Images.Item(1)
            driveproperties.Text = browser.ListView1.FocusedItem.Text & " Properties"
            driveproperties.Show()
        End If
    End Sub
    Public Sub FileCopy()
        Dim path As String
        path = browser.ToolStripComboBox1.Text & "\" & browser.ListView1.FocusedItem.Text.ToString
        Dim copydialog As New copyto
        copydialog.TextBox1.Text = path
        If copydialog.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            My.Computer.FileSystem.CopyFile(copydialog.TextBox1.Text, copydialog.TextBox2.Text, FileIO.UIOption.AllDialogs)
        End If
    End Sub
    Public Sub NavigationButtonsSetup()
        My.Settings.currentfolder = browser.ToolStripComboBox1.Text

    End Sub
    Public Sub GoBack()

    End Sub
    Public Sub GoForward()

    End Sub

    Public Sub MyComputer()
        browser.ListView1.Clear()
        For Each d As System.IO.DriveInfo In My.Computer.FileSystem.Drives
            If d.DriveType = IO.DriveType.Fixed Then
                If d.IsReady = True Then
                    browser.ListView1.Items.Add(d.Name, imageIndex:=1)
                End If
            End If
        Next
        For Each d As System.IO.DriveInfo In My.Computer.FileSystem.Drives
            If d.DriveType = IO.DriveType.CDRom Then
                If d.IsReady = True Then
                    browser.ListView1.Items.Add(d.Name, imageIndex:=2)
                End If
            End If
        Next
        For Each d As System.IO.DriveInfo In My.Computer.FileSystem.Drives
            If d.DriveType = IO.DriveType.Removable Then
                If d.IsReady = True Then
                    browser.ListView1.Items.Add(d.Name, imageIndex:=3)
                End If
            End If
        Next
        For Each d As System.IO.DriveInfo In My.Computer.FileSystem.Drives
            If d.DriveType = IO.DriveType.Network Then
                If d.IsReady = True Then
                    browser.ListView1.Items.Add(d.Name, imageIndex:=4)
                End If
            End If
        Next
        For Each d As System.IO.DriveInfo In My.Computer.FileSystem.Drives
            If d.DriveType = IO.DriveType.Ram Then
                If d.IsReady = True Then
                    browser.ListView1.Items.Add(d.Name, imageIndex:=5)
                End If
            End If
        Next
        For Each d As System.IO.DriveInfo In My.Computer.FileSystem.Drives
            If d.DriveType = IO.DriveType.Unknown Then
                If d.IsReady = True Then
                    browser.ListView1.Items.Add(d.Name, imageIndex:=6)
                End If
            End If
        Next
        browser.ToolStripStatusLabel1.Text = browser.ListView1.Items.Count & " objects"
        browser.Text = "My Computer"
    End Sub
    Public Sub BrowseComputer()
        If browser.ToolStripComboBox1.Text = "My Computer" Then
            browser.ToolStripComboBox1.Text = browser.ListView1.FocusedItem.Text.ToString
        End If
    End Sub
    Public Sub LockToolBar()
        browser.ToolStrip1.GripStyle = ToolStripGripStyle.Hidden
        browser.ToolStrip2.GripStyle = ToolStripGripStyle.Hidden
    End Sub
    Public Sub UnlockToolBar()
        browser.ToolStrip1.GripStyle = ToolStripGripStyle.Visible
        browser.ToolStrip2.GripStyle = ToolStripGripStyle.Visible
    End Sub
    Public Sub TaskManager()
        If File.Exists("C:\Windows\system32\taskmgr.exe") Then
            Process.Start("C:\Windows\system32\taskmgr.exe")
        End If
    End Sub
    Public Sub LockTaskbar()
        taskbar.FormBorderStyle = FormBorderStyle.None
    End Sub
    Public Sub UnlockTaskbar()
        taskbar.FormBorderStyle = FormBorderStyle.SizableToolWindow
    End Sub
    Public Sub SetListViewImage()
        Dim image As String = My.Settings.backgroundimage
        If File.Exists(image) Then
            browser.ListView1.BackgroundImage = New System.Drawing.Bitmap(image)
        End If
    End Sub
    Public Sub BeginLog()
        debugger.RichTextBox1.AppendText("=~=~=~=~=~=~=~=~=~= Jared Shell =~=~=~=~=~=~=~=~=~=")
        debugger.RichTextBox1.AppendText(Environment.NewLine & "Running on " & Environment.OSVersion.ToString)
        debugger.RichTextBox1.AppendText(Environment.NewLine & "Started at " & DateAndTime.Now.ToString)
        debugger.RichTextBox1.AppendText(Environment.NewLine & "-----------------------------------------------------------------------------------------------------")
        debugger.RichTextBox1.AppendText(Environment.NewLine & Environment.NewLine)
    End Sub
    Public Sub MinimizeWindows()
        Dim sh As New Shell
        sh.MinimizeAll()
    End Sub
    Public Sub PopulateTaskbar()
        For Each proc As Process In Process.GetProcesses
            If Environment.Is64BitOperatingSystem = True Then
                If proc.MainWindowTitle <> "" Then
                    Try
                        taskbar.programspics.Images.Add(Icon.ExtractAssociatedIcon(proc.MainModule.FileName))
                        Dim lvi As New ListViewItem(proc.ProcessName, taskbar.programspics.Images.Count - 1)
                        lvi.SubItems.Add(proc.MainModule.FileName)
                        taskbar.ListView1.Items.Add(lvi)
                    Catch ex As Exception
                        debugger.RichTextBox1.AppendText(ex.ToString) 'Add Exception
                        debugger.RichTextBox1.AppendText(Environment.NewLine + Environment.NewLine) 'Add New line
                    End Try

                End If
            End If
            If Environment.Is64BitOperatingSystem = False Then
                If proc.MainWindowTitle <> "" Then
                    taskbar.programspics.Images.Add(Icon.ExtractAssociatedIcon(proc.MainModule.FileName))
                    Dim lvi As New ListViewItem(proc.ProcessName, taskbar.programspics.Images.Count - 1)
                    lvi.SubItems.Add(proc.MainModule.FileName)
                    taskbar.ListView1.Items.Add(lvi)
                End If
            End If
        Next
    End Sub
    Public Function GetPrimaryNic()
        ' DESCRIPTION: this function  will provide networking details for primary network card
        Dim PrimaryNic As New Collection
        Dim tt As New ToolTip()
        For Each networkCard As NetworkInterface In NetworkInterface.GetAllNetworkInterfaces
            ' Find network cards with gateway information (this may show more than one network card depending on computer)
            For Each gatewayAddr As GatewayIPAddressInformation In networkCard.GetIPProperties.GatewayAddresses
                ' if gateway address is NOT 0.0.0.0 and the network card status is UP then we've found the main network card
                If gatewayAddr.Address.ToString <> "0.0.0.0" And networkCard.OperationalStatus.ToString() = "Up" Then
                    taskbar.PictureBox10.Image = taskbar.networkIcons.Images.Item(1)
                    If networkCard.Speed / 1000000 < 99 Then

                    Else

                    End If
                Else
                End If
            Next
        Next
        Return PrimaryNic
    End Function
    Public Sub GetToolBarItems()
        toolbarcustomize.ListView2.Items.Clear()
        Dim item As ToolStripItem
        For Each item In browser.ToolStrip1.Items
            If item.Text = Nothing Then
                toolbarcustomize.ListView2.Items.Add("Separator")
            Else
                toolbarcustomize.ListView2.Items.Add(item.Text.ToString)
            End If
        Next
    End Sub
    Public Sub PopulateSendTo()
        'Drive List
        For Each d As System.IO.DriveInfo In My.Computer.FileSystem.Drives
            If d.DriveType = IO.DriveType.Ram Then
                If d.IsReady = True Then
                    Dim menuitem As New ToolStripMenuItem(d.VolumeLabel & " (" & d.Name & ")", desktop.SendToImages.Images.Item(4))
                    desktop.sendtomenu.Items.Insert(0, menuitem)
                End If
            End If
        Next
        For Each d As System.IO.DriveInfo In My.Computer.FileSystem.Drives
            If d.DriveType = IO.DriveType.Network Then
                If d.IsReady = True Then
                    Dim menuitem As New ToolStripMenuItem(d.VolumeLabel & " (" & d.Name & ")", desktop.SendToImages.Images.Item(2))
                    desktop.sendtomenu.Items.Insert(0, menuitem)
                End If
                If d.IsReady = False Then
                    Dim menuitem As New ToolStripMenuItem(d.VolumeLabel & " (" & d.Name & ")", desktop.SendToImages.Images.Item(3))
                    desktop.sendtomenu.Items.Insert(0, menuitem)
                End If
            End If
        Next
        For Each d As System.IO.DriveInfo In My.Computer.FileSystem.Drives
            If d.DriveType = IO.DriveType.Removable Then
                If d.IsReady = True Then
                    Dim menuitem As New ToolStripMenuItem(d.VolumeLabel & " (" & d.Name & ")", desktop.SendToImages.Images.Item(1))
                    desktop.sendtomenu.Items.Insert(0, menuitem)
                End If
            End If
        Next
        For Each d As System.IO.DriveInfo In My.Computer.FileSystem.Drives
            If d.DriveType = IO.DriveType.Fixed Then
                If d.IsReady = True Then
                    Dim menuitem As New ToolStripMenuItem(d.VolumeLabel & " (" & d.Name & ")", desktop.SendToImages.Images.Item(0))
                    desktop.sendtomenu.Items.Insert(0, menuitem)
                End If
            End If
        Next
        Dim homeitem As New ToolStripMenuItem("Documents", desktop.SendToImages.Images.Item(7))
        desktop.sendtomenu.Items.Insert(0, homeitem)
        Dim desktopitem As New ToolStripMenuItem("Desktop (create shortcut)", desktop.SendToImages.Images.Item(6))
        desktop.sendtomenu.Items.Insert(0, desktopitem)
        Dim zipitem As New ToolStripMenuItem("Compressed (zipped) folder", desktop.SendToImages.Images.Item(5))
        desktop.sendtomenu.Items.Insert(0, zipitem)
    End Sub
    Public Sub AddExplorerSideBar()
        explorersidebar.TopLevel = False
        browser.Panel1.Controls.Add(explorersidebar)
        explorersidebar.Show()
        explorersidebar.MaximizeBox = True
    End Sub
    Public Sub IconsAscending()
        browser.ListView1.Sorting = SortOrder.Ascending
    End Sub
    Public Sub IconsDescending()
        browser.ListView1.Sorting = SortOrder.Descending
    End Sub
End Module
