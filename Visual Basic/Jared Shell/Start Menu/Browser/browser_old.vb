﻿Imports System.Runtime.InteropServices
Imports System.IO
Public Class browser_old
    Dim myID As String = Environment.UserName
    Private Sub browser_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call CommandParser()
        SplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        ToolStripComboBox1.Anchor = AnchorStyles.Right
    End Sub
    Private Sub CommandParser()
        Dim value As String = ToolStripLabel1.Text
        Select Case value
            Case "computer"
                Call GetDrivesType()
            Case "documents"
                Call MyDocuments()
            Case "explorer"

            Case "recent"
                Call RecentFolder()

            Case Else
                Me.Icon = My.Resources.explorer
                Me.Text = "Jared Shell"
        End Select
    End Sub
    Public Sub GetDrivesType()
        For Each d As System.IO.DriveInfo In My.Computer.FileSystem.Drives
            If d.DriveType = IO.DriveType.Fixed Then
                ListView1.Items.Add(d.Name, imageIndex:=0)
            End If
        Next
        For Each d As System.IO.DriveInfo In My.Computer.FileSystem.Drives
            If d.DriveType = IO.DriveType.CDRom Then
                ListView1.Items.Add(d.Name, imageIndex:=1)
            End If
        Next
        For Each d As System.IO.DriveInfo In My.Computer.FileSystem.Drives
            If d.DriveType = IO.DriveType.Removable Then
                ListView1.Items.Add(d.Name, imageIndex:=2)
            End If
        Next
        For Each d As System.IO.DriveInfo In My.Computer.FileSystem.Drives
            If d.DriveType = IO.DriveType.Network Then
                ListView1.Items.Add(d.Name, imageIndex:=3)
            End If
        Next
        For Each d As System.IO.DriveInfo In My.Computer.FileSystem.Drives
            If d.DriveType = IO.DriveType.Ram Then
                ListView1.Items.Add(d.Name, imageIndex:=4)
            End If
        Next
        For Each d As System.IO.DriveInfo In My.Computer.FileSystem.Drives
            If d.DriveType = IO.DriveType.Unknown Then
                ListView1.Items.Add(d.Name, imageIndex:=5)
            End If
        Next
    End Sub
    Private Sub MyDocuments()
        If My.Computer.Info.OSVersion > "6.0" Then 'Windows Vista and newer
            Dim di As String = ("C:\Users\" & myID & "\Documents")
            ListDirectories(di)
            TreeView1.Nodes.Add(di)
        End If
        If My.Computer.Info.OSVersion < "6.0" Then ' Windows XP and like
            Dim di As String = ("C:\Documents and Settings\" & myID & "\My Documents")
            ListDirectories(di)
            TreeView1.Nodes.Add(di)
        End If
    End Sub
    Private Sub RecentFolder()
        Dim RecentPath As String
        RecentPath = Environment.GetFolderPath(Environment.SpecialFolder.Recent)
        ListDirectories(RecentPath)
        TreeView1.Nodes.Add(RecentPath)
    End Sub

    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        ListView1.Clear()
        Me.Text = "Desktop"
        Me.Icon = My.Resources.desktop
        If My.Computer.Info.OSVersion > "6.0" Then 'Windows Vista and newer
            Dim di As String = ("C:\Users\" & myID & "\Desktop")
            ListDirectories(di)
            TreeView1.Nodes.Add(di)
        End If
        If My.Computer.Info.OSVersion < "6.0" Then ' Windows XP and like
            Dim di As String = ("C:\Documents and Settings\" & myID & "\Desktop")
            ListDirectories(di)
            TreeView1.Nodes.Add(di)
        End If
    End Sub

    Private Sub LinkLabel2_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel2.LinkClicked
        ListView1.Clear()
        Call GetDrivesType()
        Me.Text = "My Computer"
        ToolStripComboBox1.Text = "My Computer"
        ToolStripLabel1.Text = "computer"
        Me.Icon = My.Resources.my_computer
    End Sub

    Private Sub LargeIconsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LargeIconsToolStripMenuItem.Click
        Call functions.LargeView()
    End Sub

    Private Sub SmallIconsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SmallIconsToolStripMenuItem.Click
        Call functions.SmallView()
    End Sub

    Private Sub DetailsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DetailsToolStripMenuItem.Click
        Call functions.DetailsView()
    End Sub

    Private Sub ListToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListToolStripMenuItem.Click
        Call functions.ListView()
    End Sub

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        Me.Close()
    End Sub
    Private Sub ListView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListView1.DoubleClick
        'make sure there is an item selected first
        If ListView1.SelectedItems.Count > 0 Then
            If ToolStripLabel1.Text = "computer" Then
                If Directory.Exists("C:\" & ListView1.SelectedItems(0).Text) Then
                    Dim listdir As String = "C:\" & ListView1.SelectedItems(0).Text
                    ToolStripComboBox1.Text = listdir
                    Dim treenode As New TreeNode(listdir)
                    TreeView1.Nodes.Add(treenode)
                    TreeView1.SelectedNode = treenode
                    ListDirectories(listdir) 'call the sub to list the directories in the double clicked item
                End If

            Else
                'get the full directory from the Tag property of the SelectedItem
                If Directory.Exists(ListView1.SelectedItems(0).Tag.ToString) Then
                    Dim listdir As String = ListView1.SelectedItems(0).Tag.ToString
                    ToolStripComboBox1.Text = listdir
                    Dim treenode As New TreeNode(listdir)
                    TreeView1.Nodes.Add(treenode)
                    TreeView1.SelectedNode = treenode
                    ListDirectories(listdir) 'call the sub to list the directories in the double clicked item
                Else
                    If File.Exists(ListView1.SelectedItems.ToString) Then
                        MsgBox(ListView1.SelectedItems.ToString)
                        'Path.GetExtension()
                    End If
                End If
            End If
        End If
    End Sub

    Public Sub ListDirectories(ByVal fullpath As String)
        ListView1.Items.Clear() 'clear any existing items

        'loop through the directories in the fullpath passed to this sub
        If Directory.Exists(fullpath) Then
            For Each dirname As String In IO.Directory.GetDirectories(fullpath)

                'add the new ListViewItem with just the name of the directory
                Dim lvi As ListViewItem = ListView1.Items.Add(IO.Path.GetFileName(dirname), imageIndex:=6)

                'assign the full directory path to the Tag property of the ListViewItem
                lvi.Tag = dirname
                ToolStripComboBox1.Text = dirname
                'Me.Text = dirname
            Next

            Dim di As New IO.DirectoryInfo(fullpath) 'List Files
            Dim diar1 As IO.FileInfo() = di.GetFiles()
            Dim dra As System.IO.FileInfo
            For Each dra In diar1
                ImageList16.Images.Add(Icon.ExtractAssociatedIcon(dra.FullName))
                ImageList32.Images.Add(Icon.ExtractAssociatedIcon(dra.FullName))
                ListView1.Items.Add(dra.ToString, ImageList32.Images.Count - 1)
                ToolStripComboBox1.Text = Path.GetFullPath(dra.DirectoryName)
            Next
        Else
            ToolStripDropDownButton2.Visible = True
            ToolStripDropDownButton2.DropDownItems.Add("Folder not found")
        End If
    End Sub
    Private Sub ToolStripComboBox1_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles ToolStripComboBox1.KeyDown
        If e.KeyCode = Keys.Enter Then
            Call functions.AddressBar()
        End If
    End Sub

    Private Sub ToolStripButton4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton4.Click
        Call functions.AddressBar()
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        If TreeView1.SelectedNode.Parent Is Nothing Then
            TreeView1.SelectedNode = TreeView1.SelectedNode.PrevVisibleNode
            ListDirectories(TreeView1.SelectedNode.Text)
        End If
    End Sub

    Private Sub ToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.Click
        If TreeView1.SelectedNode.Parent Is Nothing Then
            TreeView1.SelectedNode = TreeView1.SelectedNode.NextVisibleNode
            ListDirectories(TreeView1.SelectedNode.Text)
        End If
    End Sub

    Private Sub TreeView1_NodeMouseDoubleClick(ByVal sender As Object, ByVal e As TreeNodeMouseClickEventArgs) Handles TreeView1.NodeMouseDoubleClick
        ListView1.Clear()
        Dim di As String = (TreeView1.SelectedNode.Text)
        ListDirectories(di)
        ToolStripComboBox1.Text = di
    End Sub

    Private Sub LargeIconsToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LargeIconsToolStripMenuItem1.Click
        Call functions.LargeView()
    End Sub

    Private Sub SmallIconsToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SmallIconsToolStripMenuItem1.Click
        Call functions.SmallView()
    End Sub

    Private Sub ListToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListToolStripMenuItem1.Click
        Call functions.ListView()
    End Sub

    Private Sub DetailsToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DetailsToolStripMenuItem1.Click
        Call functions.DetailsView()
    End Sub

    Private Sub ToolStripButton6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton6.Click
        Call Refresh()
    End Sub
    Private Sub RefreshToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RefreshToolStripMenuItem.Click
        Call Refresh()
    End Sub
    Private Sub ListView1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListView1.Click
        If Directory.Exists(ListView1.SelectedItems.Item(0).Tag) Then
            'It's a folder path, do something
        ElseIf File.Exists(ListView1.SelectedItems.Item(0).Tag) Then
            'It's a file path, do something
        Else

        End If
    End Sub

    Private Sub ListView1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListView1.SelectedIndexChanged
        If ListView1.SelectedItems.Count > 0 Then
            ListView1.ContextMenuStrip = itemselect
        Else
            ListView1.ContextMenuStrip = noselect
        End If
    End Sub

    Private Sub AscendingToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AscendingToolStripMenuItem.Click
        Call functions.AscendingSort()
    End Sub

    Private Sub DecendingToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DecendingToolStripMenuItem.Click
        Call functions.DescendingSort()
    End Sub

    Private Sub NoneToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NoneToolStripMenuItem.Click
        Call functions.NoSort()
    End Sub

    Private Sub OptionsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OptionsToolStripMenuItem.Click
        options.Show()
    End Sub
End Class