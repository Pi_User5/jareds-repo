﻿Imports System.IO
Public Class startmenu
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Label1.Text = Environment.UserName
        Label1.Parent = PictureBox1
        PictureBox13.Parent = PictureBox1
        Label1.BackColor = Color.Transparent
        PictureBox13.BackColor = Color.Transparent
        Call startmenutracking.MenuTracking()
        PictureBox13.Cursor = Cursors.Hand
        taskbar.PictureBox1.Image = My.Resources.start_pressed
        AddHandler programmenu.ItemClicked, AddressOf MenuClicked
        AddHandler programmenu.MouseHover, AddressOf MenuHovered    'the whole menu
    End Sub
    Private Sub Form1_Close(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.FormClosing
        taskbar.PictureBox1.Image = My.Resources.start_normal
    End Sub
    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)
        MyBase.Top = Screen.PrimaryScreen.WorkingArea.Bottom - Me.Height - 24
        MyBase.Left = Screen.PrimaryScreen.WorkingArea.Left + 2
    End Sub
    Private Sub RunBox()
        Dim oshellapp As New Object
        oshellapp = CreateObject("Shell.Application")
        oshellapp.FileRun()
        Me.Close()
    End Sub
    Private Sub PictureBox8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox8.Click
        Call RunBox()
    End Sub

    Private Sub Label10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label10.Click
        Call RunBox()
    End Sub

    Private Sub PictureBox7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox7.Click

    End Sub

    Private Sub Label9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label9.Click

    End Sub
    Private Sub PrintFolder()
        'Dim startInfo As New ProcessStartInfo("control.exe")
        'startInfo.WindowStyle = ProcessWindowStyle.Minimized
        'startInfo.Arguments = "printers"
        'Process.Start(startInfo)
        'Me.Close()
        Dim printers As String = Environment.GetFolderPath(Environment.SpecialFolder.PrinterShortcuts)
        browser.ToolStripComboBox1.Text = printers
        browser.Text = "Printers and Faxes"
        browser.Icon = My.Resources.printers
        browser.Show()
        Me.Close()
    End Sub
    Private Sub PictureBox6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox6.Click
        Call PrintFolder()
    End Sub

    Private Sub Label8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label8.Click
        Call PrintFolder()
    End Sub

    Private Sub PictureBox5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox5.Click
        Process.Start("control.exe")
        Me.Close()
    End Sub

    Private Sub Label7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label7.Click
        Process.Start("control.exe")
        Me.Close()
    End Sub
    Private Sub Computer()
        browser.Text = "My Computer"
        browser.Icon = My.Resources.my_computer
        browser.Show()
        Call shellbrowser.MyComputer()
        browser.ToolStripComboBox1.Text = "My Computer"
        Me.Close()
    End Sub
    Private Sub PictureBox4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox4.Click
        Call Computer()
    End Sub

    Private Sub Label6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label6.Click
        Call Computer()
    End Sub
    Private Sub Docs()
        Dim docs As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
        browser.ToolStripComboBox1.Text = docs
        browser.Text = "My Documents"
        browser.Icon = My.Resources.my_documents
        browser.Show()
        Me.Close()
    End Sub
    Private Sub Label4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label4.Click
        Call Docs()
    End Sub

    Private Sub PictureBox2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox2.Click
        Call Docs()
    End Sub
    Private Sub Recent()
        Dim recent As String = Environment.GetFolderPath(Environment.SpecialFolder.Recent)
        browser.ToolStripComboBox1.Text = recent
        browser.Text = "Recent"
        browser.Icon = My.Resources.folder
        browser.Show()
        Me.Close()
    End Sub
    Private Sub Label5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label5.Click
        Call Recent()
    End Sub

    Private Sub PictureBox3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox3.Click
        Call Recent()
    End Sub
    Private Sub Explorer()
        Dim explore As String = Environment.GetFolderPath(Environment.SpecialFolder.Personal)
        browser.ToolStripComboBox1.Text = explore
        browser.Text = "Personal"
        browser.Icon = My.Resources.my_documents
        browser.Show()
        Me.Close()
    End Sub
    Private Sub PictureBox12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox12.Click
        Call Explorer()
    End Sub

    Private Sub Label16_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label16.Click
        Call Explorer()
    End Sub

    Private Sub PictureBox10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox10.Click
        Me.Close()
        msgina.Show()
    End Sub

    Private Sub Label12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label12.Click
        Me.Close()
        msgina.Show()
    End Sub

    Private Sub Panel2_TrackEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Panel2.MouseEnter
        programmenu.Show(Me, New System.Drawing.Point(161, 271))
    End Sub
    Private Sub PitureBox2_TrackEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox2.MouseEnter
        Label4.BackColor = Color.FromArgb(10, 36, 106)
        Label4.ForeColor = Color.White
        PictureBox2.BackColor = Color.FromArgb(10, 36, 106)
        Panel9.BackColor = Color.FromArgb(10, 36, 106)
    End Sub
    'Run
    Private Sub Panel3_TrackEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Panel3.MouseEnter
        Label10.BackColor = Color.FromArgb(10, 36, 106)
        Label10.ForeColor = Color.White
        PictureBox8.BackColor = Color.FromArgb(10, 36, 106)
        Panel3.BackColor = Color.FromArgb(10, 36, 106)
    End Sub
    Private Sub Panel3_TrackExit(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Panel3.MouseLeave
        Label10.BackColor = Color.FromArgb(212, 208, 200)
        Label10.ForeColor = Color.Black
        PictureBox8.BackColor = Color.FromArgb(212, 208, 200)
        Panel3.BackColor = Color.FromArgb(212, 208, 200)
    End Sub
    Private Sub Label10_TrackEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label10.MouseEnter
        Label10.BackColor = Color.FromArgb(10, 36, 106)
        Label10.ForeColor = Color.White
        PictureBox8.BackColor = Color.FromArgb(10, 36, 106)
        Panel3.BackColor = Color.FromArgb(10, 36, 106)
    End Sub
    'Search
    Private Sub Panel4_TrackEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Panel4.MouseEnter
        Label9.BackColor = Color.FromArgb(10, 36, 106)
        Label9.ForeColor = Color.White
        PictureBox7.BackColor = Color.FromArgb(10, 36, 106)
        Panel4.BackColor = Color.FromArgb(10, 36, 106)
    End Sub
    Private Sub Panel4_TrackExit(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Panel4.MouseLeave
        Label9.BackColor = Color.FromArgb(212, 208, 200)
        Label9.ForeColor = Color.Black
        PictureBox7.BackColor = Color.FromArgb(212, 208, 200)
        Panel4.BackColor = Color.FromArgb(212, 208, 200)
    End Sub
    Private Sub Label9_TrackEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label9.MouseEnter
        Label9.BackColor = Color.FromArgb(10, 36, 106)
        Label9.ForeColor = Color.White
        PictureBox7.BackColor = Color.FromArgb(10, 36, 106)
        Panel4.BackColor = Color.FromArgb(10, 36, 106)
    End Sub
    'Printers and Faxes
    Private Sub Panel5_TrackEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Panel5.MouseEnter
        Label8.BackColor = Color.FromArgb(10, 36, 106)
        Label8.ForeColor = Color.White
        PictureBox6.BackColor = Color.FromArgb(10, 36, 106)
        Panel5.BackColor = Color.FromArgb(10, 36, 106)
    End Sub
    Private Sub Panel5_TrackExit(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Panel5.MouseLeave
        Label8.BackColor = Color.FromArgb(212, 208, 200)
        Label8.ForeColor = Color.Black
        PictureBox6.BackColor = Color.FromArgb(212, 208, 200)
        Panel5.BackColor = Color.FromArgb(212, 208, 200)
    End Sub
    Private Sub Label8_TrackEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label8.MouseEnter
        Label8.BackColor = Color.FromArgb(10, 36, 106)
        Label8.ForeColor = Color.White
        PictureBox6.BackColor = Color.FromArgb(10, 36, 106)
        Panel5.BackColor = Color.FromArgb(10, 36, 106)
    End Sub
    'Control Panel
    Private Sub Panel6_TrackEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Panel6.MouseEnter
        Label7.BackColor = Color.FromArgb(10, 36, 106)
        Label7.ForeColor = Color.White
        PictureBox5.BackColor = Color.FromArgb(10, 36, 106)
        Panel6.BackColor = Color.FromArgb(10, 36, 106)
    End Sub
    Private Sub Panel6_TrackExit(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Panel6.MouseLeave
        Label7.BackColor = Color.FromArgb(212, 208, 200)
        Label7.ForeColor = Color.Black
        PictureBox5.BackColor = Color.FromArgb(212, 208, 200)
        Panel6.BackColor = Color.FromArgb(212, 208, 200)
    End Sub
    Private Sub Label7_TrackEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label7.MouseEnter
        Label7.BackColor = Color.FromArgb(10, 36, 106)
        Label7.ForeColor = Color.White
        PictureBox5.BackColor = Color.FromArgb(10, 36, 106)
        Panel6.BackColor = Color.FromArgb(10, 36, 106)
    End Sub
    'My Computer
    Private Sub Panel7_TrackEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Panel7.MouseEnter
        Label6.BackColor = Color.FromArgb(10, 36, 106)
        Label6.ForeColor = Color.White
        PictureBox4.BackColor = Color.FromArgb(10, 36, 106)
        Panel7.BackColor = Color.FromArgb(10, 36, 106)
    End Sub
    Private Sub Panel7_TrackExit(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Panel7.MouseLeave
        Label6.BackColor = Color.FromArgb(212, 208, 200)
        Label6.ForeColor = Color.Black
        PictureBox4.BackColor = Color.FromArgb(212, 208, 200)
        Panel7.BackColor = Color.FromArgb(212, 208, 200)
    End Sub
    Private Sub Label6_TrackEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label6.MouseEnter
        Label6.BackColor = Color.FromArgb(10, 36, 106)
        Label6.ForeColor = Color.White
        PictureBox4.BackColor = Color.FromArgb(10, 36, 106)
        Panel7.BackColor = Color.FromArgb(10, 36, 106)
    End Sub
    'Recent
    Private Sub Panel8_TrackEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Panel8.MouseEnter
        Label5.BackColor = Color.FromArgb(10, 36, 106)
        Label5.ForeColor = Color.White
        PictureBox3.BackColor = Color.FromArgb(10, 36, 106)
        Panel8.BackColor = Color.FromArgb(10, 36, 106)
    End Sub
    Private Sub Panel8_TrackExit(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Panel8.MouseLeave
        Label5.BackColor = Color.FromArgb(212, 208, 200)
        Label5.ForeColor = Color.Black
        PictureBox3.BackColor = Color.FromArgb(212, 208, 200)
        Panel8.BackColor = Color.FromArgb(212, 208, 200)
    End Sub
    Private Sub Label5_TrackEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label5.MouseEnter
        Label5.BackColor = Color.FromArgb(10, 36, 106)
        Label5.ForeColor = Color.White
        PictureBox3.BackColor = Color.FromArgb(10, 36, 106)
        Panel8.BackColor = Color.FromArgb(10, 36, 106)
    End Sub
    'My Documents
    Private Sub Panel9_TrackEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Panel9.MouseEnter
        Label4.BackColor = Color.FromArgb(10, 36, 106)
        Label4.ForeColor = Color.White
        PictureBox2.BackColor = Color.FromArgb(10, 36, 106)
        Panel9.BackColor = Color.FromArgb(10, 36, 106)
    End Sub
    Private Sub Panel9_TrackExit(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Panel9.MouseLeave
        Label4.BackColor = Color.FromArgb(212, 208, 200)
        Label4.ForeColor = Color.Black
        PictureBox2.BackColor = Color.FromArgb(212, 208, 200)
        Panel9.BackColor = Color.FromArgb(212, 208, 200)
    End Sub
    Private Sub Label4_TrackEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label4.MouseEnter
        Label4.BackColor = Color.FromArgb(10, 36, 106)
        Label4.ForeColor = Color.White
        PictureBox2.BackColor = Color.FromArgb(10, 36, 106)
        Panel9.BackColor = Color.FromArgb(10, 36, 106)
    End Sub
    Private Sub MenuHovered(ByVal sender As Object, ByVal e As EventArgs)
        'Dim item As ToolStripItem = e. ' nothing to see here
    End Sub

    Private Sub Menu1Hovered(ByVal sender As Object, ByVal e As EventArgs)
        Dim item As ToolStripItem = CType(sender, ToolStripItem)
        'we know which item was clicked
    End Sub

    Private Sub MenuClicked(ByVal sender As Object, ByVal e As ToolStripItemClickedEventArgs)
        Dim item As ToolStripItem = e.ClickedItem
        'MessageBox.Show(item.Text)
        Dim BasePath As String
        Dim parsepathxp As String
        Dim parsepathvista As String
        Dim folderpath As String
        BasePath = Environment.GetFolderPath(Environment.SpecialFolder.CommonStartMenu)
        parsepathvista = BasePath & "\Programs\" & item.Text & ".url"
        parsepathxp = BasePath & "\Programs\" & item.Text & ".lnk"
        folderpath = BasePath & "\Programs\" & item.Text
        If My.Computer.Info.OSVersion > "6.0" Then
            If File.Exists(parsepathxp) Then
                ' This path is a file.
                Process.Start(parsepathxp)
                Me.Close()
            End If
        End If
        If My.Computer.Info.OSVersion < "6.0" Then
            If File.Exists(parsepathvista) Then
                ' This path is a file.
                Process.Start(parsepathvista)
                Me.Close()
            End If
        End If
        If Directory.Exists(folderpath) Then
            ' This path is a directory.
        End If
    End Sub

    Private Sub PictureBox13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox13.Click
        Call Explorer()
    End Sub
    Private Sub PictureBox9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox9.Click
        Application.Exit()
        Process.Start(Application.StartupPath.ToString & "\Jared Shell.exe")
    End Sub
End Class
