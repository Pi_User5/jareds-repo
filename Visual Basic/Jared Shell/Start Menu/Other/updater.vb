﻿Imports System.Net
Imports System.IO
Public Class updater
    Dim url As String = "https://dl.dropboxusercontent.com/s/j4s4ojurqysltdi/update-file.txt"
    Dim filepath As String = "D:\update-file.txt"
    Dim filecontents As String '= "1.0.0.0"
    Private Sub updater_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call CheckForUpdates()
    End Sub
    Public WithEvents download As WebClient
    Public Sub CheckForUpdates()
        download = New WebClient
        download.DownloadFileAsync(New Uri(url), filepath)
        If filecontents = My.Application.Info.Version.ToString Then
            MsgBox("Program is up to date")
            ToolStripStatusLabel1.Text = "Program is up to date"
            Button1.Enabled = False
            Me.Close()
        Else
            ToolStripStatusLabel1.Text = "New update found"
            Button1.Enabled = True
            'filecontents = File.ReadAllText(filepath)
            'MsgBox(filecontents)
            'File.Delete(filepath)
        End If
        ProgressBar1.Value = 0
    End Sub
    Public Sub CheckForUpdatesSlim()
        download = New WebClient
        download.DownloadFileAsync(New Uri(url), filepath)
        If filecontents = My.Application.Info.Version.ToString Then
            taskbar.ProgramUpdatesToolStripMenuItem.Visible = False
        Else
            taskbar.ProgramUpdatesToolStripMenuItem.Visible = True
        End If
    End Sub
    Public WithEvents download_updates As WebClient
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        ListBox1.Items.Add("Preparing to download updates")
    End Sub
    Private Sub downloadupdates_DownloadProgressChanged(ByVal sender As Object, ByVal e As DownloadProgressChangedEventArgs) Handles download_updates.DownloadProgressChanged
        ProgressBar1.Value = e.ProgressPercentage
    End Sub
End Class