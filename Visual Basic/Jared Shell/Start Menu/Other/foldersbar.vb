﻿Imports System.IO

Public Class foldersbar
    Private allowCoolMove As Boolean = False
    Private myCoolPoint As New Point
    Private Sub PictureBox2_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Panel1.MouseDown
        If e.Button = MouseButtons.Left Then
            allowCoolMove = True
            myCoolPoint = New Point(e.X, e.Y)
        End If
    End Sub
    Private Sub Panel1_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Panel1.MouseMove
        If e.Button = MouseButtons.Left Then
            If allowCoolMove = True Then
                Me.Location = New Point(Me.Location.X + e.X - myCoolPoint.X, Me.Location.Y + e.Y - myCoolPoint.Y)
            End If
        End If
    End Sub
    Private Sub Panel1_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Panel1.MouseUp
        If e.Button = MouseButtons.Left Then
            allowCoolMove = False
        End If
    End Sub
    'Label Move
    Private Sub Label1_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Label1.MouseDown
        If e.Button = MouseButtons.Left Then
            allowCoolMove = True
            myCoolPoint = New Point(e.X, e.Y)
        End If
    End Sub
    Private Sub Label1_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Label1.MouseMove
        If e.Button = MouseButtons.Left Then
            If allowCoolMove = True Then
                Me.Location = New Point(Me.Location.X + e.X - myCoolPoint.X, Me.Location.Y + e.Y - myCoolPoint.Y)
            End If
        End If
    End Sub
    Private Sub Label1_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Label1.MouseUp
        If e.Button = MouseButtons.Left Then
            allowCoolMove = False
        End If
    End Sub
    Private Sub Label2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label2.Click
        Me.Close()
    End Sub
    Public Sub PopulateTreeView()
        Dim rootNode As TreeNode
        Dim info As New DirectoryInfo("C:\")
        If info.Exists Then
            rootNode = New TreeNode(info.Name)
            rootNode.Tag = info
            GetDirectories(info.GetDirectories(), rootNode)
            TreeView1.Nodes.Add(rootNode)
        End If
    End Sub

    Private Sub GetDirectories(ByVal subDirs() As DirectoryInfo, ByVal nodeToAddTo As TreeNode)
        Dim aNode As TreeNode
        Dim subSubDirs() As DirectoryInfo
        Dim subDir As DirectoryInfo
        For Each subDir In subDirs
            Try
                aNode = New TreeNode(subDir.Name, 0, 0)
                aNode.Tag = subDir
                aNode.ImageKey = "folder"
                subSubDirs = subDir.GetDirectories()
                If subSubDirs.Length <> 0 Then
                    GetDirectories(subSubDirs, aNode)
                End If
                nodeToAddTo.Nodes.Add(aNode)
            Catch ex As Exception
            End Try
        Next subDir
    End Sub
    Public Sub New()
        InitializeComponent()
    End Sub 'New
    Private Sub PictureBox8_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles PictureBox8.DoubleClick
        If e.Button = Windows.Forms.MouseButtons.Left Then
            Me.Close()
        End If
    End Sub
End Class