﻿Module functions
    Public Sub LargeView()
        browser.ListView1.View = View.LargeIcon
    End Sub
    Public Sub SmallView()
        browser.ListView1.View = View.SmallIcon
    End Sub
    Public Sub DetailsView()
        browser.ListView1.View = View.Details
    End Sub
    Public Sub ListView()
        browser.ListView1.View = View.List
    End Sub
    Public Sub AddressBar()
        'browser.ListView1.Focus()
        'Dim di As String = (browser.ToolStripComboBox1.Text)
        'browser.ListDirectories(di)
    End Sub
    Public Sub Refresh()
        'browser.ListDirectories(browser.ToolStripComboBox1.Text)
    End Sub
    Public Sub AscendingSort()
        browser.ListView1.Sorting = SortOrder.Ascending
    End Sub
    Public Sub DescendingSort()
        browser.ListView1.Sorting = SortOrder.Descending
    End Sub
    Public Sub NoSort()
        browser.ListView1.Sorting = SortOrder.None
    End Sub
End Module
