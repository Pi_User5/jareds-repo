﻿Public Class Form1
    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        Application.Exit()
    End Sub

    Private Sub PictureBox1_Click(ByVal sender As System.Object, ByVal e As MouseEventArgs) Handles PictureBox1.MouseDown, PictureBox1.Click
        If e.Button = MouseButtons.Left Then

        End If
    End Sub

    Private Sub PictureBox1_Move(ByVal sender As System.Object, ByVal e As MouseEventArgs) Handles PictureBox1.MouseMove
        If e.Button = MouseButtons.Left Then
            If PenToolStripMenuItem.Checked = True Then
                Dim col As Color = Color.FromArgb(ToolStripButton11.BackColor.ToArgb)
                Dim p As New SolidBrush(col)
                PictureBox1.Cursor = Cursors.Hand
                PictureBox1.CreateGraphics.FillEllipse(p, e.X, e.Y, 5, 5)
            End If
            If HighlighterToolStripMenuItem.Checked = True Then
                Dim col As Color = Color.FromArgb(75, 50, 100, 100)
                Dim h As New SolidBrush(col)
                PictureBox1.Cursor = Cursors.IBeam
                PictureBox1.CreateGraphics.FillRectangle(h, e.X, e.Y, 8, 26)
            End If
            If EraserToolStripMenuItem.Checked = True Then
                PictureBox1.Cursor = Cursors.No
                PictureBox1.CreateGraphics.FillEllipse(Brushes.White, e.X, e.Y, 15, 15)
            End If
        End If
    End Sub
    Private Sub PictureBox1_Up(ByVal sender As System.Object, ByVal e As MouseEventArgs) Handles PictureBox1.MouseUp
        PictureBox1.Cursor = Cursors.Default
    End Sub

    Private Sub ToolStripButton7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton7.Click
        PenToolStripMenuItem.Checked = True
        HighlighterToolStripMenuItem.Checked = False
        EraserToolStripMenuItem.Checked = False
    End Sub

    Private Sub ToolStripButton8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton8.Click
        PenToolStripMenuItem.Checked = False
        HighlighterToolStripMenuItem.Checked = True
        EraserToolStripMenuItem.Checked = False
    End Sub

    Private Sub HighlighterToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles HighlighterToolStripMenuItem.Click
        PenToolStripMenuItem.Checked = False
        HighlighterToolStripMenuItem.Checked = True
        EraserToolStripMenuItem.Checked = False
    End Sub

    Private Sub ToolStripButton9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton9.Click
        PenToolStripMenuItem.Checked = False
        HighlighterToolStripMenuItem.Checked = False
        EraserToolStripMenuItem.Checked = True
    End Sub

    Private Sub EraserToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EraserToolStripMenuItem.Click
        PenToolStripMenuItem.Checked = False
        HighlighterToolStripMenuItem.Checked = False
        EraserToolStripMenuItem.Checked = True
    End Sub

    Private Sub ToolStripButton11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton11.Click
        Dim myResult As System.Windows.Forms.DialogResult = New System.Windows.Forms.DialogResult
        myResult = ColorDialog1.ShowDialog
        If myResult = Windows.Forms.DialogResult.OK Then
            ToolStripButton11.BackColor = ColorDialog1.Color
        End If
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.WindowState = FormWindowState.Maximized
    End Sub

    Private Sub OpenImage()
        OpenFileDialog1.Filter = "BMP Image|*.bmp|JPG Image|*.jpg|PNG Image|*.png"
        OpenFileDialog1.CheckFileExists = True
        OpenFileDialog1.CheckPathExists = True
        OpenFileDialog1.ShowDialog()
    End Sub
    Private Sub OpenFileDialog1_FileOk(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles OpenFileDialog1.FileOk
        PictureBox1.Image = New Bitmap(OpenFileDialog1.FileName)
    End Sub

    Private Sub SaveImage()
        SaveFileDialog1.Filter = "PNG|*.png|All files (*.*)|*.*"
        SaveFileDialog1.CheckPathExists = True
        SaveFileDialog1.ShowDialog()
    End Sub

    Private Sub SaveFileDialog1_FileOk(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles SaveFileDialog1.FileOk
        Dim FileToSaveAs As String = System.IO.Path.Combine(My.Computer.FileSystem.SpecialDirectories.Temp, SaveFileDialog1.FileName)
        PictureBox1.Image.Save(FileToSaveAs, System.Drawing.Imaging.ImageFormat.Png)
    End Sub
    Private Sub SaveToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveToolStripMenuItem1.Click

    End Sub

    Private Sub ToolStripButton3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton3.Click

    End Sub

    Private Sub PrintToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrintToolStripMenuItem.Click

    End Sub

    Private Sub ToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.Click
        Call OpenImage()
    End Sub

    Private Sub SaveToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveToolStripMenuItem.Click
        Call OpenImage()
    End Sub

    Private Sub NewToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NewToolStripMenuItem.Click
        newwarning.Show()
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        newwarning.Show()
    End Sub

    Private Sub AboutToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AboutToolStripMenuItem.Click
        aboutbox.Show()
    End Sub

    Private Sub ToolStripButton4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton4.Click
        Clipboard.SetDataObject(Me.PictureBox1.Image)
        PictureBox1.Image = Nothing
        ToolStripButton4.Enabled = False
    End Sub

    Private Sub ToolStripButton5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton5.Click
        Clipboard.SetDataObject(Me.PictureBox1.Image)
    End Sub

    Private Sub ToolStripButton6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton6.Click
        Dim data As IDataObject
        data = Clipboard.GetDataObject()
        Dim bmap As Bitmap
        If data.GetDataPresent(GetType(System.Drawing.Bitmap)) Then
            bmap = CType(data.GetData(GetType(System.Drawing.Bitmap)), Bitmap)
            Me.PictureBox1.Image = bmap
        End If
    End Sub
End Class
