﻿Imports System.IO
Public Class shellbackground

    Private Sub shellbackground_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.WindowState = FormWindowState.Maximized
        loginscreen.StartPosition = FormStartPosition.Manual
        loginscreen.TopLevel = False
        Me.Panel1.Controls.Add(loginscreen)
        loginscreen.Top = (Screen.PrimaryScreen.Bounds.Height - loginscreen.Height) / 2
        loginscreen.Left = (Screen.PrimaryScreen.Bounds.Width - loginscreen.Width) / 2
        loginscreen.Show()
    End Sub
    Public Sub DesktopFolder()
        Dim DeskPath As String
        DeskPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) & "\"
        If Directory.Exists(DeskPath) Then
            For Each dirname As String In IO.Directory.GetDirectories(DeskPath)
                ListView1.Items.Add(IO.Path.GetFileName(dirname), imageIndex:=4)
            Next
            Dim di As New IO.DirectoryInfo(DeskPath) 'List Files
            Dim diar1 As IO.FileInfo() = di.GetFiles()
            Dim dra As System.IO.FileInfo
            For Each dra In diar1
                FileIcons32.Images.Add(Icon.ExtractAssociatedIcon(dra.FullName))
                ListView1.Items.Add(Path.GetFileNameWithoutExtension(dra.ToString), FileIcons32.Images.Count - 1)
            Next
        End If
    End Sub
End Class