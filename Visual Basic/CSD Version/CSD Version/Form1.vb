﻿Imports Microsoft.Win32
Public Class Form1
    Dim OSname As String
    Dim buildVersion As String
    Dim buildNumber As String
    Dim currentCSD As String
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        OSname = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion", "ProductName", Nothing)
        buildVersion = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion", "CurrentVersion", Nothing)
        buildNumber = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion", "CurrentBuildNumber", Nothing)
        currentCSD = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Windows", "CSDVersion", Nothing)
        Label1.Text = "Product Name: " & OSname
        Label2.Text = "Build Number " & buildVersion & "." & buildNumber
        ComboBox1.SelectedItem = currentCSD
        Select Case currentCSD
            Case "0"
                Label3.Text = "Service Pack: None Installed"
            Case "1"
                Label3.Text = "Service Pack: Service Pack 1"
            Case "2"
                Label3.Text = "Service Pack: Service Pack 2"
            Case "3"
                Label3.Text = "Service Pack: Service Pack 3"
            Case Else
                Label3.Text = "Service Pack: Unknown Version"
        End Select
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Call UpdateSP()
        Button3.Enabled = False
    End Sub
    Private Sub UpdateSP()
        Dim csdvalue = My.Computer.Registry.LocalMachine.OpenSubKey("SYSTEM\CurrentControlSet\Control\Windows", True)
        csdvalue.SetValue("CSDVersion", ComboBox1.SelectedItem.ToString)
        csdvalue.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Call UpdateSP()
        Application.Exit()
    End Sub
End Class
